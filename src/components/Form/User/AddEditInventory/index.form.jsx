/* eslint-disable import/named */
import { Formik, Form } from "formik";
import React from "react";
import { Col, Row } from "react-bootstrap";
import { AntTextArea, Input as TextInput } from "../../../Antd";
import { CommonButton } from "../../../UiElement";
import validation from "./validation";
import CustomSelect from "../../../UiElement/CustoSelect";
import { enterOnlyIMEINumericValue } from "../../../../utils";

function AddEditInventoryForm({
  onSubmit,
  rowData,
  loading,
  brandLoading,
  modalLoading,
  variantLoading,
  brandData,
  modalData,
  variantData,
  getModalData,
  getVariantData,
  setIDS,
  ids,
  getDeviceData,
  deviceData,
  getColorData,
  colorData,
  colorLoading,
  vandorLoading,
  vandorData,
  barcodeData,
}) {
  const initialValues = {
    variant_id: rowData?.fk_variant_id || undefined,
    fk_inventory_id: "1",
    status: "pending",
    barcode: barcodeData || rowData?.barcode || "",
    date: rowData?.date || "",
    fk_device_id: rowData?.fk_device_id || undefined,
    fk_vendor_id: rowData?.fk_vendor_id || undefined,
    invoice_number: rowData?.invoice_number || "1",
    imei_serial_no_first: rowData?.imei_serial_no_first || "",
    imei_serial_no_second: rowData?.imei_serial_no_second || "",
    purchase_price_no_gst: rowData?.purchase_price_no_gst || "",
    sgst: rowData?.sgst || "",
    cgst: rowData?.cgst || "",
    igst: rowData?.igst || "",
    remark: rowData?.remark || "",
    price: rowData?.price || "",
    inv_status: rowData?.inv_status || undefined,
    color: rowData?.color || undefined,
    gst: undefined,
    model_id: rowData?.fk_model_id || undefined,
    brand_id: rowData?.fk_brand_id || undefined,
  };
  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={validation()}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {(props) => {
        return (
          <Form>
            <Row>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Barcode
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="barcode"
                      disabled
                      variant="standard"
                      type="text"
                      placeholder="Enter barcode"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    IMEI/SerialNo.
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="imei_serial_no_first"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter IMEI/SerialNo"
                      setFieldValue={props.handleChange}
                      min="0"
                      onKeyPress={(e) => {
                        // onCheckValue(e);
                        enterOnlyIMEINumericValue(e);
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group mt-1">
                  <label className="form-label" htmlFor="category-name">
                    IMEI/SerialNo.
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="imei_serial_no_second"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter IMEI/SerialNo"
                      setFieldValue={props.handleChange}
                      min="0"
                      onKeyPress={(e) => {
                        // onCheckValue(e);
                        enterOnlyIMEINumericValue(e);
                      }}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group mt-1">
                  <label className="form-label" htmlFor="category-name">
                    SerialNo.
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="serial_no"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter SerialNo"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group mt-1">
                  <label className="form-label" htmlFor="category-name">
                    Purchase Price With GST
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="purchase_price_no_gst"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter purchase Price With GST"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>

              <Col sm={4}>
                <div className="form-group mt-1">
                  <label className="form-label" htmlFor="category-name">
                    GST
                  </label>
                  <div className="form-control-wrap ">
                    <CustomSelect
                      loading={brandLoading}
                      value={rowData?.gst}
                      id="gst"
                      extraClassName="form-control form-control-lg"
                      name="gst"
                      disabled={false}
                      placeholder="Select GST"
                      arrayOfData={[
                        {
                          name: "5%",
                          id: "5%",
                        },
                        {
                          name: "8%",
                          id: "8%",
                        },
                        {
                          name: "18%",
                          id: "18%",
                        },
                        {
                          name: "28%",
                          id: "28%",
                        },
                      ]}
                      // onSelect={(e) => {
                      //   getModalData(props, e);
                      //   setIDS({
                      //     ...ids,
                      //     brand_id: e,
                      //   });
                      // }}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>

              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Selling Price
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="price"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter Selling Price"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Brand
                  </label>
                  <div className="form-control-wrap">
                    <CustomSelect
                      showSearch
                      loading={brandLoading}
                      // value={rowData?.fk_brand_id}
                      id="brand_id"
                      extraClassName="form-control form-control-lg"
                      name="brand_id"
                      disabled={false}
                      placeholder="Select Brand"
                      arrayOfData={brandData}
                      onSelect={(e) => {
                        getModalData(props, e);
                        setIDS({
                          ...ids,
                          brand_id: e,
                        });
                      }}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Model
                  </label>
                  <div className="form-control-wrap">
                    <CustomSelect
                      showSearch
                      // value={rowData?.fk_model_id}
                      loading={modalLoading}
                      id="model_id"
                      extraClassName="form-control form-control-lg"
                      name="model_id"
                      disabled={false}
                      placeholder="Select Model"
                      arrayOfData={modalData}
                      onSelect={(e) => {
                        getVariantData(props, e);
                        setIDS({
                          ...ids,
                          model_id: e,
                        });
                      }}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Variant
                  </label>
                  <div className="form-control-wrap">
                    <CustomSelect
                      showSearch
                      // value={rowData?.fk_variant_id}
                      loading={variantLoading}
                      id="variant_id"
                      extraClassName="form-control form-control-lg"
                      name="variant_id"
                      disabled={false}
                      placeholder="Select Variant"
                      arrayOfData={variantData}
                      onSelect={(e) => {
                        getDeviceData(props, e);
                        setIDS({
                          ...ids,
                          variant_id: e,
                        });
                      }}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Device
                  </label>
                  <div className="form-control-wrap">
                    <CustomSelect
                      showSearch
                      // loading={brandLoading}
                      id="fk_device_id"
                      extraClassName="form-control form-control-lg"
                      name="fk_device_id"
                      disabled={false}
                      placeholder="Select Device"
                      arrayOfData={deviceData}
                      onSelect={(e) => {
                        getColorData(props, e);
                        setIDS({
                          ...ids,
                          device_id: e,
                        });
                      }}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Color
                  </label>
                  <div className="form-control-wrap">
                    <CustomSelect
                      showSearch
                      loading={colorLoading}
                      id="color"
                      extraClassName="form-control form-control-lg"
                      name="color"
                      disabled={false}
                      placeholder="Select Color"
                      arrayOfData={colorData}
                      // onSelect={(e) => {
                      //   getColorData(props, e);
                      //   setIDS({
                      //     ...ids,
                      //     device_id: e,
                      //   });
                      // }}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>

              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Vendor Name / Code
                  </label>
                  <div className="form-control-wrap">
                    <CustomSelect
                      showSearch
                      loading={vandorLoading}
                      id="fk_vendor_id"
                      extraClassName="form-control form-control-lg"
                      name="fk_vendor_id"
                      disabled={false}
                      placeholder="Select Vendor Name / Code"
                      arrayOfData={vandorData}
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>

              <Col sm={4}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Remark <span>*</span>
                  </label>
                  <div className="form-control-wrap">
                    <AntTextArea
                      className="form-control"
                      name="remark"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter Remark"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </Col>
              <div className="form-group text-center mt-3 ">
                <CommonButton
                  type="submit"
                  htmlType="submit"
                  style={{ display: "table-footer-group" }}
                  className="btn btn-primary ripple-effect w-20"
                  loading={loading}
                >
                  {rowData?.id ? "Update" : "Submit"}
                </CommonButton>
              </div>
            </Row>
          </Form>
        );
      }}
    </Formik>
  );
}

export default AddEditInventoryForm;

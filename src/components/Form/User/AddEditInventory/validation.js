import * as yup from "yup";

export default function validation(value) {
  return yup.object().shape({
    barcode: yup.string().required("Please enter barcode"),
    fk_device_id:
      value !== "usedInv" ? yup.string().required("Please enter device") : null,
    fk_vendor_id: yup.string().required("Please enter vendor"),
    imei_serial_no_first: yup.string().required("Please enter IMEI serial no"),
    // // imei_serial_no_second: yup.string().required("Please enter IMEI serial no"),
    purchase_price_no_gst: yup.string().required("Please enter purchase price"),
    price:
      value !== "usedInv" ? yup.string().required("Please enter price") : null,
    selling_price:
      value === "usedInv"
        ? yup.string().required("Please enter selling price")
        : null,
  });
}

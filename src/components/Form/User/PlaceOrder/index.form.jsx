import { Formik, Form } from "formik";
import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
// import { useNavigate } from "react-router-dom";
import { AntTextArea, Input as TextInput } from "../../../Antd";
// import validation from "./validation";
import {
  CommonButton,
  DataTable,
  MultiSelect,
  SweetAlert,
} from "../../../UiElement";
import {
  getVanderService,
  getnewInventoryListService,
} from "../../../../services/Admin/Master/index.service";
import {
  checkValidData,
  enterOnlyNumericValue,
  logger,
} from "../../../../utils";
import validation from "./validation";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";

function OrderForm({
  onSubmit,
  loading,
  // rowData,
  setDeviceListData,
  deviceListData,
  setCustomerName,
  customerName,
  setCustomerModal,
  setCustomerNumber,
  paymentData,
  setTotalPrice,
  totalPrice,
  // customerNumber,
}) {
  // const navigate = useNavigate();

  // const [searchName, setSearchName] = useState("");
  const [isAlertVisible, setIsAlertVisible] = useState(false);
  const [barcodeValue, setBarcodeValue] = useState({
    props: "",
    barcode: "",
  });

  const [paymentType, setPaymentType] = useState([]);
  const [paymentMode, setPaymentMode] = useState();

  const initialValues = {
    fk_customer_id: customerName?.name || "",
    description: "",
    payment_type: undefined,
    totalPrice: totalPrice || "",
    discountedPrice: "",
    fk_inv_dvc_id: "",
    barcode: "",
    mobileNumber: "",
  };

  const getCustomerData = async (value, props) => {
    try {
      let payload = {
        offset: 0,
        search: value,
        user_type: "customer",
      };

      setCustomerNumber(value);

      const res = await getVanderService(payload);
      if (res?.data?.status === "1") {
        setCustomerName(res?.data?.data?.user_list[0]);
      } else {
        setIsAlertVisible(true);
        setCustomerName(true);
      }
      props.setFieldValue("mobileNumber", value);
    } catch (error) {
      logger(error);
    }
  };

  const getDeviceData = async (value) => {
    try {
      let payload = {
        barcode: value || "",
        brand_id: "",
        model_id: "",
        variant_id: "",
        offset: 0,
      };

      const res = await getnewInventoryListService(payload);
      if (res?.data?.status === "1") {
        if (deviceListData?.length > 0) {
          setDeviceListData([
            ...deviceListData,
            res?.data?.data?.mob_devices[0],
          ]);
        } else {
          setDeviceListData([res?.data?.data?.mob_devices[0]]);
        }
        barcodeValue.props.setFieldValue("barcode", "");
      }
    } catch (error) {
      logger(error);
    }
  };

  const columns = [
    {
      dataField: "barcode",
      text: "Barcode",
      // headerClasses: "w_70",
      formatter: (cell, row) => checkValidData(row?.barcode),
    },
    {
      dataField: "device_name",
      text: "Device Name",
      // headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(row?.device_name),
    },
    {
      dataField: "price",
      text: "Price",
      // headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(row?.price),
    },
  ];

  const onConfirmAlert = () => {
    // setTableLoading(true);

    setIsAlertVisible(false);
    // setCustomerName(false);
    // navigate(adminRouteMap.CUSTOMER.path);
    setCustomerModal(true);
    return false;
  };

  useEffect(() => {
    let result = paymentData?.map((elm) => {
      if (paymentType?.includes(elm?.value)) {
        return elm;
      }
    });
    const resData = result?.filter((item) => item !== undefined && item);
    setPaymentMode(resData);
  }, [paymentType]);

  // useEffect(() => {
  //   if (customerNumber?.length >= 10) {
  //     getCustomerData(customerNumber);
  //   } else {
  //     setCustomerName({
  //       ...customerName,
  //       name: "",
  //     });
  //   }
  // }, [customerNumber]);

  useEffect(() => {
    if (barcodeValue?.barcode?.length >= 4) {
      getDeviceData(barcodeValue?.barcode);
    }
    // else {
    //   setDeviceListData([]);
    //   setCustomerName("");
    // }
  }, [barcodeValue]);

  useEffect(() => {
    if (deviceListData?.length > 0) {
      const result = deviceListData.reduce((sum, item) => {
        return sum + parseFloat(item?.price);
      }, 0);
      setTotalPrice(result);
    } else {
      setTotalPrice();
    }
  }, [deviceListData]);

  return (
    <>
      <Formik
        initialValues={{ ...initialValues }}
        validationSchema={validation()}
        onSubmit={onSubmit}
        enableReinitialize
      >
        {(props) => {
          if (props?.values?.mobileNumber?.length >= 10 && !customerName) {
            getCustomerData(props?.values?.mobileNumber, props);
          }
          setPaymentType(props?.values?.payment_type);

          return (
            <Form>
              <Row gutter={12}>
                <Col md={4}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Barcode
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="barcode"
                        disabled={false}
                        // variant="standard"
                        type="text"
                        placeholder="Enter barcode"
                        setFieldValue={props.handleChange}
                        setValues={(e) =>
                          setBarcodeValue({
                            ...barcodeValue,
                            props,
                            barcode: e,
                          })
                        }
                      />
                    </div>
                  </div>
                </Col>

                <Col md={4}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Mobile Number
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="mobileNumber"
                        disabled={false}
                        // value={customerNumber}
                        type="text"
                        // setValues={(e) => getCustomerData(e, props)}
                        placeholder="Enter mobile number"
                        setFieldValue={props.handleChange}
                        min="0"
                        inputmode="numeric"
                        onKeyPress={(e) => {
                          // onCheckValue(e);
                          enterOnlyNumericValue(e);
                        }}
                      />
                    </div>
                    {/* {!searchName && (
                      <div style={{ display: "flex", flexWrap: "nowrap" }}>
                        <div
                          className="ant-form-item-explain ant-form-item-explain-connected css-dev-only-do-not-override-1okl62o"
                          role="alert"
                        >
                          <div
                            className="ant-form-item-explain-error"
                            style={{ color: "#ff4d4f" }}
                          >
                            mobileNumber is requierd
                          </div>
                        </div>
                      </div>
                    )} */}
                  </div>
                </Col>
                <Col md={4}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Booked For
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        value={customerName?.name}
                        className="form-control"
                        name="fk_customer_id"
                        disabled
                        variant="standard"
                        type="text"
                        placeholder="Booked for"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>
                <Col md={4}>
                  <div className="form-group mt-1">
                    <label className="form-label" htmlFor="category-name">
                      Total Price
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        value={totalPrice}
                        className="form-control"
                        name="totalPrice"
                        disabled
                        variant="standard"
                        type="text"
                        placeholder="Total Price"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>
                <Col md={4}>
                  <div className="form-group mt-1">
                    <label className="form-label" htmlFor="category-name">
                      Discounted Price
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="discountedPrice"
                        variant="standard"
                        type="text"
                        placeholder="Enter booked for"
                        setFieldValue={props.handleChange}
                        min="0"
                        inputmode="numeric"
                        onKeyPress={(e) => {
                          // onCheckValue(e);
                          enterOnlyNumericValue(e);
                        }}
                      />
                    </div>
                  </div>
                </Col>
                <Col md={6}>
                  <div className="form-group mt-1">
                    <label className="form-label" htmlFor="category-name">
                      Description
                    </label>
                    <div className="form-control-wrap">
                      <AntTextArea
                        className="form-control"
                        name="description"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter description"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>

                <Col md={4}>
                  <div className="form-group mt-1">
                    <label className="form-label" htmlFor="category-name">
                      Payment Type
                    </label>
                    <div className="form-control-wrap">
                      <MultiSelect
                        // loading={modalLoading}
                        id="payment_type"
                        extraClassName="form-control form-control-lg"
                        name="payment_type"
                        disabled={false}
                        placeholder="Select payment type"
                        options={paymentData}
                        mode="multiple"
                        setFieldValue={props?.handleChange}
                      />
                    </div>
                  </div>
                </Col>
                {paymentMode?.length > 0 &&
                  paymentMode?.map((item) => (
                    <Col md={4}>
                      <div className="form-group">
                        <label className="form-label" htmlFor="category-name">
                          {item?.label}
                        </label>
                        <div className="form-control-wrap">
                          <TextInput
                            className="form-control"
                            name={item?.label}
                            disabled={false}
                            variant="standard"
                            type="text"
                            placeholder={`Please enter ${item?.label} amount`}
                            setFieldValue={props.handleChange}
                            min="0"
                            inputmode="numeric"
                            onKeyPress={(e) => {
                              // onCheckValue(e);
                              enterOnlyNumericValue(e);
                            }}
                          />
                        </div>
                      </div>
                    </Col>
                  ))}

                <Col md={12}>
                  <DataTable
                    header={false}
                    pagination={false}
                    hasLimit
                    noOfPage="1"
                    sizePerPage="10"
                    page="1"
                    count="100"
                    tableData={deviceListData}
                    tableColumns={columns}
                    // param={param}
                    // defaultSort={defaultSort}
                    setSizePerPage=""
                    // tableLoader={tableLoader}
                    // tableReset={tableReset}
                    // getSearchValue={getSearchValue}
                    // searchPlaceholder={t("text.search.ManageSubscription")}
                  />
                </Col>
              </Row>

              <div className="form-group text-center mt-3 ">
                <CommonButton
                  type="submit"
                  htmlType="submit"
                  style={{ display: "table-footer-group" }}
                  className="btn btn-primary ripple-effect w-20"
                  loading={loading}
                >
                  Place Order
                </CommonButton>
              </div>
            </Form>
          );
        }}
      </Formik>

      <SweetAlert
        title="Number Not Register"
        text="you want to register this number?"
        show={isAlertVisible}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisible}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onConfirmAlert}
      />
    </>
  );
}

export default OrderForm;

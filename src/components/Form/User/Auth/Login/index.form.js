import { Form, Formik } from "formik";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Input as TextInput, Password as TextPassword } from "../../../../Antd";
import { CommonButton } from "../../../../UiElement";
import validation from "./validation";

function LoginForm({ onSubmit }) {
  const [showPassword, setShowPassword] = useState(false);

  const initialValues = {
    email: "",
    password: "",
  };

  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={validation()}
      onSubmit={onSubmit}
      enableReinitialize
    >
      {(props) => {
        return (
          <Form>
            <div className="form-group">
              <div className="form-label-group">
                <label className="form-label" htmlFor="email">
                  Email
                </label>
              </div>
              <div className="form-control-wrap">
                <TextInput
                  id="email"
                  className="form-control form-control-lg"
                  name="email"
                  disabled={false}
                  variant="standard"
                  type="email"
                  placeholder="Enter your email"
                  setFieldValue={props.handleChange}
                  icon={
                    <div className="form-icon form-icon-left">
                      <em className="icon ni ni-user" />
                    </div>
                  }
                />
              </div>
            </div>
            <div className="form-group">
              {/* <div className="form-label-group">
                <label className="form-label" htmlFor="password">
                  Password
                </label>
                <Link className="link link-primary link-sm" to="">
                  Forgot Password?
                </Link>
              </div> */}
              <div className="form-label-group">
                <label className="form-label" htmlFor="email">
                  Password
                </label>
              </div>
              <div className="form-control-wrap">
                <TextPassword
                  className="form-control form-control-lg"
                  name="password"
                  placeholder="Enter your password"
                  setFieldValue={props.handleChange}
                  type={showPassword ? "text" : "password"}
                  toggleIcon={
                    <Link
                      to="#"
                      onClick={(e) => {
                        e.preventDefault();
                        setShowPassword(!showPassword);
                      }}
                      className="form-icon form-icon-right passcode-switch"
                      data-target="password"
                    >
                      <em
                        className={`passcode-icon icon-show icon ni ni-eye${
                          showPassword ? "" : "-off"
                        } `}
                      />
                    </Link>
                  }
                  icon={
                    <div className="form-icon form-icon-left">
                      <em className="icon ni ni-lock" />
                    </div>
                  }
                />
              </div>
            </div>

            <div className="form-group">
              <CommonButton
                extraClassName="btn btn-lg btn-primary btn-block"
                // loading={loading}
                htmlType="submit"
                type="submit"
              >
                Login
              </CommonButton>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default LoginForm;

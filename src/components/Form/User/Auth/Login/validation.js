import * as yup from "yup";

export default function validation() {
  return yup.object().shape({
    password: yup.string().required("Please enter a Password"),
    email: yup
      .string()
      .required("Please enter a email")
      .email("Please enter a valid email address"),
  });
}

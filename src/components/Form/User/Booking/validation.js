import * as yup from "yup";

export default function validation() {
  return yup.object().shape({
    barcode: yup.string().required("Barcode is requierd"),
    mobileNumber: yup.string().required("Mobile number is requierd"),
    booking_amount: yup.string().required("Advance is requierd"),
    // payment_type: yup.string().required("Payment type is requierd"),
    // fk_customer_id: yup.string().required("Customer Name is requierd"),
  });
}

import { Formik, Form } from "formik";
import React, { useState } from "react";
import { Row } from "antd";
import { DatePicker, Input as TextInput } from "../../../Antd";
// import validation from "./validation";
import MultipleInput from "../../../UiElement/MultipleInput";
import { CommonButton } from "../../../UiElement";

function PaymentForm({ onSubmit, loading, rowData }) {
  const [payMode, setPayMode] = useState([]);

  const initialValues = {
    status: rowData?.cstatus || undefined,
    name:
      rowData?.charging_type ||
      rowData?.battery_type_name ||
      rowData?.network_name ||
      rowData?.os_name ||
      "",
  };

  const paymentMode = [
    {
      value: "Card",
      label: "Card",
    },
    {
      value: "UPI",
      label: "UPI",
    },
  ];

  return (
    <Formik
      initialValues={{ ...initialValues }}
      // validationSchema={validation()}
      onSubmit={onSubmit}
    >
      {(props) => {
        return (
          <Form>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Date
              </label>
              <div className="form-control-wrap">
                <DatePicker
                  name="fromDate"
                  className="form-control date-picker shadow-none"
                  placeholder="DD/MM/YY"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Customer Name
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter IMEI/SerialNo"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Perticular
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter IMEI/SerialNo"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group ">
              <label className="form-label" htmlFor="category-name">
                Payment Mode
              </label>
              <div className="form-control-wrap">
                <MultipleInput
                  showSearch
                  name="country"
                  options={paymentMode}
                  handleChangeSelect={(e) => setPayMode(e)}
                  extraClassName="text-start"
                  placeholder="Select Payment Mode"
                  id="country"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>

            {payMode?.length > 0 &&
              payMode?.map((item) => {
                return (
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      {item}
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="name"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter IMEI/SerialNo"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                );
              })}
            <Row gutter={16} className="d-flex justify-content-end">
              <CommonButton
                type="submit"
                htmlType="submit"
                className="btn btn-danger ripple-effect"
                loading={loading}
              >
                Given
              </CommonButton>
              <CommonButton
                type="submit"
                htmlType="submit"
                className="btn btn-success ripple-effect mx-3"
                loading={loading}
              >
                Received
              </CommonButton>
            </Row>
          </Form>
        );
      }}
    </Formik>
  );
}

export default PaymentForm;

import { Formik, Form } from "formik";
import React from "react";
import { AntTextArea, Input as TextInput } from "../../../Antd";
import validation from "./validation";
import { CommonButton } from "../../../UiElement";

function CustomerForm({ onSubmit, loading, rowData ,customerNumber}) {
  const initialValues = {
    status: rowData?.cstatus || undefined,
    name: rowData?.name || "",
    email: rowData?.email || "",
    mobile: customerNumber||rowData?.mobile || "",
    address: rowData?.address || "",
  };

  return (
    <>
      <Formik
        initialValues={{ ...initialValues }}
        validationSchema={validation()}
        onSubmit={onSubmit}
      >
        {(props) => {
          return (
            <Form>
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Customer Name
                </label>
                <div className="form-control-wrap">
                  <TextInput
                    className="form-control"
                    name="name"
                    disabled={false}
                    variant="standard"
                    type="text"
                    placeholder="Enter customer name"
                    setFieldValue={props.handleChange}
                  />
                </div>
              </div>
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Email
                </label>
                <div className="form-control-wrap">
                  <TextInput
                    className="form-control"
                    name="email"
                    disabled={false}
                    variant="standard"
                    type="text"
                    placeholder="Enter email"
                    setFieldValue={props.handleChange}
                  />
                </div>
              </div>
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Mobile Number
                </label>
                <div className="form-control-wrap">
                  <TextInput
                    className="form-control"
                    name="mobile"
                    disabled={customerNumber}
                    variant="standard"
                    type="text"
                    placeholder="Enter mobile number"
                    setFieldValue={props.handleChange}
                  />
                </div>
              </div>
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Address
                </label>
                <div className="form-control-wrap">
                  <AntTextArea
                    className="form-control"
                    name="address"
                    disabled={false}
                    variant="standard"
                    type="text"
                    placeholder="Enter address"
                    setFieldValue={props.handleChange}
                  />
                </div>
              </div>

              <div className="form-group text-center">
                <CommonButton
                  type="submit"
                  htmlType="submit"
                  className="btn btn-primary ripple-effect"
                  loading={loading}
                >
                  {rowData?.id ? "Update" : "Submit"}
                </CommonButton>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

export default CustomerForm;

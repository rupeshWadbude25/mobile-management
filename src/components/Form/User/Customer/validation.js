import * as yup from "yup";

export default function validation() {
  return yup.object().shape({
    name: yup.string().required("Name is requierd"),
  });
}

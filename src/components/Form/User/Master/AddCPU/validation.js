import * as yup from "yup";

export default function validation() {
  return yup.object().shape({
    status: yup.string().required("Please select status"),
    name: yup.string().required("Please enter cpu name"),
  });
}

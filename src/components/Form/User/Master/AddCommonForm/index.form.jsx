import { Formik, Form } from "formik";
import React from "react";
import { Select, Input as TextInput } from "../../../../Antd";
import { CommonButton } from "../../../../UiElement";
// import UploadInput from "../../../../Antd/Upload/index.ant";
// import validation from "./validation";
// import validation from "./validation";

function AddModalForm({
  onSubmit,
  loading,
  brand = false,
  modal = false,
  QCParameters = false,
  sensor = false,
  DeviceStatus = false,
  paymentForm = false,
  rowData,
  brandData,
  modelData,
}) {
  const initialValues = {
    status: rowData?.cstatus || undefined,
    name:
      rowData?.brand_name ||
      rowData?.model_name ||
      rowData?.qc_name ||
      rowData?.sensor_name ||
      rowData?.device_status ||
      rowData?.payment_type ||
      "",
    brand_id: rowData?.fk_brand_id || undefined,
    image: rowData?.image || "",
    variant_name: rowData?.variant_name || "",
    model_id: rowData?.fk_model_id,
  };

  const status = [
    {
      id: "active",
      name: "Active",
    },
    {
      id: "inActive",
      name: "In-Active",
    },
  ];
  return (
    <Formik
      initialValues={{ ...initialValues }}
      // validationSchema={validation()}
      onSubmit={onSubmit}
    >
      {(props) => {
        return (
          <Form>
            <div className="form-group">
              {!modal && !brand && (
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Variant Name
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="variant_name"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter variant name"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              )}
              {modal ? (
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Modal Name
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="name"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter modal name"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              ) : (
                !brand && (
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Modal Type
                    </label>
                    <div className="form-control-wrap">
                      <Select
                        showSearch
                        name="model_id"
                        disabled={false}
                        variant="standard"
                        setFieldValue={props.handleChange}
                        arrayOfData={modelData?.map((item) => {
                          if (item?.model_name) {
                            return {
                              id: item?.id,
                              name: item?.model_name,
                            };
                          }
                        })}
                        placeholder="Select model"
                      />
                    </div>
                  </div>
                )
              )}
              {brand ? (
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    {QCParameters
                      ? "QCParameters"
                      : sensor
                      ? "Sensor Name"
                      : DeviceStatus
                      ? "Device Name"
                      : paymentForm
                      ? "Payment Type"
                      : "Brand Name"}
                  </label>
                  <div className="form-control-wrap">
                    <TextInput
                      className="form-control"
                      name="name"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter name"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              ) : (
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Brand Type
                  </label>
                  <div className="form-control-wrap">
                    <Select
                      showSearch
                      name="brand_id"
                      disabled={false}
                      variant="standard"
                      setFieldValue={props.handleChange}
                      arrayOfData={brandData?.map((item) => {
                        return { id: item?.id, name: item?.brand_name };
                      })}
                      placeholder="Select brand"
                    />
                  </div>
                </div>
              )}
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Status
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={status}
                  placeholder="Select status"
                />
              </div>
            </div>

            {/* <div className="form-group">
              <label className="form-label">Image</label>
              <div className="form-group">
                <div className="upload_photo mb-2 mb-md-3 mx-auto text-center">
                  <div className="img-box">
                    <DocumentUploader
                      applyImageCropper={false}
                      name="profilePicture"
                      apiEndPoints=""
                      type="file"
                      setFieldValue={props.handleChange}
                    />
                  </div>
                </div>
              </div>
            </div> */}
            {/* {QCParameters || sensor || DeviceStatus ? (
              ""
            ) : (
              <div className="form-group">
                <label className="form-label">Image</label>
                <div className="form-group">
                  <div className="upload_photo mb-2 mb-md-3 mx-auto text-center">
                    <div className="img-box">
                      <UploadInput
                        name="image"
                        apiEndPoints=""
                        type="file"
                        defaultImageUrl="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAilBMVEUAAAD////u7u7t7e339/fv7+/+/v77+/vy8vKFhYW4uLj4+Ph1dXV8fHxCQkKZmZnNzc0yMjJXV1ff398LCwvn5+djY2OMjIzGxsZra2sSEhI6OjpSUlKgoKAlJSW+vr6srKwuLi5eXl5KSkoeHh4+Pj7T09NwcHCxsbGJiYmUlJSlpaUREREgICByJaxJAAANbUlEQVR4nO2df2OyLBfHRUBottVytn5sa6trrWu7r/f/9h41UUFFIFDr2fe/uTzxCZXD4XD0ABP1c1F2BM53s+V28+SNV0+b7XK2m0NAsuZDwtoeQobjtRCSMIEbuv3K2u5fcUgw1CC8WwzdaG0tThQoEUIc+rPHoZtrpLdvCLoJk1/h52Pophrr46eLENJgPuYHS7ee5lJCiMD9dQMmiPfthBCG8WHoBlrQS9zah2D1NnTrrOjfKiVk8igTAndDN82a7gANmby8L6EPXodul0W9FpcmKAjBauhWWdWdSAjD2wL0vBVPCFHc8pDZ7tfTyWj1d7petPhf/2KOkILGYeLt/Zh/juQKis4P2KHyiidah+yYCkB8fG3snZcqIQzu65/YTKLCFILMaa0Mn/mhYrIF2BAEw6IJxaHAkSkIcWIommzqAPclISQNrtoCAlprll9pFjfZ0ie0aCr9sWB9MpQ7cF72XXXAuwBBebMu7EPrpnY1hu2ZMBnrw5+mDkxUXPqo8AuKQ6WnUD8UsiNh/Txnphq68Se9U1OvzRenS8v8l/OZ54PKH5epMtdkqnRKrrJTejBFw6XA8ZF5bcn/ZyIgOxGzC6OwFRTXSn02XWlW/cbqwxQFIuIsudxTQmFEWYSdtsZJ6NNQuFA/KE4JRYcbKtgaJ6FPocByR5KrlAjcpUd3fYRQfKLuw6QPsXCNBr03y6opob8w9YTnzAai6yaEvHfzmowW/PNnAuB1EwLeAV0CD/KR7Yj2O4jZNxVxPFvozbkDbyAIiI/P8gMmhJlAfgRQdgSxDwF2Hg7ZoSFMAX6mMff4h8870HWXbfilNk1BIRqz8/gHzVG7WSOYW/Cm8JEjmnncg2Yb3wBhzPloS4970OyHapZFU5AfErceN3ysb4EwWFeRNh43+Z3eAKFPplUkYXI/uQnCv167ckKmiq1c1dBDbSguzqs0awBTZCIlRNcvICUk3C+Ja/5RYOpq9WlKTugP4y5bNfVL+Ev4S/hLODQhtNIsqN8si6akhEEyYabFjBnXJt9BMdMm7EjIPuSD2uR7EFNywvQnIPVxt/CP/MqPq+Vq9Wiqw2sDN+B5/xL+Ev7fEVbjsrdICGmIomh+N48iCBC+OcLk8R59s8jW5/0qH7KvgFB15AHRnjv9v7vz2Dbm8TDQSJIIgu8/ooF1BAZP3ej0S5VX14iYF5ApAo0JMpf6pTqmLM0tAtKSMT0f+dxCnbCxBxM9RTdBCMF3q5EtQTdASI+SXQvf5PoJIdhLrLzFWL9ZIyOkkcSI5z2zEduUsDzPRRSDiy4325LdhZnyMxVMNRFCGhUnNo0WXabkPk06kBZbFYrhtjx0/pN2bN6LiHhem6lsmBY+hMjhG7Sf12VKJ4rRusojv0jTnMCzDNeenr01W8Kwv/YElHzcLsJJ0axOU6B+r6XpIdH5bhtsbjGX2Eg1vYQw+/neifE0xQph1w6pNTEnDF9SCw9gWMJ61jivr8Cc8OwNbmI6KKHD+/CUm3hHoyZ8NiYstl8tyaCE8FNOuDMlpGXaHaQOCbvjBQ27iKoqm6UVxUBVd/dEnEQx+EyF1kxOJN/Lt2dxFs38Us4ZXPD0GqZseN4YSAlXtLjsOk1V/VLud3vLNpYNN3uqbSSq6MVwBhzzZk5DEvoYtQUxEsWhESEWAj8PgxLKZogzwzhN7elFBiWErWPixDDWJu6+SreeDZypsGoM1UwMm3Wsm1oHQ+diRA2I38CMEDdsWH6KnUQxdHLkwmfh/P15Y7RBul1jeHnnIHMPSMMLYiwBgWOVcb8KEFKMVPCmwHtjc16AvilKVaIYrM87N2Cnf+++79fr9fR5V1xS2nu5g5b5ZjKF0t8WbjsXA2KS7YtPD5ku+qGotoaV6x0Nvgbs29joQh7a2rMkIyC0sHArmaiUPus1E8qCPiegZWqchNJ4QbGt/IoJYetNmOot0jBlm7C+oGBEKJmjpFppmAJKo0W/pR6CBn+b1wtQNFU2IbARxYCWtraS7spGzEnrOYphZ1kT0rbKRhX9hCqmuFaNh5CCjqBkqvvgiglDWVOYNvmS+TUSSqNZpXbnp+gVEnYuDeRagGslpIpFfP9geqWEHQsDpU7IKmFv42FHNke1UYHN8RD0VOpBq4gh1WuV3KdJfwL3M2CIqE6l1LlWq0axowSHCkN9qaVeq3og9IvYQ4spon4TptrCsREi+gElhFC72O1uZIQ0PrCEmkZTdN5QxlGq2bgIKU3j18dWQgz+aQJmBRDHQ0jjzFdZtBLyhTnUZJ2QSb/UQwL4cjb13mLKqCDzaTxVI4LC23z0m0s6dKXENepep11uq0ZUirpPgF831VqvWK6P2NdolUvPm1YXySJU2wYCgcFNmOqoUyfPIWHMTYgWRCTE0i+X6V6n1qEzwuIhw7QjvKmuRCOJvsAICBESp7RvIWcKokh3qM+1PwZ0cEJIG94c8Qyqpmhg9mqeP+9pobmhCSER830yxRVTMJg2fKJbjzHBeq1yUjWC0sbi74tidMYQvBsBTgmyGokyrBpBacsosALMFJHtlGrXmtBxVI14aTG4Ifn4DKjRU2ZdXATDVo2g7Utkr+dTsYm/7XmHMtg07NxCsgb4BLNYoEZoraooHEGmAkSo7RLNtABp4KZr+0KzfsDQ+/GzI20PGaY5hWHcljAj1T7EImEyU+qbkAbSHkx0CGho9hqpORLCdrB8H0VvhBR3rMN76VzYbEJxAEJgEgbx6fPxcb0i/RHi1h3dFW0NX7O0QzwhDI75SxzW6FJC1ZGH0q5L9AI9iAUoKqn8/6ikVfaqRiT+g5mnqaYvwOdbBHHlNRxfpD0LxF7VCGA6YVfTKu+U/PsoH8KKKXQ/tyAue9DzUOWBlrWKi/DMiPuqEcAt4KZGyP373jkhNXM01bUMpIRT14S46R1YVpW/j2ooQuz2IZNqNighJq570CGhQtUIiNXzKMzFCMvRgvv3lBiOFipVI5D7S9RLCYUmCITAZdUIWc1za5qdU0hKr00kdFg14sstWq5ZthzZQVhpVXnbXkwo7nVypOEIewK8hJBrohBd6CLs5Sl6lkgYKBOG3CKs0OAuQsOQmYnM+xBzS0TCmrOcsMcevIQQVjvxS9hXLyXs7x5MZU6Y3Inv7GNr1EzYGMXwewXUGg9raw1hdI7unRIfhzcrzS/t8RL1Kl5bsTzC/Zvz2poKUODVLspSGHizMs/b7UPm6e3jsap/r9XLztfyvFOFFKGkj5NjyoQue/AwO82Ly5GXMSE7pEzYtT/JXI/PMSmaVXsC9EToOwR8rdw78qR4h4QOh4lJECin/TskdNeDx3SSPTihux5cwDyPo1dCKGbuYWc9uEdIGMTkWzcEQtV8QoGwXjXCGeALktS3pLVDtSiGal1MkRCIfqlprl2XDmGln/Lvk25saPdLxVoWvKkuQnK6c6O4Qljea0aE8tzsLkJmn6k4EtSONHxIckh7c4pbQgev9BsdoYqthmZZNPVL+EuoT6j0iFc21X1LK8e8BVMiYb9VI9h5qGV2yKlG2K3UukDYc9UIdt7d832n+NWSz0n3Gc9x2hKR0FdZP/TV3eVuUxDMjXJrVXSAaHhCGJjlLarpkdLBCSl2CJjW0hqcUPoGZgsSo/q9E+LGXQwWJUz+BiBsKi9pU0Ju4RCEbgE94WUq/Y+Hfty4FcWehFB9b1UjSlPOggi5hO1yQ9RNdHuZHsTRQp/w8rmFWqkhMz3Fo5g9uUP8jMYxPwTxz9fnQ5f4cfNP5+cfDtPsBQZjIExnYOeN5iSPcZJi63kR9gz5pn6BkITFp+rnJYfyp/0oCJkpVDdV1i/Vm+OXpkZF2F8UozUEaDGaqG3KVhRDozpDzxIJVc8TCC+tGtH6S1owxRMarj39HSCK4Xz98Ek47/YIuQ3I6/Szt0bIF+sAN0jIbSB8TIuF3hohH8c43iAhH618TS5TC82CRbMsmhII2/dbCIR8tO0NGFWNOB8CtXm8VVM8YaBoCnuQL6kSmVWNyNTgalk0pZNfynltwl7lye153oTfab2BN0corhssbo7QD4WY6Q6MklB9v0WNEJ14wvOrFUdHeEEfYvrBEy5CeluEsJabt8w/eCOE6eDxISKG9IbGwzTnrBaSXUBgPd/iUlMNe0iVTJ1filovFZcFUy91JnXKyHWb4gnV/dLs74ZsiKQbtV5qP965xflAw36RzSTxUTFO953Y6MPLf6zLCJsLrr29HuMskbT4Qr0cUlImqKqno6ZjlQvCuKVA83a/nv6d9KjvHQyxC0JgXFrUvp4rdU8tEmoXSneoFyd9aFaG2pHWJeHlo0W5xWFMvXgMahsolKtG8OdRr+ofrbTrwbvS31DHa5OvPbHOzGyJtY4H05pUWtVE+AXU5xY+Z+v+v4GYeC1onZBbf5gZE4Kj4xwlNU3z+VOVkPO7InNCAH7MSjZb1Tuq9aFfLaCZPWyNCQGeiRPG3gXqVyks6wh4/+KLCAkE9G7f/u09aMWGM24yTdg89vH8fpALCCEmIX5dDnW1/tk1B0RwEE8Pjw/7We7QX0CY/OVTEsD5brbcbhynuIr6nMVtIR+ISBhX3gWtRPg/kxCuQqGyrfcAAAAASUVORK5CYII="
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </div>
              </div>
            )} */}

            <div className="form-group text-center">
              <CommonButton
                type="submit"
                htmlType="submit"
                className="btn btn-primary ripple-effect"
                loading={loading}
              >
                Submit
              </CommonButton>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default AddModalForm;

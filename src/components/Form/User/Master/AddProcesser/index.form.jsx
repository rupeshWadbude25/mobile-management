import { Formik, Form } from "formik";
import React from "react";
import { Select, Input as TextInput } from "../../../../Antd";
import { CommonButton } from "../../../../UiElement";
import validation from "./validation";

function AddProcesserForm({ onSubmit, loading, rowData }) {
  const initialValues = {
    status: rowData?.cstatus || undefined,
    name: rowData?.processor_name || "",
  };

  const status = [
    {
      id: "active",
      name: "Active",
    },
    {
      id: "inActive",
      name: "In-Active",
    },
  ];
  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={validation()}
      onSubmit={onSubmit}
    >
      {(props) => {
        return (
          <Form>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Processer Name
              </label>

              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter processer name"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Status
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={status}
                  placeholder="Select status"
                />
              </div>
            </div>
            <div className="form-group text-center">
              <CommonButton
                type="submit"
                htmlType="submit"
                className="btn btn-primary ripple-effect"
                loading={loading}
              >
                Submit
              </CommonButton>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default AddProcesserForm;

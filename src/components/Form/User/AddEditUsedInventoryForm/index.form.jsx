import { Formik, Form } from "formik";
import React from "react";
import { AntTextArea, Select, Input as TextInput } from "../../../Antd";
import { CommonButton } from "../../../UiElement";
import validation from "./validation";

function AddEditUsedInventoryForm({ onSubmit, loading, rowData }) {
  const initialValues = {
    status: rowData?.cstatus || undefined,
    name:
      rowData?.charging_type ||
      rowData?.battery_type_name ||
      rowData?.network_name ||
      rowData?.os_name ||
      "",
  };

  return (
    <Formik
      initialValues={{ ...initialValues }}
      validationSchema={validation()}
      onSubmit={onSubmit}
    >
      {(props) => {
        return (
          <Form>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Barcode
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter barcode"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                IMEI/SerialNo.
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter IMEI/SerialNo"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                IMEI/SerialNo.
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter IMEI/SerialNo"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Purchase Price
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter Purchase Price"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Brand
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={[]}
                  placeholder="Select Brand"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Model
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={[]}
                  placeholder="Select Model"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Variant
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={[]}
                  placeholder="Select Variant"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Color
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={[]}
                  placeholder="Select Color"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Status
              </label>
              <div className="form-control-wrap">
                <Select
                  name="status"
                  disabled={false}
                  variant="standard"
                  setFieldValue={props.handleChange}
                  arrayOfData={[]}
                  placeholder="Select Status"
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Vendor Name / Code
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter Vendor Name / Code"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Vendor Mobile Number
              </label>
              <div className="form-control-wrap">
                <TextInput
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter Vendor Mobile Number"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="form-label" htmlFor="category-name">
                Remark
              </label>
              <div className="form-control-wrap">
                <AntTextArea
                  className="form-control"
                  name="name"
                  disabled={false}
                  variant="standard"
                  type="text"
                  placeholder="Enter Remark"
                  setFieldValue={props.handleChange}
                />
              </div>
            </div>
            <div className="form-group text-center">
              <CommonButton
                type="submit"
                htmlType="submit"
                className="btn btn-primary ripple-effect"
                loading={loading}
              >
                {rowData?.id ? "Update" : "Submit"}
              </CommonButton>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
}

export default AddEditUsedInventoryForm;

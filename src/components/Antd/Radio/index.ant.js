import React from "react";
import { Radio } from "antd";
// import { useField } from "formik";

function RadioComponent({
  radioGroupArray,
  onChange,
  className = "",
  name,
  onItemChange,
  ...rest
}) {
  // const [field, meta, helpers] = useField(name);
  // const config = { ...field, ...rest };

  // if (meta && meta?.touched && meta?.error) {
  //   config.error = true;
  //   config.helperText = meta.error;
  // }

  // const handleChangeSelect = ({ target }) => {
  //   // if (onChange) onChange();
  //   helpers.setValue(target?.value);
  //   // if (onItemChange) onItemChange(target?.value);
  // };

  return (
    // <Form.Item
    //   label={rest?.label}
    //   name={name}
    //   help={meta?.error && meta?.touched ? meta.error : ""}
    //   validateStatus={config?.error ? "error" : "success"}
    // >
    <Radio.Group
      // defaultValue={field?.value === false ? false : field?.value || null}
      className={className}
      options={radioGroupArray}
      // onChange={handleChangeSelect}
      {...rest}
    />
    // </Form.Item>
  );
}

export default RadioComponent;

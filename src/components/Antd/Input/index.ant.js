import { Form } from "antd";
import { useField } from "formik";

function Input({ name, icon, setFieldValue, setValues = false, ...rest }) {
  const [field, meta, helpers] = useField(name);
  const config = { ...field, ...rest };

  if (meta && meta.touched && meta.error) {
    config.error = true;
    config.helperText = meta.error;
  }

  const onChange = (e) => {
    helpers.setValue(e.target.value);
    if (setValues) {
      setValues(e.target.value);
    }
  };

  return (
    <Form.Item
      className="mb-0"
      label={rest?.label}
      help={meta.error && meta?.error && meta?.touched ? meta.error : ""}
      validateStatus={config.error ? "error" : "success"}
    >
      {icon}
      <input type="text" name={name} {...field} {...rest} onChange={onChange} />
    </Form.Item>
  );
}

export default Input;

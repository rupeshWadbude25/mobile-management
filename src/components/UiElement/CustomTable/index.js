/* eslint-disable no-shadow */
/* eslint-disable no-prototype-builtins */
/* eslint-disable no-unused-expressions */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import { Table } from "antd";
// import { useSelector } from "react-redux";
import { t } from "i18next";
import { AntTooltip } from "../../Antd";
import { CommonButton } from "../Button";
// import AntTooltip from "../Tooltip";
// import { getActions } from "../../../services/index.service";
// import { PermissionContext } from "../../../context/permission.context";

function CustomTable(props) {
  const {
    count,
    sizePerPage,
    tableData,
    tableColumns,
    currentPage,
    handleActionSelect,
    handleSelectedRows,
    header = true,
    showAction = true,
    disabledAction = [],
    isCard = true,
    selectRow = false,
    statusOptDisabled = true,
    showPageSizeAction = true,
    statusAction,
    statusActionOption,
    scrollTopPosition = false,
    searchPlaceholder = "",
    searchField = false,
    isModalPadding = false,
    isModalShadow = false,
    actions = [
      { label: "Make Active", id: "active" },
      { label: "Make Inactive", id: "inactive" },
      { label: "Make Delete", id: "delete" },
    ],
    tableTitle = false,
    // disableSelectRowCheckboxKeys,
    setSearchData = false,
    setPage = false,
    tableLoader = false,
    searchDisabled = false,
    setSortColumn,
    setSortOrder,
    submitBtn = false,
    pagination = true,
  } = props;
  const query = new URLSearchParams(useLocation()?.search);
  const [search, setSearch] = useState(query.get("search") || "");
  const [selected, setSelected] = useState([]);
  const [actionValue, setActionValue] = useState("");

  let divScroll = document.getElementById("scrollView");
  let table = document.querySelector(".ant-table table");
  let scrollWraper = document.getElementById("scrollWraper");
  let tableWraper = document.querySelector(".ant-table-content");

  setTimeout(() => {
    if (divScroll) {
      divScroll.style.width = `${table.scrollWidth}px`;
    }
  }, 500);
  if (scrollWraper && tableWraper) {
    scrollWraper.onscroll = function () {
      tableWraper.scrollLeft = scrollWraper.scrollLeft;
    };
    tableWraper.onscroll = function () {
      scrollWraper.scrollLeft = tableWraper.scrollLeft;
    };
  }

  useEffect(() => {
    if (setSearchData) {
      setSearchData(search);
    }
  }, [search]);

  const handleSearchValue = (val) => {
    setSearch(val);
  };

  function onTableCheckboxChange(e) {
    // console.log("E select 1")
    try {
      handleSelectedRows(e);
      setActionValue("-");
    } catch (err) {
      console.log(err);
    }
  }

  async function onTableActionChange(e) {
    // console.log("E select 2")
    try {
      e.preventDefault();
      if (e?.target?.value === "") {
        return true;
      }
      await handleActionSelect(e);
      setActionValue("-");
    } catch (err) {
      console.log(err);
    }
  }

  const onTableSelectChange = (rows) => {
    // console.log("E select 3")
    try {
      setSelected([...rows]);
      onTableCheckboxChange(rows);
    } catch (err) {
      console.log(err);
    }
  };

  //   const disableRowCheckboxByCondition = (record) => {
  //     let isDisable = false;
  //     typeof disableSelectRowCheckboxKeys === "object" &&
  //       Object.keys(disableSelectRowCheckboxKeys).forEach((key) => {
  //         if (record?.[key] === disableSelectRowCheckboxKeys?.[key]) {
  //           isDisable = true;
  //         }
  //       });
  //     return isDisable;
  //   };

  const rowSelection = {
    mode: "checkbox",
    clickToSelect: false,
    onChange: onTableSelectChange,
    selectedRowKeys: selected,
    // getCheckboxProps: (record) => {
    //   return {
    //     disabled:
    //       record.isAssigned === 1
    //         ? true
    //         : !isUserCanEdit
    //         ? true
    //         : disableSelectRowCheckboxKeys
    //         ? disableRowCheckboxByCondition(record)
    //         : false,
    //   };
    // },
  };

  const handleSelect = (e) => {
    setPage({
      currentPage,
      sizePerPage: e.target.value,
    });
  };

  const handleChange = (pagination, filters, sorter) => {
    if (sorter.hasOwnProperty("column")) {
      if (sorter?.order) {
        setSortColumn(sorter?.column?.sortType || sorter?.field);
        setSortOrder(sorter?.order);
      }
      setSortColumn(sorter?.field);
      setSortOrder(sorter?.order);
    }
  };
  return (
    <div className="nk-block">
      <div
        className={`${isCard ? "card" : ""} position-static ${
          isModalShadow ? "shadow-none" : ""
        }`}
      >
        <div className={`card-inner ${isModalPadding ? "p-0" : ""}`}>
          {tableTitle && <h5 className="title mb-4">{tableTitle}</h5>}
          <div className="common-table">
            <div className="dataTables_wrapper dt-bootstrap4 no-footer">
              {header && (
                <Row className="justify-between g-2 mb-3">
                  <Col sm="4" md={6} className="text-start">
                    <div
                      id="DataTables_Table_0_filter"
                      className="dataTables_filter"
                    >
                      {searchPlaceholder ? (
                        <AntTooltip
                          placement="topLeft"
                          promptText={`Search by ${searchPlaceholder}`}
                        >
                          <label>
                            <input
                              type="search"
                              className="form-control form-control-md"
                              placeholder={t("text.common.charactersLimit")}
                              aria-controls="DataTables_Table_0"
                              disabled={!searchDisabled}
                              onChange={(e) =>
                                handleSearchValue(e.target.value)
                              }
                              value={search}
                            />
                          </label>
                        </AntTooltip>
                      ) : (
                        searchField && (
                          <label>
                            <input
                              type="search"
                              className="form-control form-control-md"
                              placeholder="Search..."
                              aria-controls="DataTables_Table_0"
                              onChange={(e) =>
                                handleSearchValue(e.target.value)
                              }
                              value={search}
                            />
                          </label>
                        )
                      )}
                    </div>
                  </Col>

                  <Col sm="8" md={6} className="text-end">
                    <div className="d-flex justify-content-sm-end filterButtons">
                      {selectRow && (
                        <>
                          {showAction && (
                            <div className="datatable-filter">
                              <div
                                className="dataTables_length"
                                id="DataTables_Table_0_length"
                              >
                                <div className="form-control-select">
                                  <select
                                    value={actionValue}
                                    className="custom-select custom-select-sm form-control form-control-sm"
                                    onChange={onTableActionChange}
                                    style={
                                      Array.isArray(selected) &&
                                      selected.length > 0
                                        ? {}
                                        : { pointerEvents: "none" }
                                    }
                                  >
                                    <option value="">Select Action</option>
                                    {statusOptDisabled && (
                                      <>
                                        {actions
                                          .filter(
                                            (e) =>
                                              !disabledAction.includes(e.id)
                                          )
                                          .map((e) => {
                                            return (
                                              <option key={e.id} value={e.id}>
                                                {e.label}
                                              </option>
                                            );
                                          })}
                                      </>
                                    )}
                                  </select>
                                </div>
                              </div>
                            </div>
                          )}
                          {statusAction && (
                            <div className="datatable-filter status">
                              <div
                                className="dataTables_length"
                                id="DataTables_Table_0_length"
                              >
                                <div className="form-control-select">
                                  <select
                                    className="custom-select custom-select-sm form-control form-control-sm"
                                    onChange={handleSelect}
                                  >
                                    {statusActionOption.map(
                                      (option, optionIdx) => (
                                        <option
                                          key={optionIdx}
                                          value={option.name}
                                        >
                                          {option.name}
                                        </option>
                                      )
                                    )}
                                  </select>
                                </div>
                              </div>
                            </div>
                          )}
                          {showPageSizeAction && (
                            <div className="datatable-filter">
                              <div
                                className="dataTables_length"
                                id="DataTables_Table_0_length"
                              >
                                <div className="form-control-select">
                                  <select
                                    value={sizePerPage || "10"}
                                    name="DataTables_Table_0_length"
                                    aria-controls="DataTables_Table_0"
                                    className="custom-select custom-select-sm form-control form-control-sm"
                                    onChange={handleSelect}
                                  >
                                    <option value="10">10</option>
                                    <option value="25">25</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                          )}
                        </>
                      )}
                    </div>
                  </Col>
                </Row>
              )}
              {scrollTopPosition && (
                <div id="scrollWraper" className="topScroll">
                  <div id="scrollView" style={{ height: "10px" }} />
                </div>
              )}

              <Table
                className="newCustomTable"
                dataSource={tableData}
                columns={tableColumns}
                loading={tableLoader}
                rowSelection={
                  selectRow ? showAction && rowSelection : selectRow
                }
                onChange={handleChange}
                rowKey="id"
                pagination={
                  pagination && {
                    pageSizeOptions: ["10", "25", "50", "100"],
                    showSizeChanger: true,
                    defaultPageSize: sizePerPage,
                    total: count,
                    showTotal: (count, range) =>
                      `${range[0]}-${range[1]} of ${count} items`,
                    current: currentPage,
                    onChange: (pageNo, pageSize) => {
                      setPage({
                        currentPage: pageNo,
                        sizePerPage: pageSize,
                      });
                    },
                  }
                }
              />
              {submitBtn && (
                <div className="d-flex justify-content-end">
                  <CommonButton
                    className="btn  btn-primary mt-4 ms-4"
                    type="primary"
                    htmlType="submit"
                    //   loading={loading}
                  >
                    Submit
                  </CommonButton>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CustomTable;

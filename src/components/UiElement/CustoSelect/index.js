import React from "react";
import { Form, Select as AntSelect } from "antd";
import { useField } from "formik";

const { Option } = AntSelect;

export default function CustomSelect({
  name,
  icon,
  setFieldValue,
  arrayOfData = [],
  onSelectChange,
  extraClassName,
  placeholder,
  defaultValue,
  amount = false,
  showSearch = true,
  bankType,
  amountWithMonth = false,
  ...rest
}) {
  const [field, meta, helpers] = useField(name);

  const config = { ...field, ...rest };

  if (meta && meta.touched && meta.error) {
    config.error = true;
    config.helperText = meta.error;
  }

  const handleChangeSelect = (value) => {
    helpers.setValue(value);
    helpers.setError("");
    helpers.setTouched(false);
  };

  const onSearch = () => {
    // console.log("search:", value);
  };
  return (
    <>
      <Form.Item
        label={rest?.label}
        help={meta.error && meta?.error && meta?.touched ? meta.error : ""}
        validateStatus={config.error ? "error" : "success"}
      >
        <AntSelect
          showSearch={showSearch}
          size="large"
          {...field}
          {...rest}
          onChange={handleChangeSelect}
          onSearch={onSearch}
          placeholder={placeholder}
          defaultValue={defaultValue}
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().includes(input.toLowerCase())
          }
        >
          {arrayOfData
            ?.filter((value) => value?.name !== "" && value)
            ?.map((item) => (
              <Option
                key={item?.id || item?.name}
                disabled={item?.disabled || false}
                value={bankType ? item?.type : item?.id}
              >
                {amount
                  ? amountWithMonth
                    ? `${item?.LoanProduct?.name} - ${item?.LoanProduct?.amount} - ${item.months}`
                    : `${item?.name} - ${item.amount}`
                  : item?.name || `${item?.firstName}${" "}${item?.lastName}`}
              </Option>
            ))}
        </AntSelect>
      </Form.Item>
    </>
  );
}

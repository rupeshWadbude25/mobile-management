import React from "react";

export function GeneralText({
  label,
  value,
  currency = "",
  extraClassName = "",
}) {
  return (
    <div className={`${extraClassName}`}>
      <div className="profile-ud wider">
        <span className="profile-ud-label">
          <span>{label}</span> :
        </span>
        <span className="profile-ud-value text-left text-break">
          {currency} {value}
        </span>
      </div>
    </div>
  );
}

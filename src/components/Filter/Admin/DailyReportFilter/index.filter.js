// import {Formik, Form } from "formik";
// import moment from "moment";
import React from "react"; // , { useState }
import { Link } from "react-router-dom";
import { Form, Formik } from "formik";
import { CommonButton, DatePicker } from "../../../index";
// import { DatePicker, CommonButton, Select } from "../../..";
// import { classicDateFormat } from "../../../../helpers";
// import { disabledFutureDate } from "../../../../utils";

function DailyReportFilter() {
  // const onHandleReset = (resetForm) => {
  //   resetForm({});
  //   setDisableReset(true);
  //   if (
  //     filterData?.registeredFrom ||
  //     filterData?.registeredTo ||
  //     filterData?.status ||
  //     filterData?.countryId ||
  //     filterData?.stateId ||
  //     filterData?.cityId
  //   )
  //     onReset();
  // };

  return (
    // <Formik
    //   initialValues={{ ...initialValues }}
    //   onSubmit={disableSubmit && onSubmit}
    //   enableReinitialize
    //   validate={(e) => {
    //     if (
    //       e.registeredFrom ||
    //       e.registeredTo ||
    //       e.status ||
    //       e.countryId ||
    //       e.cityId
    //     ) {
    //       setDisableReset(false);
    //       setDisableSubmit(true);
    //     } else {
    //       setDisableReset(true);
    //       setDisableSubmit(false);
    //     }
    //   }}
    // >
    //   {(props) => {
    //     const { values } = props;
    // return (

    <Formik
      initialValues={{}}
      // validationSchema={validation()}
      // onSubmit={onSubmit}
    >
      {() => {
        return (
          <Form>
            <div className="dropdown-body dropdown-body-rg">
              <div className="row g-3">
                <div className="col-12">
                  <div className="form-group ">
                    <label className="overline-title overline-title-alt">
                      Start Date
                    </label>
                    <div className="form-control-wrap ">
                      <DatePicker
                        name="fromDate"
                        className="form-control date-picker shadow-none"
                        placeholder="DD/MM/YY"
                      />
                    </div>
                  </div>
                </div>
                <div className="col-12">
                  <div className="form-group ">
                    <label className="overline-title overline-title-alt">
                      End Date
                    </label>
                    <div className="form-control-wrap ">
                      <DatePicker
                        name="fromDate"
                        className="form-control date-picker shadow-none"
                        placeholder="DD/MM/YY"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="dropdown-foot between">
              <Link
                to="#"
                onClick={(e) => {
                  e.preventDefault();
                  // if (!disableReset) {
                  //   onHandleReset(props.resetForm);
                  // }
                }}
                type="button"
                className="clickable"
              >
                Reset
              </Link>
              <CommonButton
                extraClassName="btn btn-sm btn-primary"
                htmlType="submit"
                // loading={loading}
                type="submit"
              >
                Filter
              </CommonButton>
            </div>
          </Form>
        );
      }}
    </Formik>
    // );
    //   }}
    // </Formik>
  );
}

export default DailyReportFilter;

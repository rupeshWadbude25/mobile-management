// import { DesktopOutlined } from "@ant-design/icons";

import { baseRoutes } from "../../../helpers/baseRoutes";

const accessRoute = {
  REPORT: {
    path: `${baseRoutes.adminBaseRoutes}report`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-files-fill" />
      </span>
    ),
  },
};

export default accessRoute;

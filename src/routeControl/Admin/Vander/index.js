// import { DesktopOutlined } from "@ant-design/icons";

import { baseRoutes } from "../../../helpers/baseRoutes";

const accessRoute = {
  VANDER: {
    path: `${baseRoutes.adminBaseRoutes}vander`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-user" />
      </span>
    ),
  },
};

export default accessRoute;

// import { DesktopOutlined } from "@ant-design/icons";

import { baseRoutes } from "../../../helpers/baseRoutes";

const accessRoute = {
  USERS: {
    path: `${baseRoutes.adminBaseRoutes}`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-mobile" />
      </span>
    ),
  },
  CUSTOMER: {
    path: `${baseRoutes.adminBaseRoutes}customer`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-user" />
      </span>
    ),
  },
};

export default accessRoute;

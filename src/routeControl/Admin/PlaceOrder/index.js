// import { DesktopOutlined } from "@ant-design/icons";

import { baseRoutes } from "../../../helpers/baseRoutes";

const accessRoute = {
  PLACE_ORDER: {
    path: `${baseRoutes.adminBaseRoutes}place-order`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-package-fill" />
      </span>
    ),
  },
  ORDER_DETAILS: {
    path: `${baseRoutes.adminBaseRoutes}order-details`,
  },
};

export default accessRoute;

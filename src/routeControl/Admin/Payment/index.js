// import { DesktopOutlined } from "@ant-design/icons";

import { baseRoutes } from "../../../helpers/baseRoutes";

const accessRoute = {
  PAYMENT: {
    path: `${baseRoutes.adminBaseRoutes}payment`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-money" />
      </span>
    ),
  },
};

export default accessRoute;

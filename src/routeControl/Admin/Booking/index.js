// import { DesktopOutlined } from "@ant-design/icons";

import { baseRoutes } from "../../../helpers/baseRoutes";

const accessRoute = {
  BOOKING: {
    path: `${baseRoutes.adminBaseRoutes}booking`,
    icon: (
      <span className="nk-menu-icon">
        <em className="icon ni ni-package-fill" />
      </span>
    ),
  },
  BOOKING_DETAILS: {
    path: `${baseRoutes.adminBaseRoutes}booking-details`,
  },
};

export default accessRoute;

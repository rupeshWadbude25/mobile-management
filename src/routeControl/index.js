import adminRoutes from "./Admin";

const moduleRoutesMap = {
  admin: { ...adminRoutes },
};
export default moduleRoutesMap;

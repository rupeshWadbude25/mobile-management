import { Customer } from "../../../pages";
import adminRouteMap from "../../../routeControl/adminRouteMap";

export default function route() {
  return [
    {
      path: adminRouteMap.CUSTOMER.path,
      name: "Customer",
      key: adminRouteMap.CUSTOMER.path,
      private: false,
      belongsToSidebar: true,
      icon: adminRouteMap.CUSTOMER.icon,
      element: <Customer />,
    },
  ];
}

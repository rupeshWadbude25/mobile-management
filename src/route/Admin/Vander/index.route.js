import { VanderList } from "../../../pages";
import adminRouteMap from "../../../routeControl/adminRouteMap";

export default function route() {
  return [
    {
      path: adminRouteMap.VANDER.path,
      name: "Vander",
      key: adminRouteMap.VANDER.path,
      private: false,
      belongsToSidebar: true,
      icon: adminRouteMap.VANDER.icon,
      element: <VanderList />,
    },
  ];
}

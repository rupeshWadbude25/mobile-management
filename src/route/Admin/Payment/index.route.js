import { Payment } from "../../../pages";
import adminRouteMap from "../../../routeControl/adminRouteMap";

export default function route() {
  return [
    {
      path: adminRouteMap.PAYMENT.path,
      name: "Payment",
      key: adminRouteMap.PAYMENT.path,
      private: false,
      belongsToSidebar: true,
      icon: adminRouteMap.PAYMENT.icon,
      element: <Payment />,
    },
  ];
}

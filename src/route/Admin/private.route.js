import DashboardRoutes from "./Dashboard/index.route";
import masterRoutes from "./Master/index.route";
import adminAccountRoutes from "./AdminAccount/index.route";
import addDevice from "./AddDevice/index.route";
import inventory from "./Inventory/index.route";
import invoice from "./Invoice/index.route";
// import payment from "./Payment/index.route";
import booking from "./Booking/index.route";
import report from "./Report/index.route";
import users from "./User/index";
import placeOrder from "./PlaceOrder/index.route";

export default function route() {
  return [
    ...DashboardRoutes(),
    ...masterRoutes(),
    ...adminAccountRoutes(),
    ...addDevice(),
    ...inventory(),
    ...booking(),
    ...placeOrder(),
    ...users(),
    ...invoice(),
    // ...payment(),
    ...report(),
  ];
}

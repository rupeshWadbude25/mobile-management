/* eslint-disable no-unused-vars */
import axios from "axios";
import config from "../config";

const { BASEURL } = config;
const client = axios.create({});

client.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    const errorResponse = JSON.parse(JSON.stringify(error));
    return Promise.reject(error);
  }
);

const axiosClient = (
  endpoint,
  payload = {},
  method = "get",
  headers = {
    "Content-Type": "text/plain",
  }
) => {
  let axiosConfig = {
    method: method.toLowerCase(),
  };
  // if (endpoint !== "login" && endpoint !== "verifyOtp") {
  axiosConfig.headers = headers;
  // }
  if (method === "get") {
    payload.user_id = "1";
    axiosConfig.params = payload;
  } else {
    payload.user_id = "1";
    axiosConfig.data = payload;
  }

  return client(`${BASEURL}${endpoint}`, axiosConfig);
};
export default axiosClient;

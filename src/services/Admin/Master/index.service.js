import axiosClient from "../../axios";

export const loginService = (payload) => {
  return axiosClient("user_login", payload, "POST");
};

export const getBrandList = (payload) => {
  let obj = { ...payload };
  if (payload?.list_type) {
    obj.list_type = payload?.list_type;
  } else {
    obj.list_type = "";
  }
  return axiosClient("brand_list", (payload = obj), "POST");
};
export const addBrandService = (payload) => {
  return axiosClient("add_brand", payload, "POST");
};
export const updateBrandStatusService = (payload) => {
  return axiosClient("update_brand", payload, "POST");
};
export const addColorService = (payload) => {
  return axiosClient("add_color", payload, "POST");
};
export const getColorList = (payload) => {
  return axiosClient("color_list", payload, "POST");
};
export const updateColorService = (payload) => {
  return axiosClient("update_color", payload, "POST");
};
export const addCPUService = (payload) => {
  return axiosClient("add_cpu", payload, "POST");
};
export const getCPUList = (payload) => {
  return axiosClient("cpu_list", payload, "POST");
};
export const updateCPUService = (payload) => {
  return axiosClient("update_cpu", payload, "POST");
};
export const addDisplayService = (payload) => {
  return axiosClient("add_display", payload, "POST");
};
export const getDisplayList = (payload) => {
  return axiosClient("display_list", payload, "POST");
};
export const updateDisplayService = (payload) => {
  return axiosClient("update_display", payload, "POST");
};
export const addBatteryChargingService = (payload) => {
  return axiosClient("add_batterycharging", payload, "POST");
};
export const getBatteryChargingList = (payload) => {
  return axiosClient("batterycharging_list", payload, "POST");
};
export const updateBatteryChargingService = (payload) => {
  return axiosClient("update_batterycharging", payload, "POST");
};
export const addBatteryTypeService = (payload) => {
  return axiosClient("add_batterytype", payload, "POST");
};
export const getBatteryTypeList = (payload) => {
  return axiosClient("batterytype_list", payload, "POST");
};
export const updateBatteryTypeService = (payload) => {
  return axiosClient("update_batterytype", payload, "POST");
};
export const addNetworkService = (payload) => {
  return axiosClient("add_network", payload, "POST");
};
export const getNetworkList = (payload) => {
  return axiosClient("network_list", payload, "POST");
};
export const updateNetworkService = (payload) => {
  return axiosClient("update_network", payload, "POST");
};
export const addOSService = (payload) => {
  return axiosClient("add_os", payload, "POST");
};
export const getOSList = (payload) => {
  return axiosClient("os_list", payload, "POST");
};
export const updateOSService = (payload) => {
  return axiosClient("update_os", payload, "POST");
};
export const addProcesserService = (payload) => {
  return axiosClient("add_processor", payload, "POST");
};
export const getProcesserList = (payload) => {
  return axiosClient("processor_list", payload, "POST");
};
export const updateProcesserService = (payload) => {
  return axiosClient("update_processor", payload, "POST");
};
export const addSimService = (payload) => {
  return axiosClient("add_sim", payload, "POST");
};
export const getSimList = (payload) => {
  return axiosClient("sim_list", payload, "POST");
};
export const updateSimService = (payload) => {
  return axiosClient("update_sim", payload, "POST");
};
export const addRamService = (payload) => {
  return axiosClient("add_ram", payload, "POST");
};
export const getRamList = (payload) => {
  return axiosClient("ram_list", payload, "POST");
};
export const updateRamService = (payload) => {
  return axiosClient("update_ram", payload, "POST");
};
export const addVideoTypeService = (payload) => {
  return axiosClient("add_video", payload, "POST");
};
export const getVideoTypeList = (payload) => {
  return axiosClient("video_type_list", payload, "POST");
};
export const updateVideoTypeService = (payload) => {
  return axiosClient("update_video_type", payload, "POST");
};
export const addVariantService = (payload) => {
  return axiosClient("add_variant", payload, "POST");
};
export const getVariantList = (payload) => {
  return axiosClient("variant_list", payload, "POST");
};
export const updateVariantService = (payload) => {
  return axiosClient("update_variant", payload, "POST");
};
export const addModalService = (payload) => {
  return axiosClient("add_model", payload, "POST");
};
export const getModalList = (payload) => {
  let obj = { ...payload };
  if (payload?.list_type) {
    obj.list_type = payload?.list_type;
  }
  return axiosClient("model_list", (payload = obj), "POST");
};
export const updateModalService = (payload) => {
  return axiosClient("update_model", payload, "POST");
};
export const addMasterQCService = (payload) => {
  return axiosClient("add_qc_parameter", payload, "POST");
};
export const getMasterQCList = (payload) => {
  return axiosClient("qc_parameter_list", payload, "POST");
};
export const updateMasterQCService = (payload) => {
  return axiosClient("update_qc_parameter", payload, "POST");
};
export const addSensorService = (payload) => {
  return axiosClient("add_sensor", payload, "POST");
};
export const getSensorList = (payload) => {
  return axiosClient("sensor_list", payload, "POST");
};
export const updateSensorService = (payload) => {
  return axiosClient("update_sensor", payload, "POST");
};
export const addDeviceService = (payload) => {
  return axiosClient("add_new_device", payload, "POST");
};
export const addQCPerametersService = (payload) => {
  return axiosClient("add_qc_param_device", payload, "POST");
};
export const getDeviceService = (payload) => {
  return axiosClient("device_list", payload, "POST");
};
export const addVanderService = (payload) => {
  return axiosClient("add_user", payload, "POST");
};

export const getVanderService = (payload) => {
  return axiosClient("user_list", payload, "POST");
};
export const updateVanderService = (payload) => {
  return axiosClient("update_user", payload, "POST");
};
export const deviceColorListService = (payload) => {
  return axiosClient("device_color_list", payload, "POST");
};
export const newInventoryService = (payload) => {
  return axiosClient("add_new_device_inventory", payload, "POST");
};
export const updateInventoryService = (payload) => {
  return axiosClient("update_new_device_inventory", payload, "POST");
};
export const getnewInventoryListService = (payload) => {
  return axiosClient("device_inventory_list", payload, "POST");
};
export const getQCParamsListService = (payload) => {
  return axiosClient("device_qc_param_list", payload, "POST");
};
export const getQCParamsDetailsService = (payload) => {
  return axiosClient("device_inventory_details", payload, "POST");
};
export const updateQCParamService = (payload) => {
  return axiosClient("new_device_qc_update", payload, "POST");
};
export const getDeviceStatusList = (payload) => {
  return axiosClient("device_status_list", payload, "POST");
};
export const addDeviceStatusService = (payload) => {
  return axiosClient("add_device_status", payload, "POST");
};
export const updateDeviceStatusService = (payload) => {
  return axiosClient("update_device_status", payload, "POST");
};
export const deviceBookingService = (payload) => {
  return axiosClient("book_device", payload, "POST");
};
export const getPaymentTypeList = (payload) => {
  return axiosClient("payment_type_list", payload, "POST");
};
export const addPaymentTypeService = (payload) => {
  return axiosClient("add_payment_type", payload, "POST");
};
export const updatePaymentTypeService = (payload) => {
  return axiosClient("update_payment_type", payload, "POST");
};

export const getBookingListService = (payload) => {
  return axiosClient("booking_list", payload, "POST");
};
export const deleteBookingService = (payload) => {
  return axiosClient("booking_delete", payload, "POST");
};
export const getBarcodeService = (payload) => {
  return axiosClient("generate_device_barcode", payload, "POST");
};
export const getBookingDetailsService = (payload) => {
  return axiosClient("booking_details", payload, "POST");
};
export const placeOrderService = (payload) => {
  return axiosClient("place_order", payload, "POST");
};

export const getOrderListService = (payload) => {
  return axiosClient("order_list", payload, "POST");
};

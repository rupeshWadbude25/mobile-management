import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  Breadcrumb,
  DataTable,
  PageHeader,
  actionFormatter,
} from "../../../components";
import adminRouteMap from "../../../routeControl/adminRouteMap";
import {
  checkValidData,
  decodeQueryData,
  logger,
  textFormatter,
} from "../../../utils";
import { getDeviceService } from "../../../services/Admin/Master/index.service";

function DeviceList() {
  const location = useLocation();
  const { search } = location;
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [searchName, setSearchName] = useState("");

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getDeviceData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
        search: searchName,
      };

      const res = await getDeviceService(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.mob_devices);
        setNoOfPage(
          res?.data?.data?.length > 0
            ? Math.ceil(res?.data?.data.total_devices / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_devices);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  // async function deleteBrand() {
  //   try {
  //     let payload = {
  //       brand_name: rowData?.name,
  //       status: rowData?.status,
  //       image: "",
  //       type: "2",
  //       brand_id: rowData?.id,
  //     };
  //     const res = await updateBrandStatusService(payload);
  //     if (res?.data?.status === "1") {
  //       modalNotification({
  //         type: "success",
  //         message: res?.data?.message,
  //       });
  //       getBrandData();
  //     }
  //   } catch (error) {
  //     logger(error);
  //   }
  // }

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const getSearchValue = (val) => {
    setSearchName(val);
    if (val) {
      tableReset();
    }
  };
  const options = () => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "redirect",
        path: adminRouteMap.EDIT_DEVICE.path,
      },
      // {
      //   name: "Delete",
      //   icon: "icon ni ni-trash",
      //   action: "confirm",
      //   onClickHandle: () => {
      //     // setRowData(row);
      //     // setIsAlertVisibleDelete(true);
      //     document.body.click();
      //   },
      // },
    ];

    return optionsArr;
  };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Device List",
    },
  ];

  const columns = [
    // {
    //   dataField: "id",
    //   text: "S.No.",
    //   headerClasses: "w_70",
    // },
    {
      dataField: "device_name",
      text: "Device Name",
      // headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(textFormatter(row?.device_name)),
    },
    {
      dataField: "brand_name",
      text: "Brand Name",
      // headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(textFormatter(row?.brand_name)),
    },
    {
      dataField: "model_name",
      text: "Model Name",
      // headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(textFormatter(row?.model_name)),
    },
    {
      dataField: "variant_name",
      text: "Variant Name",
      // headerClasses: "sorting",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.variant_name)),
    },
    {
      dataField: "cstatus",
      text: "Status",
      // headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(textFormatter(row?.cstatus)),
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getDeviceData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getDeviceData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Device List">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
        </div>
      </div>
      <DataTable
        // header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        getSearchValue={getSearchValue}
        searchPlaceholder="Device Name"
      />
    </>
  );
}

export default DeviceList;

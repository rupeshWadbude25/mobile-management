import React, { useEffect, useState } from "react";
import { Row, Col } from "react-bootstrap";
import { useLocation } from "react-router-dom";
import { Form, Formik } from "formik";
// import { Input } from "antd";
// import { useSelector } from "react-redux";
// import { Input } from "antd";
import {
  Breadcrumb,
  PageHeader,
  // Input as TextInput,
} from "../../../components";
import CustomSelect from "../../../components/UiElement/CustoSelect/index";
import Tabs from "../../../components/UiElement/Tabs";
import Specification from "./specification";
import QCParameters from "./qcParameters";
import { logger } from "../../../utils";
import {
  getBrandList,
  getModalList,
  getVariantList,
} from "../../../services/Admin/Master/index.service";
// import { getDeviceData } from "../../../redux/AuthSlice/index.slice";
// import validation from "./Specification/validation";

function AddDevice() {
  const location = useLocation();
  // const deviceData = useSelector(getDeviceData);
  const { pathname } = location;
  const [brandLoading, setBrandLoading] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [variantLoading, setVariantLoading] = useState(false);
  const [brandData, setBrandData] = useState([]);
  const [modalData, setModalData] = useState([]);
  const [variantData, setVariantData] = useState([]);
  const [parentTab, setParentTab] = useState("specification");
  const [initialValues, setInitialValue] = useState({
    brand_id: undefined,
    model_id: undefined,
    variant_id: undefined,
    device_name: "",
  });

  // const initialValues = {
  //   brand_id: undefined,
  //   model_id: undefined,
  //   variant_id: undefined,
  // };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: pathname === "/edit-device" ? "Edit Device" : "Add Device",
    },
  ];

  const getBrandData = async () => {
    setBrandLoading(true);
    try {
      let payload = {
        list_type: "2",
      };
      const res = await getBrandList(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.brand_list?.map((item) => {
          return {
            name: item?.brand_name,
            id: item?.id,
          };
        });
        setBrandData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setBrandLoading(false);
  };

  const getModalData = async (props, id) => {
    setModalLoading(true);
    props?.setFieldValue("model_id", undefined);
    props?.setFieldValue("variant_id", undefined);
    try {
      let payload = {
        brand_id: id,
      };
      const res = await getModalList(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.model_list?.map((item) => {
          return {
            name: item?.model_name,
            id: item?.id,
          };
        });
        setModalData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setModalLoading(false);
  };

  const getVariantData = async (props, id) => {
    setVariantLoading(true);
    props?.setFieldValue("variant_id", undefined);
    try {
      let payload = {
        model_id: id,
      };
      const res = await getVariantList(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.variant_list?.map((item) => {
          return {
            name: item?.variant_name,
            id: item?.id,
          };
        });
        setVariantData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setVariantLoading(false);
  };

  const onSubmit = async (value) => {
    try {
      setInitialValue({
        ...initialValues,
        ...value,
      });
    } catch (error) {
      logger(error);
    }
  };

  const tabContent = [
    parentTab === "specification"
      ? {
          name: "Specification",
          key: "specification",
          content: (
            <div className="tab-pane active">
              <Specification
                initialValues={initialValues}
                setParentTab={setParentTab}
                deviceFormSubmit={onSubmit}
              />
            </div>
          ),
        }
      : {
          name: "QC Parameters",
          key: "qcParameters",
          content: (
            <div className="tab-pane ">
              <QCParameters
                setParentTab={setParentTab}
                setInitialValue={setInitialValue}
                initialValues={initialValues}
              />
            </div>
          ),
        },
  ];

  useEffect(() => {
    getBrandData();
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader
            heading={pathname === "/edit-device" ? "Edit Device" : "Add Device"}
          >
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
        </div>
      </div>
      <Formik
        initialValues={{ ...initialValues }}
        // validationSchema={validation(1)}
        onSubmit={onSubmit}
        enableReinitialize
      >
        {(props) => {
          return (
            <Form>
              <div className="dropdown-body dropdown-body-rg">
                <div className="row g-3">
                  <Row className="g-2 g-xxl-4">
                    <Col lg={4}>
                      <div className="form-group ">
                        <label className="overline-title overline-title-alt">
                          Brand
                        </label>
                        <CustomSelect
                          showSearch
                          loading={brandLoading}
                          id="brand_id"
                          value={initialValues?.brand_id}
                          extraClassName="form-control form-control-lg"
                          name="brand_id"
                          disabled={false}
                          placeholder="Select Brand"
                          arrayOfData={brandData}
                          onSelect={(e) => {
                            getModalData(props, e);
                            setInitialValue({
                              ...initialValues,
                              brand_id: e,
                            });
                          }}
                          setFieldValue={props.handleChange}
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <label className="overline-title overline-title-alt">
                          Model
                        </label>
                        <CustomSelect
                          showSearch
                          loading={modalLoading}
                          id="model_id"
                          value={initialValues?.model_id}
                          extraClassName="form-control form-control-lg"
                          name="model_id"
                          disabled={false}
                          placeholder="Select Model"
                          arrayOfData={modalData}
                          setFieldValue={props.handleChange}
                          onSelect={(e) => {
                            getVariantData(props, e);
                            setInitialValue({
                              ...initialValues,
                              model_id: e,
                            });
                          }}
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <label className="overline-title overline-title-alt">
                          Variant
                        </label>
                        <CustomSelect
                          showSearch
                          loading={variantLoading}
                          id="variant_id"
                          value={initialValues?.variant_id}
                          extraClassName="form-control form-control-lg"
                          name="variant_id"
                          disabled={false}
                          placeholder="Select Variant"
                          arrayOfData={variantData}
                          setFieldValue={props.handleChange}
                          onSelect={(e) =>
                            setInitialValue({
                              ...initialValues,
                              variant_id: e,
                            })
                          }
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                    {/* <Col lg={3}>
                    <CommonButton
                      extraClassName="btn  btn-primary mt-4 ms-4"
                      // loading={loading}
                      htmlType="submit"
                      type="submit"
                    >
                      Add Device
                    </CommonButton>
                  </Col> */}
                  </Row>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
      <Tabs
        tabContent={tabContent}
        tabsFor="table"
        border
        activeKey={parentTab}
        setActiveKey={setParentTab}
      />
    </>
  );
}

export default AddDevice;

import React from "react";
import Tabs from "../../../components/UiElement/Tabs";
import Functions from "./QCParameters/function";

function QCParameters({ setParentTab, setInitialValue, initialValues }) {
  const tabContent = [
    {
      name: "Functions",
      key: "Product Overview",
      content: (
        <div className="tab-pane active">
          <Functions
            setParentTab={setParentTab}
            setInitialValue={setInitialValue}
            initialValues={initialValues}
          />
        </div>
      ),
    },
    // {
    //   name: "Screen Condition",
    //   key: "Feature",
    //   content: <div className="tab-pane ">sfsaf</div>,
    // },
    // {
    //   name: "Body Conditions",
    //   key: "Camera",
    //   content: <div className="tab-pane ">sfsaf</div>,
    // },
    // {
    //   name: "Warrenty",
    //   key: "Battery",
    //   content: <div className="tab-pane ">sfsaf</div>,
    // },
    // {
    //   name: "Asessories",
    //   key: "General",
    //   content: <div className="tab-pane ">sfsaf</div>,
    // },
  ];
  return (
    <>
      <Tabs
        tabContent={tabContent}
        tabsFor="table"
        border
        // activeKey={defaultKey}
        // setActiveKey={setDefaultKey}
      />
    </>
  );
}

export default QCParameters;

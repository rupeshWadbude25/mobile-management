import React, { useEffect, useState } from "react";
import { Checkbox, Form } from "antd";
import { useLocation } from "react-router-dom";
import { textFormatter } from "../../../../components";
import {
  addQCPerametersService,
  getMasterQCList,
} from "../../../../services/Admin/Master/index.service";
import {
  checkValidData,
  decodeQueryData,
  logger,
  modalNotification,
} from "../../../../utils";
import CustomTable from "../../../../components/UiElement/CustomTable";

function Functions({ setParentTab, setInitialValue, initialValues }) {
  const deviceID = localStorage.getItem("deviceID");
  const location = useLocation();
  const { search } = location;
  const [selectAll, setSelectAll] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  // const [sizePerPage, setSizePerView] = useState(10);
  // const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [loading, setLoading] = useState(false);
  const [state, setState] = useState({
    selectedRows: [],
  });
  const [page, setPage] = useState({
    currentPage: 1,
    sizePerPage: 10,
  });
  const { currentPage, sizePerPage } = page;
  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getQCData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: currentPage,
      };

      const res = await getMasterQCList(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.qc_parameter_list);
        // setNoOfPage(
        //   res?.data?.data?.total_qc_parameter > 0
        //     ? Math.ceil(res?.data?.data.total_qc_parameter / sizePerPage)
        //     : 1
        // );

        setPage({
          currentPage: Math.ceil(
            res?.data?.data?.qc_parameter_list.length / sizePerPage
          ),
          sizePerPage: 10,
        });
        setTotalCount(res?.data?.data?.total_qc_parameter);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const onCheckAllChange = (e) => {
    // setLoading(true);
    if (e?.target?.checked) {
      if (e?.target.value === "all") {
        setLoading(true);
        setSelectAll(true);
        const result = state?.selectedRows?.map((item) => {
          return {
            id: item?.id,
            qc_name: item?.qc_name,
            checked: true,
            cstatus: item?.cstatus,
          };
        });
        setTableData(result);
        setTimeout(() => {
          setLoading(false);
        }, 0);
      } else {
        // console.log("tableData", tableData);
        const result = state?.selectedRows?.map((item) => {
          if (item?.qc_name === e?.target.value) {
            return {
              id: item?.id,
              qc_name: item?.qc_name,
              checked: true,
              cstatus: item?.cstatus,
            };
          } else {
            return item;
          }
        });
        setTableData(result);
      }
    } else if (e?.target.value === "all") {
      setLoading(true);
      setSelectAll(false);
      const result = state?.selectedRows?.map((item) => {
        return {
          id: item?.id,
          qc_name: item?.qc_name,
          checked: false,
          cstatus: item?.cstatus,
        };
      });
      setTableData(result);
      setTimeout(() => {
        setLoading(false);
      }, 0);
    } else {
      setSelectAll(false);
      const result = state?.selectedRows?.map((item) => {
        if (item?.qc_name === e?.target.value) {
          return {
            id: item?.id,
            qc_name: item?.qc_name,
            checked: false,
            cstatus: item?.cstatus,
          };
        } else {
          return item;
        }
      });
      setTableData(result);
    }
    // setTimeout(() => {
    //   setLoading(false);
    // }, 0);
  };

  const columns = [
    {
      dataIndex: "name",
      title: "Parameter Name",
      // headerClasses: "sorting",
      render: (cell, row) => checkValidData(textFormatter(row?.qc_name)),
    },
    {
      dataIndex: "cstatus",
      title: "Status",
      // headerClasses: "sorting",
      render: (cell, row) => checkValidData(textFormatter(row?.cstatus)),
    },
    {
      dataField: "selectAll",
      text: (
        <>
          SelectAll{" "}
          <Checkbox
            value="all"
            defaultChecked={selectAll}
            className="ms-1"
            onChange={onCheckAllChange}
          />
        </>
      ),
      // headerClasses: "sorting",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => (
        <div className="d-flex justify-content-end">
          <Checkbox
            defaultChecked={row?.checked}
            value={row?.qc_name}
            // className="ms-4"
            onChange={onCheckAllChange}
          />
        </div>
      ),
    },
  ];

  const onFinish = async () => {
    try {
      let payload = {
        device_id: deviceID?.toString(),
        qc_params: state?.selectedRows,
      };
      const res = await addQCPerametersService(payload);
      if (res?.data?.status === "1") {
        setInitialValue({
          ...initialValues,
          brand_id: undefined,
          model_id: undefined,
          variant_id: undefined,
          device_name: "",
        });
        setParentTab("specification");
        localStorage.setItem("deviceID", null);
        modalNotification({
          type: "success",
          message: "Add new device successfully",
        });
      }
    } catch (error) {
      logger(error);
    }
  };

  function onRowSelect(e) {
    try {
      setState({ ...state, selectedRows: [...e] });
    } catch (err) {
      console.log("err", err);
    }
  }
  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getQCData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getQCData();
    }
  }, []);
  return (
    <>
      {loading ? (
        ""
      ) : (
        <Form onFinish={onFinish}>
          <CustomTable
            header={false}
            hasLimit
            selectRow
            submitBtn
            noOfPage={noOfPage}
            sizePerPage={sizePerPage}
            page={page}
            count={totalCount}
            setPage={setPage}
            tableData={tableData}
            tableColumns={columns}
            // setSizePerPage={setSizePerView}
            tableLoader={tableLoading}
            tableReset={tableReset}
            handleSelectedRows={onRowSelect}
          />
          {/* <div className="text-end">
            <CommonButton
              className="btn btn-success"
              type="primary"
              htmlType="submit"
              loading={loading}
            >
              Submit
            </CommonButton>
          </div> */}
        </Form>
      )}
    </>
  );
}

export default Functions;

import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { Input } from "antd";
import { Form, Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { logger } from "../../../../utils";
import { CommonButton, MultiSelect } from "../../../../components";
import {
  getDeviceData,
  performanceData,
  updatePerformanceData,
} from "../../../../redux/AuthSlice/index.slice";
import validation from "./validation";
import {
  getColorList,
  getNetworkList,
  getProcesserList,
  getSensorList,
  getSimList,
} from "../../../../services/Admin/Master/index.service";
import CustomSelect from "../../../../components/UiElement/CustoSelect";

function Feature({ setDefaultKey, defaultKey }) {
  const dispatch = useDispatch();
  const deviceData = useSelector(getDeviceData);
  const [sensorData, setSensorsData] = useState([]);
  const [colorData, setColorData] = useState([]);
  const [networkData, setNetworkData] = useState([]);
  const [simData, setSIMData] = useState([]);
  const [processorData, setProcessorData] = useState([]);
  const initialValues = {
    sensors: undefined,
    color: undefined,
    sim: undefined,
    network: undefined,
    processor: undefined,
  };

  const [state, setState] = useState(initialValues);
  const getData = async () => {
    try {
      const res = await getSensorList();
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.sensor_list?.map((item) => {
          return {
            label: item?.sensor_name,
            value: item?.id,
          };
        });
        setSensorsData(arr);
      }
      const colorRes = await getColorList();
      if (colorRes?.data?.status === "1") {
        let arr = colorRes?.data?.data?.color_list?.map((item) => {
          return {
            label: item?.color_name,
            value: item?.id,
          };
        });
        setColorData(arr);
      }
      const networkRes = await getNetworkList();
      if (networkRes?.data?.status === "1") {
        let arr = networkRes?.data?.data?.network_list?.map((item) => {
          return {
            label: item?.network_name,
            value: item?.id,
          };
        });
        setNetworkData(arr);
      }
      const simRes = await getSimList();
      if (simRes?.data?.status === "1") {
        let arr = simRes?.data?.data?.sim_list?.map((item) => {
          return {
            label: item?.sim_name,
            value: item?.id,
          };
        });
        setSIMData(arr);
      }
      const processorRes = await getProcesserList();
      if (processorRes?.data?.status === "1") {
        let arr = processorRes?.data?.data?.processor_list?.map((item) => {
          return {
            name: item?.processor_name,
            id: item?.id,
          };
        });
        setProcessorData(arr);
      }
    } catch (error) {
      logger(error);
    }
  };

  const onSumbit = async (value) => {
    try {
      let payload = {
        id: 2,
        feature: { ...value },
      };
      if (deviceData?.length && deviceData[1]?.id === 2) {
        dispatch(
          updatePerformanceData({ id: deviceData[1]?.id, feature: payload })
        );
      } else {
        dispatch(performanceData(payload));
      }
      setDefaultKey("camera");
    } catch (error) {
      logger(error);
    }
  };

  useEffect(() => {
    if (deviceData?.length) {
      let values = deviceData[0]?.performance;

      setState({
        ...state,
        sensors: values?.sensors || undefined,
        color: values?.color || undefined,
        sim: values?.sim || undefined,
        network: values?.network || undefined,
        processor: values?.processor || undefined,
      });
    }
  }, [deviceData]);
  useEffect(() => {
    if (defaultKey === "feature") {
      getData();
      console.log("uesrrrrr", deviceData);

      if (deviceData) {
        if (!deviceData[0]?.performance) {
          setDefaultKey("performance");
        }
      } else {
        setDefaultKey("performance");
      }
    }
  }, [defaultKey]);

  return (
    <>
      <Formik
        initialValues={{ ...state }}
        validationSchema={validation(2)}
        onSubmit={onSumbit}
        enableReinitialize
      >
        {(props) => {
          return (
            <Form>
              <div className="dropdown-body dropdown-body-rg">
                <div className="row g-3">
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Sensors"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <MultiSelect
                          // loading={modalLoading}
                          id="sensors"
                          extraClassName="form-control form-control-lg"
                          name="sensors"
                          disabled={false}
                          placeholder="Select Sensors"
                          options={sensorData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Color"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <MultiSelect
                          // loading={modalLoading}
                          id="color"
                          extraClassName="form-control form-control-lg"
                          name="color"
                          disabled={false}
                          placeholder="Select Color"
                          options={colorData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Network"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <MultiSelect
                          // loading={modalLoading}
                          id="network"
                          extraClassName="form-control form-control-lg"
                          name="network"
                          disabled={false}
                          placeholder="Select Network"
                          options={networkData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="SIM"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <MultiSelect
                          // loading={modalLoading}
                          id="sim"
                          extraClassName="form-control form-control-lg"
                          name="sim"
                          disabled={false}
                          placeholder="Select Sensors"
                          options={simData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Processor"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          // loading={modalLoading}
                          id="processor"
                          extraClassName="form-control form-control-lg"
                          name="processor"
                          disabled={false}
                          placeholder="Select Processor"
                          arrayOfData={processorData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <div className="form-group d-flex justify-content-center">
                    <CommonButton
                      extraClassName="btn  btn-primary mt-4 ms-4"
                      // loading={loading}
                      htmlType="submit"
                      type="submit"
                    >
                      Next
                    </CommonButton>
                  </div>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

export default Feature;

import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { Input } from "antd";
import { Form, Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { CommonButton } from "../../../../components";
import { logger, modalNotification } from "../../../../utils";
import CustomSelect from "../../../../components/UiElement/CustoSelect";
import {
  getDeviceData,
  performanceData,
  resetPerformanceData,
  updatePerformanceData,
} from "../../../../redux/AuthSlice/index.slice";
import validation from "./validation";
import {
  addDeviceService,
  getBatteryChargingList,
  getBatteryTypeList,
} from "../../../../services/Admin/Master/index.service";

function Battery({ setDefaultKey, defaultKey, formValues, setParentTab }) {
  const dispatch = useDispatch();
  const deviceData = useSelector(getDeviceData);
  const initialValues = {
    batteryType: undefined,
    batteryCharging: undefined,
  };
  const [state, setState] = useState(initialValues);
  const [batteryTypeData, setBatteryTypeData] = useState([]);
  const [batteryChargingData, setBatteryChargingData] = useState([]);
  const [allFormValue, setAllFormValue] = useState({});

  const getData = async () => {
    try {
      const res = await getBatteryTypeList();
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.batterytype_list?.map((item) => {
          return {
            name: item?.battery_type_name,
            id: item?.id,
          };
        });
        setBatteryTypeData(arr);
      }
      const ramRes = await getBatteryChargingList();
      if (ramRes?.data?.status === "1") {
        let arr = ramRes?.data?.data?.batterycharging_list?.map((item) => {
          return {
            name: item?.charging_type,
            id: item?.id,
          };
        });
        setBatteryChargingData(arr);
      }
    } catch (error) {
      logger(error);
    }
  };

  const onSumbit = async (value) => {
    try {
      // if (formValues?.device_name === "") {
      //   modalNotification({
      //     type: "error",
      //     message: "Please Enter device name",
      //   });
      //   return;
      // }
      if (formValues?.brand_id === "") {
        modalNotification({
          type: "error",
          message: "Please select brand",
        });
        return;
      }
      if (formValues?.model_id === "") {
        modalNotification({
          type: "error",
          message: "Please select model",
        });
        return;
      }
      if (formValues?.variant_id === "") {
        modalNotification({
          type: "error",
          message: "Please select variant",
        });
        return;
      }

      let formPayload = {
        id: 4,
        battery: { ...value },
      };
      if (deviceData?.length && deviceData[3]?.id === 4) {
        dispatch(
          updatePerformanceData({ id: deviceData[1]?.id, feature: formPayload })
        );
      } else {
        dispatch(performanceData(formPayload));
      }

      let payload = {
        ...formValues,
        ...value,
        ...allFormValue,
        general: [{}],
        status: "active",
      };

      const res = await addDeviceService(payload);

      if (res?.data?.status === "1") {
        setParentTab("qcParameters");
        dispatch(resetPerformanceData());
        localStorage.setItem("deviceID", res?.data?.data[0]?.id);
      }
      // setDefaultKey("general");
    } catch (error) {
      logger(error);
    }
  };

  useEffect(() => {
    if (defaultKey === "battery") {
      if (deviceData) {
        if (!deviceData[0]?.performance) {
          setDefaultKey("performance");
        }
        if (!deviceData[1]?.feature) {
          setDefaultKey("feature");
        }
        if (!deviceData[2]?.camera) {
          setDefaultKey("camera");
        }
        if (!deviceData[3]?.battery) {
          setDefaultKey("battery");
        }

        const result = deviceData?.map((item) => {
          if (item?.performance) {
            return item?.performance;
          }
          if (item?.feature) {
            return item?.feature;
          }
          if (item?.camera) {
            return item?.camera;
          }
          if (item?.battery) {
            return item?.battery;
          }
        });
        let tempObj = {
          ...result[0],
          ...result[1],
          ...result[2],
          ...result[3],
        };
        console.log("tempObj", tempObj);

        setAllFormValue(tempObj);
      } else {
        setDefaultKey("performance");
      }
    }
  }, [defaultKey]);

  useEffect(() => {
    if (deviceData?.length) {
      let values = deviceData[0]?.performance;

      setState({
        ...state,
        primary_camera: values?.batteryType || undefined,
        secondry_camera: values?.batteryCharging || undefined,
      });
    }
  }, [deviceData]);
  useEffect(() => {
    if (defaultKey === "battery") {
      getData();
      if (deviceData) {
        if (!deviceData[0]?.performance) {
          setDefaultKey("performance");
        }
        if (!deviceData[1]?.feature) {
          setDefaultKey("feature");
        }
        if (!deviceData[2]?.camera) {
          setDefaultKey("camera");
        }
      } else {
        setDefaultKey("performance");
      }
    }
  }, [defaultKey]);
  return (
    <>
      <Formik
        initialValues={{ ...state }}
        validationSchema={validation(4)}
        onSubmit={onSumbit}
        enableReinitialize
      >
        {(props) => {
          return (
            <Form>
              <div className="dropdown-body dropdown-body-rg">
                <div className="row g-3">
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          id="email"
                          className="form-control form-control-lg "
                          name="email"
                          disabled
                          floatingLabel={false}
                          type="email"
                          placeholder="Battery Type"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          id="batteryType"
                          extraClassName="form-control form-control-lg"
                          name="batteryType"
                          disabled={false}
                          placeholder="Select Battery Type"
                          arrayOfData={batteryTypeData}
                          setFieldValue={props?.handleChange}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          id="email"
                          className="form-control form-control-lg "
                          name="email"
                          disabled
                          floatingLabel={false}
                          type="email"
                          placeholder="Battery Charging"
                          // setFieldValue={props.handleChange}
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          // loading={modalLoading}
                          id="batteryCharging"
                          extraClassName="form-control form-control-lg"
                          name="batteryCharging"
                          disabled={false}
                          placeholder="Select Battery Charging"
                          arrayOfData={batteryChargingData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className="form-group d-flex justify-content-center">
                <CommonButton
                  extraClassName="btn  btn-primary mt-4 ms-4"
                  // loading={loading}
                  htmlType="submit"
                  type="submit"
                >
                  Submit
                </CommonButton>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

export default Battery;

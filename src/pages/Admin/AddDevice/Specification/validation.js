import * as yup from "yup";

export default function validation(step) {
  return step === 1
    ? yup.object().shape({
        cpu: yup.string().required("CPU is required"),
        ram: yup.string().required("RAM is required"),
        storage: yup.string().required("Storage is required"),
        displaySize: yup.string().required("Display Size is required"),
        displayType: yup.string().required("Display Type is required"),
        opratingSystem: yup.string().required("Operating System is required"),
        // brand_id: yup.string().required("Brand is required"),
        // model_id: yup.string().required("Modal is required"),
        // variant_id: yup.string().required("Variant is required"),
        // device_name: yup.string().required("Device name is required"),
      })
    : step === 2
    ? yup.object().shape({
        sensors: yup.array().required("Sensors is required"),
        color: yup.array().required("Color is required"),
        network: yup.array().required("Network is required"),
        sim: yup.array().required("SIM is required"),
        processor: yup.string().required("Processor is required"),
      })
    : step === 3
    ? yup.object().shape({
        primary_camera: yup.string().required("Primary Camera is required"),
        secondry_camera: yup.string().required("Secondry Camera is required"),
      })
    : step === 4
    ? yup.object().shape({
        batteryType: yup.string().required("Battery Type is required"),
        batteryCharging: yup.string().required("Battery Charging is required"),
      })
    : null;
}

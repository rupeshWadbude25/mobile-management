import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { Input } from "antd";
import { Form, Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { enterOnlyNumericValue, logger } from "../../../../utils";
import { CommonButton, Input as TextInput } from "../../../../components";
import {
  getDeviceData,
  performanceData,
  updatePerformanceData,
} from "../../../../redux/AuthSlice/index.slice";
import validation from "./validation";

function Camera({ setDefaultKey, defaultKey }) {
  const dispatch = useDispatch();
  const deviceData = useSelector(getDeviceData);
  const initialValues = {
    primary_camera: "",
    secondry_camera: "",
  };

  const [state, setState] = useState(initialValues);
  const onSumbit = async (value) => {
    try {
      let payload = {
        id: 3,
        camera: { ...value },
      };
      if (deviceData?.length && deviceData[2]?.id === 3) {
        dispatch(
          updatePerformanceData({ id: deviceData[2]?.id, camera: payload })
        );
      } else {
        dispatch(performanceData(payload));
      }
      setDefaultKey("battery");
    } catch (error) {
      logger(error);
    }
  };

  useEffect(() => {
    if (deviceData?.length) {
      let values = deviceData[0]?.performance;

      setState({
        ...state,
        primary_camera: values?.primary_camera || "",
        secondry_camera: values?.secondry_camera || "",
      });
    }
  }, [deviceData]);

  useEffect(() => {
    if (defaultKey === "camera") {
      if (deviceData) {
        if (!deviceData[0]?.performance) {
          setDefaultKey("performance");
        }
        if (!deviceData[1]?.feature) {
          setDefaultKey("feature");
        }
      } else {
        setDefaultKey("performance");
      }
    }
  }, [defaultKey]);
  return (
    <>
      <Formik
        initialValues={{ ...state }}
        validationSchema={validation(3)}
        onSubmit={onSumbit}
        enableReinitialize
      >
        {(props) => {
          return (
            <Form>
              <div className="dropdown-body dropdown-body-rg">
                <div className="row g-3">
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Primary Camera"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <TextInput
                          id="primary_camera"
                          className="form-control form-control-lg "
                          name="primary_camera"
                          disabled={false}
                          floatingLabel={false}
                          type="text"
                          placeholder="Enter Primary Camera"
                          setFieldValue={props.handleChange}
                          min="0"
                          inputmode="numeric"
                          onKeyPress={(e) => {
                            // onCheckValue(e);
                            enterOnlyNumericValue(e);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Secondary Camera"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <TextInput
                          id="secondry_camera"
                          className="form-control form-control-lg "
                          name="secondry_camera"
                          disabled={false}
                          floatingLabel={false}
                          type="text"
                          placeholder="Enter Secondary Camera"
                          setFieldValue={props.handleChange}
                          min="0"
                          inputmode="numeric"
                          onKeyPress={(e) => {
                            // onCheckValue(e);
                            enterOnlyNumericValue(e);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                </div>
              </div>
              <div className="form-group d-flex justify-content-center">
                <CommonButton
                  extraClassName="btn  btn-primary mt-4 ms-4"
                  // loading={loading}
                  htmlType="submit"
                  type="submit"
                >
                  Next
                </CommonButton>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

export default Camera;

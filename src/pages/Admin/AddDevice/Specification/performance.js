import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { Form, Formik } from "formik";
import { CommonButton, Input as TextInput } from "../../../../components";
import {
  getCPUList,
  getDisplayList,
  getOSList,
  getRamList,
} from "../../../../services/Admin/Master/index.service";
import { enterOnlyNumericValue, logger } from "../../../../utils";
import CustomSelect from "../../../../components/UiElement/CustoSelect";
import {
  getDeviceData,
  performanceData,
  updatePerformanceData,
} from "../../../../redux/AuthSlice/index.slice";
import validation from "./validation";

function Performance({ setDefaultKey, defaultKey, deviceFormSubmit }) {
  const dispatch = useDispatch();
  const deviceData = useSelector(getDeviceData);

  const initialValues = {
    cpu: undefined,
    ram: undefined,
    storage: undefined,
    displayType: undefined,
    displaySize: undefined,
    opratingSystem: undefined,
  };
  const [state, setState] = useState(initialValues);
  const [cpuData, setCPUData] = useState([]);
  const [ramData, setRAMData] = useState([]);
  const [displayData, setDisplayData] = useState([]);
  const [osData, setOSData] = useState([]);

  const getData = async () => {
    try {
      const res = await getCPUList();
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.cpu_list?.map((item) => {
          return {
            name: item?.cpu_name,
            id: item?.id,
          };
        });
        setCPUData(arr);
      }
      const ramRes = await getRamList();
      if (ramRes?.data?.status === "1") {
        let arr = ramRes?.data?.data?.ram_list?.map((item) => {
          return {
            name: item?.ram_name,
            id: item?.id,
          };
        });
        setRAMData(arr);
      }
      const displayRes = await getDisplayList();
      if (displayRes?.data?.status === "1") {
        let arr = displayRes?.data?.data?.display_list?.map((item) => {
          return {
            name: item?.display_name,
            id: item?.id,
          };
        });
        setDisplayData(arr);
      }
      const OSRes = await getOSList();
      if (OSRes?.data?.status === "1") {
        let arr = OSRes?.data?.data?.os_list?.map((item) => {
          return {
            name: item?.os_name,
            id: item?.id,
          };
        });
        setOSData(arr);
      }
    } catch (error) {
      logger(error);
    }
  };

  const onSumbit = async (value) => {
    // console.log("value", value);
    deviceFormSubmit();
    try {
      let payload = {
        id: 1,
        performance: { ...value },
      };
      deviceFormSubmit();
      if (deviceData?.length > 0) {
        dispatch(
          updatePerformanceData({
            id: deviceData[0]?.id,
            performance: { ...value },
          })
        );
      } else {
        // console.log("heree", payload);

        dispatch(performanceData(payload));
      }
      setDefaultKey("feature");
    } catch (error) {
      logger(error);
    }
  };

  useEffect(() => {
    if (deviceData?.length) {
      let values = deviceData[0]?.performance;

      setState({
        ...state,
        cpu: values?.cpu || undefined,
        ram: values?.ram || undefined,
        storage: values?.storage || undefined,
        displayType: values?.displayType || undefined,
        displaySize: values?.displaySize || undefined,
        opratingSystem: values?.opratingSystem || undefined,
      });
    }
  }, [deviceData]);

  useEffect(() => {
    if (defaultKey === "performance") {
      getData();
    }
  }, [defaultKey]);

  return (
    <>
      <Formik
        initialValues={{ ...state }}
        validationSchema={validation(1)}
        onSubmit={onSumbit}
        enableReinitialize
      >
        {(props) => {
          return (
            <Form>
              <div className="dropdown-body dropdown-body-rg">
                <div className="row g-3">
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Chipset/CPU"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          // loading={modalLoading}
                          showSearch
                          id="cpu"
                          extraClassName="form-control form-control-lg"
                          name="cpu"
                          disabled={false}
                          placeholder="Select Chipset/CPU"
                          arrayOfData={cpuData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="RAM"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          showSearch
                          // loading={modalLoading}
                          id="ram"
                          extraClassName="form-control form-control-lg"
                          name="ram"
                          disabled={false}
                          placeholder="Select RAM"
                          arrayOfData={ramData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Storage"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <TextInput
                          id="storage"
                          className="form-control form-control-lg "
                          name="storage"
                          disabled={false}
                          floatingLabel={false}
                          type="text"
                          placeholder="Enter Storage"
                          setFieldValue={props.handleChange}
                          min="0"
                          inputmode="numeric"
                          onKeyPress={(e) => {
                            // onCheckValue(e);
                            enterOnlyNumericValue(e);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Display Type"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          // loading={modalLoading}
                          id="displayType"
                          extraClassName="form-control form-control-lg"
                          name="displayType"
                          disabled={false}
                          placeholder="Select Display Type"
                          arrayOfData={displayData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Display Size"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <TextInput
                          id="displaySize"
                          className="form-control form-control-lg "
                          name="displaySize"
                          disabled={false}
                          floatingLabel={false}
                          type="text"
                          placeholder="Enter Display Size"
                          setFieldValue={props.handleChange}
                          min="0"
                          inputmode="numeric"
                          onKeyPress={(e) => {
                            // onCheckValue(e);
                            enterOnlyNumericValue(e);
                          }}
                        />
                      </div>
                    </Col>
                  </Row>
                  <Row className="g-2 g-xxl-4">
                    <Col lg={3}>
                      <div className="form-group ">
                        <Input
                          className="form-control form-control-lg "
                          disabled
                          placeholder="Oprating System"
                        />
                      </div>
                    </Col>
                    <Col lg={4}>
                      <div className="form-group ">
                        <CustomSelect
                          // loading={modalLoading}
                          id="opratingSystem"
                          extraClassName="form-control form-control-lg"
                          name="opratingSystem"
                          disabled={false}
                          placeholder="Select Oprating System"
                          arrayOfData={osData}
                          setFieldValue={props?.handleChange}
                          // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
                        />
                      </div>
                    </Col>
                  </Row>
                  <div className="form-group d-flex justify-content-center">
                    <CommonButton
                      extraClassName="btn  btn-primary mt-4 ms-4"
                      // loading={loading}
                      htmlType="submit"
                      type="submit"
                    >
                      Next
                    </CommonButton>
                  </div>
                </div>
              </div>
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

export default Performance;

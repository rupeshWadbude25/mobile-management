import React, { useEffect, useState } from "react";
import { Col, Row } from "react-bootstrap";
import { Input, Form } from "antd";
// import { Form, Formik } from "formik";
import { useDispatch, useSelector } from "react-redux";
import { CommonButton } from "../../../../components";
import { logger } from "../../../../utils";
import {
  getDeviceData,
  resetPerformanceData,
} from "../../../../redux/AuthSlice/index.slice";
import { addDeviceService } from "../../../../services/Admin/Master/index.service";

function General({ value, setDefaultKey, defaultKey, setParentTab }) {
  const dispatch = useDispatch();
  const deviceData = useSelector(getDeviceData);
  const [allFormValue, setAllFormValue] = useState({});
  const [formFields] = useState([{ key: "", value: "" }]);
  const onFinish = async (values) => {
    let arr = [];
    arr.push(values);
    try {
      let payload = {
        ...value,
        ...allFormValue,
        general: arr,
        status: "active",
      };
      const res = await addDeviceService(payload);

      if (res?.data?.status === "1") {
        setParentTab("qcParameters");
        dispatch(resetPerformanceData());
        localStorage.setItem("deviceID", res?.data?.data[0]?.id);
      }
    } catch (error) {
      logger(error);
    }
  };

  useEffect(() => {
    if (defaultKey === "general") {
      if (deviceData) {
        if (!deviceData[0]?.performance) {
          setDefaultKey("performance");
        }
        if (!deviceData[1]?.feature) {
          setDefaultKey("feature");
        }
        if (!deviceData[2]?.camera) {
          setDefaultKey("camera");
        }
        if (!deviceData[3]?.battery) {
          setDefaultKey("battery");
        }

        const result = deviceData?.map((item) => {
          if (item?.performance) {
            return item?.performance;
          }
          if (item?.feature) {
            return item?.feature;
          }
          if (item?.camera) {
            return item?.camera;
          }
          if (item?.battery) {
            return item?.battery;
          }
        });
        let tempObj = {
          ...result[0],
          ...result[1],
          ...result[2],
          ...result[3],
        };
        setAllFormValue(tempObj);
      } else {
        setDefaultKey("performance");
      }
    }
  }, [defaultKey]);

  return (
    <>
      <Form onFinish={onFinish}>
        <div className="dropdown-body dropdown-body-rg">
          <div className="row g-3">
            {formFields?.map((item) => {
              return (
                <Row className="g-2 g-xxl-4">
                  <Col lg={3}>
                    <div className="form-group ">
                      <Form.Item name={item?.key}>
                        <Input
                          className="form-control form-control-lg "
                          placeholder="key"
                          // onChange={(e) =>
                          //   setFormFields({
                          //     ...formFields,
                          //     key: e.target.value,
                          //   })
                          // }
                        />
                      </Form.Item>
                    </div>
                  </Col>
                  <Col lg={4}>
                    <div className="form-group ">
                      <Form.Item name={item?.value}>
                        <Input
                          className="form-control form-control-lg "
                          placeholder="value"
                          // onChange={(e) =>
                          //   setFormFields({
                          //     ...formFields,
                          //     value: e.target.value,
                          //   })
                          // }
                        />
                      </Form.Item>
                    </div>
                  </Col>
                </Row>
              );
            })}
          </div>
        </div>
        <div className="form-group d-flex justify-content-center">
          <CommonButton
            extraClassName="btn  btn-primary mt-4 ms-4"
            // loading={loading}
            htmlType="submit"
            type="submit"
          >
            Submit
          </CommonButton>
        </div>
      </Form>
    </>
  );
}

export default General;

import React, { useState } from "react";
import Tabs from "../../../components/UiElement/Tabs";
import Performance from "./Specification/performance";
import Feature from "./Specification/Feature";
import Camera from "./Specification/Camera";
import Battery from "./Specification/Battery";
// import General from "./Specification/General";

function Specification({ setParentTab, deviceFormSubmit, initialValues }) {
  const [defaultKey, setDefaultKey] = useState("performance");
  const tabContent = [
    {
      name: "Performance",
      key: "performance",
      content: (
        <div className="tab-pane active">
          <Performance
            setDefaultKey={setDefaultKey}
            deviceFormSubmit={deviceFormSubmit}
            defaultKey={defaultKey}
          />
        </div>
      ),
    },
    {
      name: "Feature",
      key: "feature",
      content: (
        <div className="tab-pane ">
          <Feature setDefaultKey={setDefaultKey} defaultKey={defaultKey} />
        </div>
      ),
    },
    {
      name: "Camera",
      key: "camera",
      content: (
        <div className="tab-pane ">
          <Camera setDefaultKey={setDefaultKey} defaultKey={defaultKey} />
        </div>
      ),
    },
    {
      name: "Battery",
      key: "battery",
      content: (
        <div className="tab-pane ">
          <Battery
            setDefaultKey={setDefaultKey}
            defaultKey={defaultKey}
            setParentTab={setParentTab}
            formValues={initialValues}
          />
        </div>
      ),
    },
    // {
    //   name: "General",
    //   key: "general",
    //   content: (
    //     <div className="tab-pane ">
    //       <General
    //         value={initialValues}
    //         setDefaultKey={setDefaultKey}
    //         defaultKey={defaultKey}
    //         setParentTab={setParentTab}
    //       />
    //     </div>
    //   ),
    // },
  ];

  return (
    <>
      <Tabs
        tabContent={tabContent}
        tabsFor="table"
        border
        activeKey={defaultKey}
        setActiveKey={setDefaultKey}
      />
    </>
  );
}

export default Specification;

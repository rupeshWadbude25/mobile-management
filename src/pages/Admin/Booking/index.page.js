/* eslint-disable no-plusplus */
/* eslint-disable import/named */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
} from "../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import {
  checkValidData,
  checkValidPrice,
  decodeQueryData,
  logger,
  modalNotification,
  textFormatter,
} from "../../../utils";
import BookingForm from "../../../components/Form/User/Booking/index.form";
import {
  addVanderService,
  deleteBookingService,
  deviceBookingService,
  getBookingListService,
  getPaymentTypeList,
} from "../../../services/Admin/Master/index.service";
import CustomerForm from "../../../components/Form/User/Customer/index.form";
import adminRouteMap from "../../../routeControl/adminRouteMap";

function Booking() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");
  const [deviceListData, setDeviceListData] = useState([]);
  const [customerName, setCustomerName] = useState("");
  const [loading, setLoading] = useState(false);
  const [customerModal, setCustomerModal] = useState(false);
  const [customerNumber, setCustomerNumber] = useState("");
  const [searchName, setSearchName] = useState("");
  const [paymentData, setPaymentData] = useState([]);
  const [paymentDataList, setPaymentDataList] = useState([]);

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "View",
        icon: "icon ni ni-eye",
        action: "redirect",
        path: `${adminRouteMap?.BOOKING_DETAILS?.path}/${row?.id}`,
      },
      {
        name: "Delete",
        icon: "icon ni ni-trash",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };
  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Booking",
    },
  ];

  const getBookinvgData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
        search_device: searchName,
      };

      const res = await getBookingListService(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.booking_list);
        setNoOfPage(
          res?.data?.data?.booking_list > 0
            ? Math.ceil(res?.data?.data?.booking_list || [] / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_booking || 0);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const columns = [
    {
      dataField: "variant_name",
      text: "Device Name",
      formatter: (cell, row) => textFormatter(row?.variant_name),
      // headerClasses: "sorting",
    },
    {
      dataField: "customer_name",
      text: "Customer Name",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.customer_name)),
      // headerClasses: "sorting",
    },
    {
      dataField: "customer_mobile",
      text: "Mobile Number",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.customer_mobile)),
      // headerClasses: "sorting",
    },
    {
      dataField: "price",
      text: "Price",
      formatter: (cell, row) => checkValidPrice(row?.price),
      // headerClasses: "sorting",
    },
    {
      dataField: "payment_type_name",
      text: "Payment Mode",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.payment_type_name)),
      // headerClasses: "sorting",
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  const getPaymentData = async () => {
    try {
      let payload = {
        offset: 0,
      };
      const res = await getPaymentTypeList(payload);
      if (res?.data?.status === "1") {
        setPaymentDataList(res?.data?.data?.payment_type_list);
        let arr = res?.data?.data?.payment_type_list?.map((item) => {
          return {
            label: item?.payment_type,
            value: item?.payment_type,
          };
        });
        setPaymentData(arr);
      }
    } catch (error) {
      logger(error);
    }
  };

  const onSubmit = async (value) => {
    // console.log("value", value);
    setLoading(true);
    let finalArray = [];
    paymentDataList?.map((item) => {
      let arr = value?.payment_type?.map((elm) => {
        if (item?.payment_type === elm) {
          finalArray.push({
            payment_type: elm,
            value: value[elm],
            id: item?.id,
          });
          // delete value.value[elm];
        }
      });
      return arr;
    });
    delete value.payment_type;

    let payload = {
      // barcode: value?.barcode,
      // booking_amount: value?.booking_amount,
      // booking_date: value?.booking_date,
      description: value?.description,
      fk_customer_id: value?.fk_customer_id,
      fk_inv_dvc_id: deviceListData?.map((item) => item?.id),
      mobileNumber: value?.mobileNumber,
      payment_type: finalArray,
    };

    // console.log("payload", payload);

    try {
      // let payload = { ...value };
      payload.fk_customer_id = customerName?.id;
      // payload.fk_inv_dvc_id = deviceListData[0]?.id;
      const response = await deviceBookingService(payload);
      if (response?.data?.status === "1") {
        setShow(false);
        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
        getBookinvgData();
      } else {
        modalNotification({
          type: "error",
          message: response?.data?.message,
        });
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  const addCustomer = async (value) => {
    setLoading(true);
    try {
      let payload = {
        ...value,
        user_type: "customer",
      };
      if (rowData?.id) {
        payload.vendor_id = rowData?.id;
        payload.status = rowData?.cstatus;
      }
      const res = await addVanderService(payload);
      if (res?.data?.status === "1") {
        setCustomerName(res?.data?.data[0]);
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setCustomerModal(false);
      } else {
        modalNotification({
          type: "error",
          message: res?.data?.message,
        });
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  async function deleteBooking() {
    try {
      let payload = {
        booking_id: rowData?.id,
        status: rowData?.status,
        inventory_id: rowData?.fk_inv_dvc_id,
      };
      const res = await deleteBookingService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        getBookinvgData();
      }
    } catch (error) {
      logger(error);
    }
  }
  const onTypeDeleteConfirmAlert = () => {
    deleteBooking(rowData);
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const getSearchValue = (val) => {
    setSearchName(val);
    if (val) {
      tableReset();
    }
  };

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getBookinvgData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getBookinvgData();
    }
  }, []);

  useEffect(() => {
    getPaymentData();
  }, []);

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Booking">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Booking"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header
        hasLimit
        headerPagination={false}
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        getSearchValue={getSearchValue}
        searchPlaceholder="Customer Name , Mobile Number , Variant Name"
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this user?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        size="lg"
        show={show}
        title={rowData?.id ? "Edit Booking" : "Add Booking"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <BookingForm
          paymentData={paymentData}
          onSubmit={onSubmit}
          rowData={rowData}
          setDeviceListData={setDeviceListData}
          deviceListData={deviceListData}
          setCustomerName={setCustomerName}
          customerName={customerName}
          loading={loading}
          setCustomerModal={setCustomerModal}
          setCustomerNumber={setCustomerNumber}
        />
      </ModalComponent>

      <ModalComponent
        // size="lg"
        show={customerModal}
        title="Add Customer"
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setCustomerModal(false);
        }}
      >
        <CustomerForm
          onSubmit={addCustomer}
          customerNumber={customerNumber}
          loading={loading}
        />
      </ModalComponent>
    </>
  );
}

export default Booking;

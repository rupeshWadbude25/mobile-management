import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
  VanderForm,
} from "../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import {
  checkValidData,
  decodeQueryData,
  logger,
  modalNotification,
  textFormatter,
} from "../../../utils";
import {
  addVanderService,
  getVanderService,
  updateVanderService,
} from "../../../services/Admin/Master/index.service";

function vender() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");
  const [loading, setLoading] = useState(false);
  const [searchName, setSearchName] = useState("");

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  const getVanderData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
        search: searchName,
        user_type: "vendor",
      };

      const res = await getVanderService(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.user_list);
        setNoOfPage(
          res?.data?.data?.total_user > 0
            ? Math.ceil(res?.data?.data.total_user / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_user);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const onSubmit = async (value) => {
    setLoading(true);
    try {
      let payload = {
        ...value,
        user_type: "vendor",
      };
      if (rowData?.id) {
        payload.vendor_id = rowData?.id;
        payload.status = rowData?.cstatus;
      }
      const res = rowData?.id
        ? await updateVanderService(payload)
        : await addVanderService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setShow(false);
        getVanderData();
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  const updateVandorStatus = async () => {
    setLoading(true);
    try {
      let payload = {
        user_type: "vendor",
        name: rowData?.name || "",
        email: rowData?.email || "",
        code: rowData?.code || "",
        mobile: rowData?.mobile || "",
        alt_mobile: rowData?.alt_mobile || "",
        tax_number: rowData?.tax_number || "",
        address: rowData?.address || "",
        vendor_id: rowData?.id,
        status: rowData?.cstatus === "active" ? "inactive" : "active",
      };

      const res = await updateVanderService(payload);

      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setShow(false);
        getVanderData();
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  const onTypeDeleteConfirmAlert = () => {
    updateVandorStatus();
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: row?.cstatus === "active" ? "InActive" : "Active",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };
  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Vandors",
    },
  ];

  const columns = [
    {
      dataField: "name",
      text: "Vandor Name",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "code",
      text: "Vandor Code",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "email",
      text: "Email",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "tax_number",
      text: "Tax number",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "mobile",
      text: "Mobile Number",
      formatter: (cell) => textFormatter(cell),
      // headerClasses: "sorting",
    },
    {
      dataField: "address",
      text: "Address",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "cstatus",
      text: "Status",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  const getSearchValue = (val) => {
    setSearchName(val);
    if (val) {
      tableReset();
    }
  };
  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getVanderData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getVanderData();
    }
  }, []);

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Vandor">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Vendor"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        // header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        getSearchValue={getSearchValue}
        searchPlaceholder="Vender name , Vender code, Email , Mobile number"
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this vender?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        size="lg"
        title={rowData?.id ? "Update vender" : "Add vender"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <VanderForm
          // onSubmit={addBatteryType}
          loading={loading}
          onSubmit={onSubmit}
          rowData={rowData}
        />
      </ModalComponent>
    </>
  );
}

export default vender;

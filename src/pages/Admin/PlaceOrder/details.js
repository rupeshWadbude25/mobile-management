import React, { useState } from "react";
import { useTranslation } from "react-i18next";
import {
  checkValidCount,
  checkValidData,
  FormContainer,
  GlobalLoader,
  PageHeader,
} from "../../../components";
import Breadcrumb from "../../../components/UiElement/Breadcrumb";
import { GeneralText } from "../../../components/UiElement/GeneralText";

function OrderDetails() {
  const { t } = useTranslation();
  const [productData] = useState([]);

  const [loading] = useState(false);

  const breadcrumb = [
    {
      path: "#",
      name: "Place Order",
    },
    {
      path: "#",
      name: "Order Details",
    },
  ];

  // const getBookingDetails = async () => {
  //   try {
  //     let payload = {
  //       booking_id: "1",
  //     };
  //     const res = await getBookingDetailsService(payload);
  //     console.log("res", res);
  //   } catch (error) {
  //     logger(error);
  //   }
  // };

  // useEffect(() => {
  //   getBookingDetails();
  // }, []);

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Order Details">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
        </div>
      </div>
      {loading ? (
        <GlobalLoader />
      ) : (
        <FormContainer>
          <div className="row mb-5 text-capitalize">
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.productId")}
              value={checkValidData(productData?.productId)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.productName")}
              value={checkValidData(productData?.productName)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.brandName")}
              value={checkValidData(productData?.Brand?.name)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.category")}
              value={checkValidData(productData?.categoryDetails?.name)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.subCategory")}
              value={checkValidData(productData?.subCategoryDetails?.name)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.childCategory")}
              value={checkValidData(productData?.childCategoryDetails?.name)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.price")}
              value={checkValidCount(productData?.price)}
              currency="$"
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.availableQty")}
              value={checkValidCount(productData?.quantity)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.totalSold")}
              value={checkValidCount(productData?.totalSold)}
            />
            <GeneralText
              extraClassName="col-sm-6 col-lg-4 col-xxl-3"
              label={t("text.productDetails.sellerName")}
              value={`${productData?.Brand?.sellerDetails?.firstName || "-"} ${
                productData?.Brand?.sellerDetails?.lastName || ""
              }`}
            />
            {productData?.sellerProductVariantDetails?.length >= 1 &&
              productData?.sellerProductVariantDetails.map((item, key) => {
                return (
                  <GeneralText
                    extraClassName="col-sm-6 col-lg-4 col-xxl-3"
                    label={item?.ProductVariant?.name || ""}
                    value={item?.ProductVariantAttribute?.attributeNames}
                    key={key}
                  />
                );
              })}
          </div>
        </FormContainer>
      )}
    </>
  );
}

export default OrderDetails;

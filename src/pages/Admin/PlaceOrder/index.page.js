/* eslint-disable no-plusplus */
/* eslint-disable import/named */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
} from "../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import {
  checkValidData,
  checkValidPrice,
  decodeQueryData,
  logger,
  modalNotification,
  textFormatter,
} from "../../../utils";
import {
  addVanderService,
  getOrderListService,
  getPaymentTypeList,
  placeOrderService,
} from "../../../services/Admin/Master/index.service";
import CustomerForm from "../../../components/Form/User/Customer/index.form";
import adminRouteMap from "../../../routeControl/adminRouteMap";
import OrderForm from "../../../components/Form/User/PlaceOrder/index.form";

function PlaceOrder() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");
  const [deviceListData, setDeviceListData] = useState([]);
  const [customerName, setCustomerName] = useState("");
  const [loading, setLoading] = useState(false);
  const [customerModal, setCustomerModal] = useState(false);
  const [customerNumber, setCustomerNumber] = useState("");
  const [searchName, setSearchName] = useState("");
  const [paymentData, setPaymentData] = useState([]);
  const [paymentDataList, setPaymentDataList] = useState([]);
  const [totalPrice, setTotalPrice] = useState();

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "View",
        icon: "icon ni ni-eye",
        action: "redirect",
        path: `${adminRouteMap?.ORDER_DETAILS?.path}/${row?.id}`,
      },
      // {
      //   name: "Delete",
      //   icon: "icon ni ni-trash",
      //   action: "confirm",
      //   onClickHandle: () => {
      //     setRowData(row);
      //     setIsAlertVisibleDelete(true);
      //     document.body.click();
      //   },
      // },
    ];

    return optionsArr;
  };
  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Place Order",
    },
  ];

  const getOrderData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
        search_device: searchName,
      };
      const res = await getOrderListService(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.booking_list);
        setNoOfPage(
          res?.data?.data?.booking_list > 0
            ? Math.ceil(res?.data?.data?.booking_list || [] / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_booking || 0);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const columns = [
    {
      dataField: "variant_name",
      text: "Device Name",
      formatter: (cell, row) => textFormatter(row?.variant_name),
      // headerClasses: "sorting",
    },
    {
      dataField: "customer_name",
      text: "Customer Name",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.customer_name)),
      // headerClasses: "sorting",
    },
    {
      dataField: "customer_mobile",
      text: "Mobile Number",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.customer_mobile)),
      // headerClasses: "sorting",
    },
    {
      dataField: "price",
      text: "Price",
      formatter: (cell, row) => checkValidPrice(row?.price),
      // headerClasses: "sorting",
    },
    {
      dataField: "payment_type_name",
      text: "Payment Mode",
      formatter: (cell, row) =>
        checkValidData(textFormatter(row?.payment_type_name)),
      // headerClasses: "sorting",
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  const getPaymentData = async () => {
    try {
      let payload = {
        offset: 0,
      };
      const res = await getPaymentTypeList(payload);
      if (res?.data?.status === "1") {
        setPaymentDataList(res?.data?.data?.payment_type_list);
        let arr = res?.data?.data?.payment_type_list?.map((item) => {
          return {
            label: item?.payment_type,
            value: item?.payment_type,
          };
        });
        setPaymentData(arr);
      }
    } catch (error) {
      logger(error);
    }
  };

  const onSubmit = async (value) => {
    setLoading(true);
    let finalArray = [];
    paymentDataList?.map((item) => {
      let arr = value?.payment_type?.map((elm) => {
        if (item?.payment_type === elm) {
          finalArray.push({
            payment_type: elm,
            value: value[elm],
            id: item?.id,
          });
        }
      });
      return arr;
    });
    delete value.payment_type;

    let payload = {
      description: value?.description,
      fk_customer_id: value?.fk_customer_id,
      total_price: totalPrice || "",
      discounted_price: value?.discountedPrice,
      quantity: deviceListData?.length > 0 ? deviceListData?.length : "",
      delivery_charge: "0",
      coupon_id: "0",
      inventory_device: deviceListData?.map((item) => item?.id),
      mobileNumber: value?.mobileNumber,
      payment_mode: finalArray,
    };
    try {
      payload.fk_customer_id = customerName?.id;
      const response = await placeOrderService(payload);
      if (response?.data?.status === "1") {
        setShow(false);
        setCustomerNumber();
        setCustomerName({
          ...customerName,
          name: "",
        });
        setCustomerName("");
        setTotalPrice();
        setDeviceListData([]);
        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
      } else {
        modalNotification({
          type: "error",
          message: response?.data?.message,
        });
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  const addCustomer = async (value) => {
    setLoading(true);
    try {
      let payload = {
        ...value,
        user_type: "customer",
      };
      if (rowData?.id) {
        payload.vendor_id = rowData?.id;
        payload.status = rowData?.cstatus;
      }
      const res = await addVanderService(payload);
      if (res?.data?.status === "1") {
        setCustomerName(res?.data?.data[0]);
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setCustomerModal(false);
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  const getSearchValue = (val) => {
    setSearchName(val);
    if (val) {
      tableReset();
    }
  };

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getOrderData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getOrderData();
    }
  }, []);

  useEffect(() => {
    getPaymentData();
  }, []);

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Place Order">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Place Order"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header
        hasLimit
        headerPagination={false}
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        tableLoader={tableLoading}
        tableReset={tableReset}
        getSearchValue={getSearchValue}
        searchPlaceholder="Customer Name , Mobile Number , Variant Name"
      />

      <ModalComponent
        size="lg"
        show={show}
        title={rowData?.id ? "Edit Order" : "Place Order"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
          setCustomerNumber();
          setCustomerName();
          setTotalPrice();
        }}
      >
        <OrderForm
          totalPrice={totalPrice}
          setTotalPrice={setTotalPrice}
          paymentData={paymentData}
          onSubmit={onSubmit}
          rowData={rowData}
          setDeviceListData={setDeviceListData}
          deviceListData={deviceListData}
          setCustomerName={setCustomerName}
          customerName={customerName}
          loading={loading}
          setCustomerModal={setCustomerModal}
          setCustomerNumber={setCustomerNumber}
          customerNumber={customerNumber}
        />
      </ModalComponent>

      <ModalComponent
        // size="lg"
        show={customerModal}
        title="Add Customer"
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setCustomerModal(false);
        }}
      >
        <CustomerForm
          onSubmit={addCustomer}
          customerNumber={customerNumber}
          loading={loading}
        />
      </ModalComponent>
    </>
  );
}

export default PlaceOrder;

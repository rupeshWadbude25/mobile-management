/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
import React from "react";
import { Form, Input, Radio, Table } from "antd";
import { render } from "@testing-library/react";
import {
  CommonButton,
  DataTable,
  checkValidData,
} from "../../../../components";
import RadioComponent from "../../../../components/Antd/Radio/index.ant";
import { logger } from "../../../../utils";

function QCTable({ tableData }) {
  const columns = [
    {
      dataField: "qc_name",
      title: "Parameter Name",
      render: (cell, row) => checkValidData(row?.qc_name),
    },
    {
      text: "",
      headerClasses: "sorting text-center",
      render: (cell, row) => (
        <>
          <Form.Item
            name={`device_qc_id${row?.id}`}
            initialValue={row?.id}
            className="ms-1"
          />
          <Form.Item
            name={`${row?.id}-price`}
            // initialValue={`qc_value${row?.id}`}
            className="ms-1"
          >
            <Radio.Group
              className="RadioBtn text-center"
              options={[
                { label: "Yes", value: "1" },
                { label: "No", value: "0" },
              ]}
            />
          </Form.Item>
        </>
      ),
    },
    {
      dataField: "email",
      title: "Description",
      headerClasses: "sorting",
      render: (cell, row) => (
        <Form.Item
          name={`qc_description${row?.id}`}
          // initialValue={`qc_description${row?.id}`}
          className="ms-1"
        >
          <Input
            className="form-control"
            variant="standard"
            type="text"
            placeholder="Enter description"
          />
        </Form.Item>
      ),
    },
    {
      dataField: "email",
      title: "Price",
      headerClasses: "sorting",
      render: (cell, row) => (
        <Form.Item
          name={`qc_repair_cost${row?.id}`}
          // initialValue={`qc_repair_cost${row?.id}`}
          className="ms-1"
        >
          <Input
            className="form-control"
            variant="standard"
            type="text"
            placeholder="Enter price"
          />
        </Form.Item>
      ),
    },
  ];

  const onFinish = async (value) => {
    try {
      let finalArray = [];
      for (let index = 0; index < Object.values(value).length; index++) {
        if (index % 4 === 0) {
          finalArray.push({
            device_qc_id: Object.values(value)[index],
            qc_value: Object.values(value)[index + 1],
            qc_description: Object.values(value)[index + 2],
            qc_repair_cost: Object.values(value)[index + 3],
          });
        }
      }
      console.log("finalArray", finalArray);
    } catch (error) {
      logger(error);
    }
  };

  return (
    <>
      <Form onFinish={onFinish}>
        {" "}
        {/* <DataTable
          pagination={false}
          //   selectRow
          submitBtn
          header={false}
          hasLimit
          noOfPage="1"
          sizePerPage="10"
          page="1"
          count="100"
          tableData={tableData}
          tableColumns={columns}
          // param={param}

          // defaultSort={defaultSort}
          setSizePerPage=""
          // tableLoader={tableLoader}
          // tableReset={tableReset}
          // getSearchValue={getSearchValue}
          // searchPlaceholder={t("text.search.ManageSubscription")}
        /> */}
        {/* <Table dataSource={tableData} columns={columns} /> */}
        <ol>
          <li>
            <div className="d-flex justify-content-between">
              <div className="ms-5">
                <h6>Name</h6>
              </div>
              <div>
                <h6>Yes / No</h6>
              </div>
              <div>
                <h6>Description</h6>
              </div>
              <div className="mx-5">
                <h6>Price</h6>
              </div>
            </div>
          </li>
          {tableData?.map((item) => (
            <li>
              <div className="d-flex justify-content-between mt-3">
                <Form.Item
                  name={`device_qc_id${item?.id}`}
                  initialValue={item?.id}
                />
                <div className="">{item?.qc_name}</div>
                <div>
                  <Form.Item
                    name={`${item?.id}-price`}
                    // initialValue={`qc_value${row?.id}`}
                    className="ms-1"
                  >
                    <Radio.Group
                      className="RadioBtn text-center"
                      options={[
                        { label: "Yes", value: "1" },
                        { label: "No", value: "0" },
                      ]}
                    />
                  </Form.Item>
                </div>
                <div>
                  <Form.Item
                    name={`qc_description${item?.id}`}
                    className="ms-5"
                    // initialValue={`qc_description${row?.id}`}
                  >
                    <Input
                      className="form-control"
                      variant="standard"
                      type="text"
                      placeholder="Enter description"
                    />
                  </Form.Item>
                </div>
                <div>
                  <Form.Item
                    name={`qc_repair_cost${item?.id}`}
                    // initialValue={`qc_description${row?.id}`}
                    className="ms-5"
                  >
                    <Input
                      className="form-control"
                      variant="standard"
                      type="text"
                      placeholder="Enter description"
                    />
                  </Form.Item>
                </div>
              </div>
            </li>
          ))}
        </ol>
        <div className="d-flex justify-content-end">
          <CommonButton
            className="btn btn-success ms-2"
            type="primary"
            htmlType="submit"
            //   loading={loading}
          >
            Submit
          </CommonButton>
        </div>
      </Form>
    </>
  );
}

export default QCTable;

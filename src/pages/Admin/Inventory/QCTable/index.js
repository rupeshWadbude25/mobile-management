/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import {
  getQCParamsDetailsService,
  getQCParamsListService,
} from "../../../../services/Admin/Master/index.service";
import { logger } from "../../../../utils";
import { Breadcrumb, PageHeader } from "../../../../components";
import QCForm from "./checkQC";
import QCTable from "./qcTable";
import Tabs from "../../../../components/UiElement/Tabs";

function QCFormIndex() {
  const params = useParams();
  const [tableData, setTableData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [defaultKey, setDefaultKey] = useState("qcForm");
  const initialValues = {
    accessories_alb: "",
    or_sm_imei_box_alb: "",
    or_bill_alb: "",
    warranty_alb: "",
    front_camera: "",
    back_camera: "",
    alert_slider: "",
    battery_health: "",
    charging_port: "",
    scraches_on_screen: "",
    original_screen: "",
    sp_or_ln_on_screen: "",
    shading_on_screen: "",
    back_glass: "",
    device_qc_params: "",
  };

  const [state, setState] = useState(initialValues);
  const [inventoryDetails, setInventoryDetails] = useState({
    qcDetails: "",
    qcParam: "",
  });

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "New Inventory",
    },
    {
      path: "#",
      name: "QC Form",
    },
  ];

  const getQCParmsList = async () => {
    try {
      let payload = {
        device_id: "20",
      };
      const response = await getQCParamsListService(payload);
      if (response?.data?.status) {
        setTableData(response?.data?.data);
      }
    } catch (error) {
      logger(error);
    }
  };

  const getDetails = async () => {
    setLoading(true);
    try {
      let payload = {
        inventory_device_id: params?.id || "",
      };

      const response = await getQCParamsDetailsService(payload);
      if (response?.data?.status === "1") {
        const value = response?.data?.data[0];
        setState({
          ...state,
          code: value?.code,
          modal_name: value?.model_name,
          variant: value?.variant_name,
          color: value?.color_name,
          imei_number: value?.imei_serial_no_first,
        });
        setInventoryDetails({
          ...inventoryDetails,
          qcDetails: response?.data?.data[0]?.inv_qc_details[0],
          qcParam: response?.data?.data[0]?.inv_qc_param,
        });
        setTableData(response?.data?.data[0]?.inv_qc_param);
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  useEffect(() => {
    getDetails();
    getQCParmsList();
  }, []);

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="QC Form">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
        </div>
      </div>
      {/* <Tabs
        tabContent={tabContent}
        tabsFor="table"
        border
        activeKey={defaultKey}
        setActiveKey={setDefaultKey}
      /> */}
      <QCForm
        // setParentTab={setParentTab}
        // setInitialValue={setInitialValue}
        inventoryDetails={inventoryDetails}
        state={state}
        tableData={tableData}
        loading={loading}
      />
    </>
  );
}

export default QCFormIndex;

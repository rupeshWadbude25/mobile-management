/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
import React, { useEffect, useState } from "react";
import { Col, DatePicker, Form, Input, Radio, Spin } from "antd";
import { Row } from "react-bootstrap";
// import { Form, Formik } from "formik";

import moment from "moment";
import { useNavigate, useParams } from "react-router-dom";
import dayjs from "dayjs";
import {
  CommonButton,
  PageHeader,
  Input as TextInput,
} from "../../../../components";

import RadioComponent from "../../../../components/Antd/Radio/index.ant";
import QCTable from ".";
import {
  dateFormatter,
  enterOnlyNumericValue,
  logger,
  modalNotification,
} from "../../../../utils";
import { dobFormate } from "../../../../helpers";
import { updateQCParamService } from "../../../../services/Admin/Master/index.service";
import adminRouteMap from "../../../../routeControl/adminRouteMap";

function QCForm({ state, loading, tableData, inventoryDetails }) {
  const params = useParams();
  const navigate = useNavigate();
  const [date, setDate] = useState();
  const modulesType = [
    { label: "Yes", value: "1" },
    { label: "No", value: "2" },
    { label: "Box without IMEI", value: "3" },
  ];
  const originalBill = [
    { label: "Yes", value: "1" },
    { label: "No", value: "2" },
  ];
  const accessoriesArr = [
    { label: "Cable Available", value: "1" },
    { label: "Adaptor Available", value: "2" },
    { label: "All Accesories Available", value: "3" },
    { label: "No Accessories Available", value: "4" },
  ];
  const frontCameraArr = [
    { label: "Yes Working", value: "1" },
    { label: "Minor Blur / Dust", value: "2" },
    { label: "Not Working", value: "3" },
  ];
  const backCameraArr = [
    { label: "Yes All Camera Working", value: "1" },
    { label: "Minor Blur / Dust", value: "2" },
    { label: "Some Camera Not Working", value: "3" },
    { label: "All Camera Not Working", value: "4" },
  ];
  const sliderArr = [
    { label: "Yes Working", value: "1" },
    { label: "Not Working", value: "2" },
    { label: "Not Available", value: "3" },
  ];
  const chargingPortArr = [
    { label: "Yes Working", value: "1" },
    { label: "Not Working", value: "2" },
    { label: "Loose", value: "3" },
  ];
  const trueTuneArr = [
    { label: "Yes Working", value: "1" },
    { label: "Not Working", value: "2" },
    { label: "Not Applicable (Android)", value: "3" },
  ];
  const originalScreenArr = [
    { label: "Yes", value: "1" },
    { label: "Yes Original but Unknown Part", value: "2" },
    { label: "Yes But Glass Changed", value: "3" },
    { label: "No Duplicate", value: "4" },
    { label: "Broken / Cracked", value: "5" },
  ];
  const scrachesArr = [
    { label: "No Scraches", value: "1" },
    { label: "Minor Scraches", value: "2" },
    { label: "Major Scraches", value: "3" },
  ];
  const sportLineArr = [
    { label: "No Spot & Lines", value: "1" },
    { label: "Minor White / Black Spots", value: "2" },
    { label: "Major White / Black Spots", value: "3" },
    { label: "Minor Lines", value: "4" },
    { label: "Major Lines", value: "5" },
  ];
  const shadingArr = [
    { label: "No Shading", value: "1" },
    { label: "Minor Shading", value: "2" },
    { label: "Major Shading", value: "3" },
  ];
  const scrachesDentsArr = [
    { label: "No Scraches & Dents", value: "1" },
    { label: "Minor Dents / Scraches", value: "2" },
    { label: "Major Dents / Scraches", value: "3" },
  ];
  const backGlassArr = [
    { label: "Yes", value: "1" },
    { label: "Cracked", value: "2" },
    { label: "Changed", value: "3" },
  ];
  const formatDataArr = [{ label: "Yes", value: "1" }];
  const onSubmit = async (value) => {
    let formVal = { ...value };
    let tempVal2 = { ...value };
    delete tempVal2.battery_health;
    delete tempVal2.accessories_alb;
    delete tempVal2.alert_slider;
    delete tempVal2.back_camera;
    delete tempVal2.back_glass;
    delete tempVal2.charging_port;
    delete tempVal2.format_data_rm_sim;
    delete tempVal2.front_camera;
    delete tempVal2.imei_number;
    delete tempVal2.or_bill_alb;
    delete tempVal2.or_sm_imei_box_alb;
    delete tempVal2.original_screen;
    delete tempVal2.scrach_dnt_on_body;
    delete tempVal2.scraches_on_screen;
    delete tempVal2.shading_on_screen;
    delete tempVal2.sp_or_ln_on_screen;
    delete tempVal2.true_tone;
    delete tempVal2.warranty_alb;
    delete tempVal2.code;
    try {
      let finalArray = [];
      for (let index = 0; index < Object.values(tempVal2).length; index++) {
        if (index % 4 === 0) {
          finalArray.push({
            device_qc_id: Object.values(tempVal2)[index],
            qc_value: Object.values(tempVal2)[index + 1],
            qc_description: Object.values(tempVal2)[index + 2],
            qc_repair_cost: Object.values(tempVal2)[index + 3],
          });
        }
      }
      let payload = {
        fk_device_inv_id: params?.id,
        device_qc_params: finalArray,
        or_sm_imei_box_alb: formVal?.or_sm_imei_box_alb,
        or_bill_alb: formVal?.or_bill_alb,
        accessories_alb: formVal?.accessories_alb,
        warranty_alb: dateFormatter(date?.$d),
        front_camera: formVal?.front_camera,
        back_camera: formVal?.back_camera,
        alert_slider: formVal?.alert_slider,
        battery_health: formVal?.battery_health,
        charging_port: formVal?.charging_port,
        true_tone: formVal?.true_tone,
        original_screen: formVal?.original_screen,
        scraches_on_screen: formVal?.scraches_on_screen,
        sp_or_ln_on_screen: formVal?.sp_or_ln_on_screen,
        shading_on_screen: formVal?.shading_on_screen,
        scrach_dnt_on_body: formVal?.scrach_dnt_on_body,
        back_glass: formVal?.back_glass,
        format_data_rm_sim: formVal?.format_data_rm_sim,
      };
      payload.warranty_alb = dateFormatter(date?.$d, "YYYY/MM/DD");
      const res = await updateQCParamService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        navigate(adminRouteMap.INVENTORY.path);
      }
    } catch (error) {
      logger(error);
    }
  };

  return (
    <>
      {loading ? (
        <Spin spinning={loading} />
      ) : (
        <Form onFinish={onSubmit} id="form">
          <Row>
            {/* <Col sm={6}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Email
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="email"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter barcode"
                        // setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col> */}
            {/* <Col sm={6}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Name
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="name"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter barcode"
                        // setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col> */}
            {/* <Col sm={6}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Customer / Vendor Name
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="customer_vendor_name"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter IMEI/SerialNo"
                        // setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col> */}
            {/* <Col sm={6}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Customer / Vendor Mobile Number
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="number"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter IMEI/SerialNo"
                        // setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col> */}
            <Col sm={6}>
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Vendor Code
                </label>
                <div className="form-control-wrap qcInput">
                  {/* <TextInput
                    className="form-control"
                    name="code"
                    disabled
                    variant="standard"
                    type="text"
                    placeholder="Enter vender code"
                    // setFieldValue={props.handleChange}
                  /> */}

                  <Form.Item initialValue={state?.code}>
                    <Input
                      value={state?.code}
                      disabled
                      className="form-control"
                      variant="standard"
                      type="text"
                      placeholder={state?.code}
                    />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <>
              <h5 className="mt-4">Phone Details</h5>
              <hr style={{ width: "90%" }} className="mb-4" />
              <Col sm={8}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Model Name
                  </label>
                  <div className="form-control-wrap qcInput">
                    {/* <TextInput
                      className="form-control"
                      name="modal_name"
                      disabled
                      variant="standard"
                      type="text"
                      placeholder="Enter purchase Price Without GST"
                      // setFieldValue={props.handleChange}
                    /> */}
                    <Form.Item initialValue={state?.modal_name}>
                      <Input
                        disabled
                        value={state?.modal_name}
                        className="form-control"
                        variant="standard"
                        type="text"
                        placeholder={state?.modal_name}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Variant
                  </label>
                  <div className="form-control-wrap qcInput">
                    {/* <TextInput
                      className="form-control"
                      name="variant"
                      disabled
                      variant="standard"
                      type="text"
                      placeholder="Enter SGST9%"
                      // setFieldValue={props.handleChange}
                    /> */}
                    <Form.Item initialValue={state?.variant}>
                      <Input
                        disabled
                        className="form-control"
                        variant="standard"
                        type="text"
                        value={state?.variant}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Color
                  </label>
                  <div className="form-control-wrap qcInput">
                    {/* <TextInput
                      className="form-control"
                      name="color"
                      disabled
                      variant="standard"
                      type="text"
                      placeholder="Enter CGST9%"
                      // setFieldValue={props.handleChange}
                    /> */}
                    <Form.Item initialValue={state?.color}>
                      <Input
                        disabled
                        className="form-control"
                        variant="standard"
                        type="text"
                        value={state?.color}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    IMEI Number
                  </label>
                  <div className="form-control-wrap qcInput">
                    {/* <TextInput
                      className="form-control"
                      name="imei_number"
                      disabled
                      variant="standard"
                      type="text"
                      placeholder="Enter IGST18%"
                      // setFieldValue={props.handleChange}
                    /> */}

                    <Form.Item initialValue={state?.imei_number}>
                      <Input
                        className="form-control"
                        variant="standard"
                        type="text"
                        disabled
                        value={state?.imei_number}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Col>

              <Col sm={8}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Battery Health
                  </label>
                  <div className="form-control-wrap">
                    {/* <TextInput
                      className="form-control"
                      name="Battery_health"
                      disabled={false}
                      variant="standard"
                      type="text"
                      placeholder="Enter Selling Price"
                      // setFieldValue={props.handleChange}
                    /> */}
                    <Form.Item
                      initialValue={inventoryDetails?.qcDetails?.battery_health}
                      name="battery_health"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                    >
                      <Input
                        className="form-control"
                        variant="standard"
                        type="text"
                        placeholder="Enter description"
                        min="0"
                        inputmode="numeric"
                        onKeyPress={(e) => {
                          // onCheckValue(e);
                          enterOnlyNumericValue(e);
                        }}
                      />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8}>
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Phone Has Warranty Available ?
                  </label>
                  <div className="form-control-wrap">
                    {/* <DatePicker
                      name="warranty_alb"
                      className="form-control date-picker shadow-none"
                      placeholder="DD/MM/YY"
                    /> */}
                    <Form.Item
                      initialValue={
                        dayjs(inventoryDetails?.qcDetails?.warranty_alb) || ""
                      }
                      name="warranty_alb"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                    >
                      <DatePicker
                        defaultValue={dayjs(
                          inventoryDetails?.qcDetails?.warranty_alb
                        )}
                        onChange={(e) => setDate(e)}
                        name="warranty_alb"
                        className="form-control date-picker shadow-none"
                        placeholder="DD/MM/YYYY"
                      />
                    </Form.Item>
                  </div>
                </div>
              </Col>
            </>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Original Same IMEI Box Available ?
                </label>
                <div className="form-control-wrap">
                  {/* <RadioComponent
                    radioGroupArray={modulesType}
                    name="or_sm_imei_box_alb "
                  /> */}
                  {console.log(
                    "inventoryDetails",
                    inventoryDetails?.qcDetails?.or_sm_imei_box_alb
                  )}
                  <Form.Item
                    name="or_sm_imei_box_alb"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                    initialValue={
                      inventoryDetails?.qcDetails?.or_sm_imei_box_alb
                    }
                  >
                    <Radio.Group options={modulesType} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Original Bill Available ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={inventoryDetails?.qcDetails?.or_bill_alb}
                    name="or_bill_alb"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={originalBill} />
                  </Form.Item>
                </div>
              </div>
            </Col>

            <Col sm={6} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Accessories Available ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={inventoryDetails?.qcDetails?.accessories_alb}
                    name="accessories_alb"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={accessoriesArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>

            <h5 className="mt-4">Body Condition & Screen Condition</h5>
            <hr style={{ width: "90%" }} />

            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Scraches on Screen ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={
                      inventoryDetails?.qcDetails?.scraches_on_screen
                    }
                    name="scraches_on_screen"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={scrachesArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Spots/Lines on Screen ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={
                      inventoryDetails?.qcDetails?.sp_or_ln_on_screen
                    }
                    name="sp_or_ln_on_screen"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={sportLineArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={6} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Original Screen ?
                </label>
                <div className="form-control-wrap">
                  {/* <RadioComponent
                    radioGroupArray={originalScreenArr}
                    name="original_screen"
                    /> */}
                  <Form.Item
                    initialValue={inventoryDetails?.qcDetails?.original_screen}
                    name="original_screen"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={originalScreenArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Shading on Screen ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={
                      inventoryDetails?.qcDetails?.shading_on_screen
                    }
                    name="shading_on_screen"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={shadingArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Scraches/Dents on Body ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={
                      inventoryDetails?.qcDetails?.scrach_dnt_on_body
                    }
                    name="scrach_dnt_on_body"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={scrachesDentsArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Back Glass Original ?
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    initialValue={inventoryDetails?.qcDetails?.back_glass}
                    name="back_glass"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={backGlassArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <Col sm={8} className="mt-4">
              <div className="form-group">
                <label className="form-label" htmlFor="category-name">
                  Format Data & Remove Sim Card
                </label>
                <div className="form-control-wrap">
                  <Form.Item
                    name="format_data_rm_sim"
                    initialValue={
                      inventoryDetails?.qcDetails?.format_data_rm_sim
                    }
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                  >
                    <Radio.Group options={formatDataArr} />
                  </Form.Item>
                </div>
              </div>
            </Col>
            <>
              <h5 className="mt-5">
                Functional Check Report Please Check Carefully
              </h5>
              <hr style={{ width: "90%" }} />
              <Col sm={8} className="mt-4">
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Front Camera Working Perfectly ?
                  </label>
                  <div className="form-control-wrap">
                    <Form.Item
                      initialValue={inventoryDetails?.qcDetails?.front_camera}
                      name="front_camera"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                      // initialValue={`qc_value${row?.id}`}
                    >
                      <Radio.Group options={frontCameraArr} />
                    </Form.Item>
                  </div>
                </div>
              </Col>

              <Col sm={8} className="mt-4">
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    All Back Camera Working Perfectly ?
                  </label>
                  <div className="form-control-wrap">
                    {/* <RadioComponent
                      radioGroupArray={backCameraArr}
                      name="back_camera "
                    /> */}
                    <Form.Item
                      initialValue={inventoryDetails?.qcDetails?.back_camera}
                      name="back_camera"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                      // initialValue={`qc_value${row?.id}`}
                    >
                      <Radio.Group options={backCameraArr} />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8} className="mt-4">
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Alert Slider Working Perfectly ?
                  </label>
                  <div className="form-control-wrap">
                    <Form.Item
                      initialValue={inventoryDetails?.qcDetails?.alert_slider}
                      name="alert_slider"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                      // initialValue={`qc_value${row?.id}`}
                    >
                      <Radio.Group options={sliderArr} />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8} className="mt-4">
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Charging Port Working Perfectly ?
                  </label>
                  <div className="form-control-wrap">
                    <Form.Item
                      initialValue={inventoryDetails?.qcDetails?.charging_port}
                      name="charging_port"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                      // initialValue={`qc_value${row?.id}`}
                    >
                      <Radio.Group options={chargingPortArr} />
                    </Form.Item>
                  </div>
                </div>
              </Col>
              <Col sm={8} className="mt-4">
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    True Tone Working ?
                  </label>
                  <div className="form-control-wrap">
                    <Form.Item
                      initialValue={inventoryDetails?.qcDetails?.true_tone}
                      name="true_tone"
                      rules={[
                        {
                          required: true,
                          message: "This field is required",
                        },
                      ]}
                      // initialValue={`qc_value${row?.id}`}
                    >
                      <Radio.Group options={trueTuneArr} />
                    </Form.Item>
                  </div>
                </div>
              </Col>

              <Col className="mt-4">
                <div className="form-group">
                  <label className="form-label" htmlFor="category-name">
                    Working Perfectly ?
                  </label>

                  {/* {PerfectlyArr?.map((item, index) => (
                        <div className="form-control-wrap mt-3 ">
                          <span className="mx-3 ">{item}</span>
                          <RadioComponent
                            radioGroupArray={originalBill}
                            name={`type${index}`}
                          />
                        </div>
                      ))} */}
                </div>
              </Col>
            </>
          </Row>
          {tableData?.map((item) => (
            <li>
              <div className="d-flex justify-content-between mt-3">
                <Form.Item
                  name={`device_qc_id${item?.id}`}
                  initialValue={item?.id}
                />
                <div className="">{item?.qc_name}</div>
                <div>
                  <Form.Item
                    name={`${item?.id}-price`}
                    // initialValue={`qc_value${row?.id}`}
                    className="ms-1 qcRadio"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                    initialValue={item?.qc_value || ""}
                  >
                    <Radio.Group
                      // defaultValue={item?.fk_device_qc_id}
                      className="RadioBtn text-center"
                      options={[
                        { label: "Yes", value: "1" },
                        { label: "No", value: "0" },
                      ]}
                    />
                  </Form.Item>
                </div>
                <div>
                  <Form.Item
                    initialValue={item?.qc_description}
                    name={`qc_description${item?.id}`}
                    className="ms-5"
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                    // initialValue={`qc_description${row?.id}`}
                  >
                    <Input
                      className="form-control"
                      variant="standard"
                      type="text"
                      placeholder="Enter description"
                    />
                  </Form.Item>
                </div>
                <div>
                  <Form.Item
                    initialValue={item?.qc_repair_cost}
                    name={`qc_repair_cost${item?.id}`}
                    // initialValue={`qc_description${row?.id}`}
                    rules={[
                      {
                        required: true,
                        message: "This field is required",
                      },
                    ]}
                    className="ms-5"
                  >
                    <Input
                      className="form-control"
                      variant="standard"
                      type="text"
                      placeholder="Enter description"
                      min="0"
                      inputmode="numeric"
                      onKeyPress={(e) => {
                        // onCheckValue(e);
                        enterOnlyNumericValue(e);
                      }}
                    />
                  </Form.Item>
                </div>
              </div>
            </li>
          ))}

          <div className="form-group text-center mt-3 ">
            <CommonButton
              id="submit"
              type="submit"
              htmlType="submit"
              style={{ display: "table-footer-group" }}
              className="btn btn-primary ripple-effect w-20"
              // loading={loading}
            >
              Submit
            </CommonButton>
          </div>
        </Form>
      )}
    </>
  );
}

export default QCForm;

/* eslint-disable import/extensions */
import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  actionFormatter,
  checkValidData,
  textFormatter,
} from "../../../components";
import AddEditInventory from "./addEditInventory";
import { decodeQueryData, logger } from "../../../utils";
import { getnewInventoryListService } from "../../../services/Admin/Master/index.service";

function AddDevice() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [rowData, setRowData] = useState("");
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getInventoryData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        barcode: "",
        brand_id: "",
        model_id: "",
        variant_id: "",
        inventory_status: "1",
        offset: page,
      };
      const res = await getnewInventoryListService(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.mob_devices);
        setNoOfPage(
          res?.data?.data?.total_devices > 0
            ? Math.ceil(res?.data?.data.total_devices / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_devices);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "New Inventory",
    },
  ];

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };
  const options = (row) => {
    const optionsArr = [
      // {
      //   name: "View",
      //   icon: "icon ni ni-eye",
      //   action: "redirect",
      //   path: adminRouteMap.INVENTORY_Details.path,
      //   // onClickHandle: () => {showViewDiscountModal(); setViewData(row); setViewDataModal('pending'); document.body.click()}
      // },
      {
        name: "Update Inventory",
        // icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      // {
      //   name: "QC Update",
      //   // icon: "icon ni ni-pen",
      //   action: "redirect",
      //   path: `${adminRouteMap.CHECK_QC.path}/${row?.id}`,
      // },
    ];
    return optionsArr;
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const columns = [
    {
      dataField: "barcode",
      text: "Barcode",
      formatter: (cell, row) => checkValidData(row?.barcode),
    },
    {
      dataField: "device_name",
      text: "Device Name",
      formatter: (cell, row) => checkValidData(row?.device_name),
    },

    {
      dataField: "code",
      text: "Vendor Code",
      formatter: (cell, row) => checkValidData(row?.code),
    },
    {
      dataField: "price",
      text: "Price",
      formatter: (cell, row) =>
        checkValidData(`${parseFloat(row?.price).toFixed(2)} `),
    },
    // {
    //   dataField: "QC Status",
    //   text: "Device Status",
    //   formatter: (cell, row) =>
    //     checkValidData(textFormatter(row?.device_status)),
    // },
    {
      dataField: "QC Status",
      text: "QC Status",
      formatter: (cell, row) =>
        checkValidData(row?.qc_status === "0" ? "Not Done" : "Done"),
    },
    {
      dataField: "cstatus",
      text: "Status",
      formatter: (cell, row) => checkValidData(textFormatter(row?.cstatus)),
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getInventoryData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getInventoryData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading=" New Inventory">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add New Inventory"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />

      <ModalComponent
        size="xl"
        show={show}
        title={rowData?.id ? "Edit New Inventory" : "Add New Inventory"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddEditInventory
          usedInventory
          rowData={rowData}
          setShow={setShow}
          getInventoryData={getInventoryData}
        />
      </ModalComponent>
    </>
  );
}

export default AddDevice;

import React from "react";
import AddEditUsedInventoryForm from "../../../../components/Form/User/AddEditUsedInventoryForm/index.form";

function AddEditUsedInventory({ rowData }) {
  const onSubmit = async (value) => {
    console.log("value", value);
  };

  return <AddEditUsedInventoryForm rowData={rowData} onSubmit={onSubmit} />;
}

export default AddEditUsedInventory;

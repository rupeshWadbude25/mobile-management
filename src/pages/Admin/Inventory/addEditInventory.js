/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
import React, { useEffect, useState } from "react";
import AddEditInventoryForm from "../../../components/Form/User/AddEditInventory/index.form";
import {
  deviceColorListService,
  getBarcodeService,
  getBrandList,
  getDeviceService,
  getModalList,
  getVanderService,
  getVariantList,
  newInventoryService,
  updateInventoryService,
} from "../../../services/Admin/Master/index.service";
import { logger, modalNotification } from "../../../utils";

function AddEditInventory({ rowData, setShow, getInventoryData }) {
  const [brandLoading, setBrandLoading] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [variantLoading, setVariantLoading] = useState(false);
  const [colorLoading, setColorLoading] = useState(false);
  const [vandorLoading, setVandorLoading] = useState(false);
  const [brandData, setBrandData] = useState([]);
  const [modalData, setModalData] = useState([]);
  const [variantData, setVariantData] = useState([]);
  const [deviceData, setDeviceData] = useState([]);
  const [colorData, setColorData] = useState([]);
  const [vandorData, setVandorData] = useState([]);
  const [barcodeData, setBarcodeData] = useState();

  const [ids, setIDS] = useState({
    brand_id: undefined,
    modal_id: undefined,
    variant_id: undefined,
    device_id: undefined,
  });
  const onSubmit = async (value) => {
    let payload = {
      ...value,
    };
    payload.inventory_status = "1";
    if (rowData?.id) {
      payload.inv_device_id = rowData?.id;
    }
    const res = rowData?.id
      ? await updateInventoryService(payload)
      : await newInventoryService(payload);
    if (res?.data?.status === "1") {
      setShow(false);
      modalNotification({
        type: "success",
        message: res?.data?.message,
      });
      getInventoryData();
    }
  };

  const getBrandData = async () => {
    setBrandLoading(true);
    try {
      const res = await getBrandList();
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.brand_list?.map((item) => {
          return {
            name: item?.brand_name,
            id: item?.id,
          };
        });
        setBrandData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setBrandLoading(false);
  };

  const getModalData = async (props, id) => {
    setModalLoading(true);
    if (props) {
      props?.setFieldValue("model_id", undefined);
      props?.setFieldValue("variant_id", undefined);
      props?.setFieldValue("color", undefined);
      props?.setFieldValue("fk_device_id", undefined);
    }
    try {
      let payload = {
        brand_id: id,
      };
      const res = await getModalList(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.model_list?.map((item) => {
          return {
            name: item?.model_name,
            id: item?.id,
          };
        });
        setModalData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setModalLoading(false);
  };

  const getVariantData = async (props, id) => {
    setVariantLoading(true);
    if (props) {
      props?.setFieldValue("variant_id", undefined);
      props?.setFieldValue("fk_device_id", undefined);
      props?.setFieldValue("color", undefined);
    }
    try {
      let payload = {
        model_id: id,
      };
      const res = await getVariantList(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.variant_list?.map((item) => {
          return {
            name: item?.variant_name,
            id: item?.id,
          };
        });
        setVariantData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setVariantLoading(false);
  };
  const getDeviceData = async (props, id) => {
    setVariantLoading(true);
    if (props) {
      props?.setFieldValue("fk_device_id", undefined);
      props?.setFieldValue("color", undefined);
    }
    try {
      let payload = {
        brand_id: ids?.brand_id,
        model_id: ids?.modal_id,
        variant_id: ids?.variant_id,
      };
      const res = await getDeviceService(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.mob_devices?.map((item) => {
          return {
            name: item?.device_name,
            id: item?.id,
          };
        });
        setDeviceData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setVariantLoading(false);
  };
  const getColorData = async (id) => {
    setColorLoading(true);
    try {
      let payload = {
        device_id: ids?.device_id,
      };
      const res = await deviceColorListService(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.map((item) => {
          return {
            name: item?.color_name,
            id: item?.id,
          };
        });
        setColorData(arr);
      }
    } catch (error) {
      logger(error);
    }
    setColorLoading(false);
  };
  const getVandorData = async (props, id) => {
    setVandorLoading(true);
    try {
      let payload = {
        offset: 0,
      };
      const res = await getVanderService(payload);
      if (res?.data?.status === "1") {
        let arr = res?.data?.data?.user_list?.map((item) => {
          if (item?.code !== null) {
            return {
              name: item?.code,
              id: item?.id,
            };
          }
        });
        const result = arr.filter((item) => item !== undefined && item);
        setVandorData(result);
      }
    } catch (error) {
      logger(error);
    }
    setVandorLoading(false);
  };

  const getBarcode = async () => {
    try {
      let payload = {
        device_type: "new",
      };
      const res = await getBarcodeService(payload);
      if (res?.data?.status === "1") {
        setBarcodeData(res?.data?.data);
      }
    } catch (error) {
      logger(error);
    }
  };

  useEffect(() => {
    // getBarcode();
    getBrandData();
    getVandorData();
  }, []);

  useEffect(() => {
    if (!rowData?.id) {
      getBarcode();
    }
  }, [rowData?.id]);

  useEffect(() => {
    if (
      rowData?.fk_brand_id &&
      rowData?.fk_model_id &&
      rowData?.fk_device_id &&
      rowData?.color
    ) {
      getModalData("", rowData?.fk_brand_id);
      getVariantData("", rowData?.fk_model_id);
      getDeviceData("", rowData?.fk_device_id);
      getColorData("", rowData?.color);
    }
  }, [rowData]);

  return (
    <AddEditInventoryForm
      barcodeData={barcodeData}
      onSubmit={onSubmit}
      rowData={rowData}
      brandLoading={brandLoading}
      modalLoading={modalLoading}
      variantLoading={variantLoading}
      colorLoading={colorLoading}
      brandData={brandData}
      modalData={modalData}
      variantData={variantData}
      getModalData={getModalData}
      getVariantData={getVariantData}
      setIDS={setIDS}
      ids={ids}
      getDeviceData={getDeviceData}
      deviceData={deviceData}
      getColorData={getColorData}
      colorData={colorData}
      vandorData={vandorData}
      vandorLoading={vandorLoading}
    />
  );
}

export default AddEditInventory;

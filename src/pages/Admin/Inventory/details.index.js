/* eslint-disable no-unused-vars */
/* eslint-disable no-plusplus */
import React from "react";
import { Form, Input } from "antd";
import { Breadcrumb, DataTable, PageHeader } from "../../../components";

function InventoryDetails() {
  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "/inventory",
      name: "Manage Inventory",
    },
    {
      path: "#",
      name: "Manage Inventory Details",
    },
  ];

  const customerData = [
    {
      id: 1,
      name: "Ashley Lawson",
      email: "ashley@test.com",
      description: "Lorazepam is used to relieve anxiety",
      price: "",
      status: "active",
    },
    {
      id: 2,
      name: "Ashley Lawson",
      email: "ashley@test.com",
      description: "Lorazepam is used to relieve anxiety",
      price: "",
      status: "active",
    },
    {
      id: 3,
      name: "Ashley Lawson",
      email: "ashley@test.com",
      description: "Lorazepam is used to relieve anxiety",
      price: "",
      status: "active",
    },
  ];

  const columns = [
    {
      dataField: "id",
      text: "S.No.",
      headerClasses: "w_70",
    },
    {
      dataField: "name",
      text: "Parameter Name",
      headerClasses: "sorting",
    },
    {
      dataField: "email",
      text: "Description",
      headerClasses: "sorting",
      formatter: (cell, row) => (
        <Form.Item
          name={`${row?.id}-description`}
          initialValue={row?.description || undefined}
          className="ms-1"
        >
          <Input
            className="form-control"
            data-ms-editor="true"
            // onChange={(e) => {
            //   setValue([{ id: row?.id, name: e.target.value }]);
            // }}
            type="text"
          />
        </Form.Item>
      ),
    },
    {
      dataField: "phone",
      text: "Price",
      headerClasses: "sorting",
      formatter: (cell, row) => (
        <Form.Item
          name={`${row?.id}-price`}
          initialValue={row?.price || undefined}
          className="ms-1"
        >
          <Input
            className="form-control"
            data-ms-editor="true"
            // onChange={(e) => {
            //   setValue([{ id: row?.id, name: e.target.value }]);
            // }}
            type="text"
          />
        </Form.Item>
      ),
    },
  ];
  const onFinish = async (value) => {
    let finalArray = [];
    for (let index = 0; index < Object.values(value).length; index++) {
      if (index % 2 === 0) {
        finalArray.push({
          description: Object.values(value)[index],
          price: Object.values(value)[index + 1],
        });
      }
    }
  };

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading=" Manage Inventory Details">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
        </div>
      </div>
      <Form onFinish={onFinish}>
        <DataTable
          submitBtn
          //   selectRow
          hasLimit
          noOfPage="1"
          sizePerPage="10"
          page="1"
          count="100"
          tableData={customerData}
          tableColumns={columns}
          // param={param}
          // defaultSort={defaultSort}
          setSizePerPage=""
          // tableLoader={tableLoader}
          // tableReset={tableReset}
          // getSearchValue={getSearchValue}
          // searchPlaceholder={t("text.search.ManageSubscription")}
        />
      </Form>
    </>
  );
}

export default InventoryDetails;

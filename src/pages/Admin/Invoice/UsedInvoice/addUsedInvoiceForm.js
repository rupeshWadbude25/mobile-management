import { Formik, Form } from "formik";
import React, { useEffect, useState } from "react";
import { Col, Row } from "antd";
import { Link } from "react-router-dom";
import {
  Input as TextInput,
  CommonButton,
  DataTable,
  Select,
  SweetAlert,
} from "../../../../components";
import { checkValidData, logger, modalNotification } from "../../../../utils";
import {
  getnewInventoryListService,
  getVanderService,
} from "../../../../services/Admin/Master/index.service";

function AddUsedInvioce({
  rowData,
  setCustomerModal,
  setCustomerName,
  customerName,
  setCustomerNumber,
  setDeviceListData,
  deviceListData,
}) {
  const [deviceList, setDeviceList] = useState([1]);
  const [searchName, setSearchName] = useState("");
  const [isAlertVisible, setIsAlertVisible] = useState(false);
  const [barcodeValue, setBarcodeValue] = useState({
    props: "",
    barcode: "",
  });
  let tempArr = [];
  let deleteKeyArr = [1];

  const initialValues = {
    fk_customer_id: customerName?.name || "",
    status: rowData?.cstatus || undefined,
    name:
      rowData?.charging_type ||
      rowData?.battery_type_name ||
      rowData?.network_name ||
      rowData?.os_name ||
      "",
  };

  const columns = [
    {
      dataField: "barcode",
      text: "Barcode",
      formatter: (cell, row) => checkValidData(row?.barcode),
      // headerClasses: "w_70",
    },
    {
      dataField: "model_name",
      text: "Model",
      formatter: (cell, row) => checkValidData(row?.model_name),
      // headerClasses: "sorting",
    },
    {
      dataField: "imei_serial_no_first",
      text: "IMEI",
      formatter: (cell, row) => checkValidData(row?.imei_serial_no_first),
      // headerClasses: "sorting",
    },
    {
      dataField: "price",
      text: "Price",
      formatter: (cell, row) => checkValidData(row?.price),
      // headerClasses: "sorting",
    },
  ];

  const paymentMode = [
    {
      id: "Card",
      name: "Card",
    },
    {
      id: "UPI",
      name: "UPI",
    },
  ];

  const addNewDevice = () => {
    // arr.push(arr[arr?.length - 1]?.arr?.length + 1);
    // console.log("arr", arr);
    setDeviceList([
      ...deviceList,
      deviceList.push(
        deviceList[deviceList?.length - 1]?.deviceList?.length + 1
      ),
    ]);
  };

  const deleteDevice = (value) => {
    console.log("value", value);
    const result = deviceList?.filter((elm) => elm !== value && elm);
    console.log("result", result);
    setDeviceList(result);
    // setDeviceList([
    //   ...deviceList,
    //   deviceList.push(
    //     deviceList[deviceList?.length - 1]?.deviceList?.length + 1
    //   ),
    // ]);
  };

  const getCustomerData = async (value) => {
    try {
      let payload = {
        offset: 0,
        search: value,
        user_type: "customer",
      };

      const res = await getVanderService(payload);
      if (res?.data?.status === "1") {
        setCustomerName(res?.data?.data?.user_list[0]);
      } else {
        setIsAlertVisible(true);
      }
    } catch (error) {
      logger(error);
    }
  };

  const getDeviceData = async (value) => {
    try {
      let payload = {
        barcode: value || "",
        brand_id: "",
        model_id: "",
        variant_id: "",
        offset: 0,
        inventory_status: "2",
      };

      const res = await getnewInventoryListService(payload);
      if (res?.data?.status === "1") {
        // if (deviceListData?.length > 0) {
        //   setDeviceListData([
        //     ...deviceListData,
        //     res?.data?.data?.mob_devices[0],
        //   ]);
        // } else {
        setDeviceListData([res?.data?.data?.mob_devices[0]]);
        // }
        barcodeValue.props.setFieldValue("barcode", "");
      } else {
        modalNotification({
          type: "error",
          message: res?.data?.message,
        });
        setDeviceListData([]);
      }
    } catch (error) {
      logger(error);
    }
  };

  const onConfirmAlert = () => {
    // setTableLoading(true);

    setIsAlertVisible(false);
    // navigate(adminRouteMap.CUSTOMER.path);
    setCustomerModal(true);
    return false;
  };

  useEffect(() => {
    if (searchName?.length >= 10) {
      getCustomerData(searchName);
    } else {
      setCustomerName({
        ...customerName,
        name: "",
      });
    }
  }, [searchName]);

  useEffect(() => {
    if (barcodeValue?.barcode?.length >= 4) {
      getDeviceData(barcodeValue?.barcode);
    } else {
      setDeviceListData([]);
      setCustomerName("");
    }
  }, [barcodeValue]);

  return (
    <>
      <Formik
        initialValues={{ ...initialValues }}
        //   validationSchema={validation()}
        //   onSubmit={onSubmit}
      >
        {(props) => {
          if (props?.values?.mobileNumber) {
            setCustomerNumber(props?.values?.mobileNumber);
          } else {
            setCustomerName("");
          }
          return (
            <Form className="">
              <Row gutter={16}>
                <Col className="gutter-row" span={6}>
                  <div className="form-group ">
                    <label className="form-label" htmlFor="category-name">
                      Barcode
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="barcode"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter barcode"
                        value={barcodeValue?.barcode}
                        setFieldValue={props.handleChange}
                        setValues={(e) =>
                          setBarcodeValue({
                            ...barcodeValue,
                            props,
                            barcode: e,
                          })
                        }
                      />
                    </div>
                  </div>
                </Col>

                <Col className="gutter-row" span={6}>
                  <div className="form-group ">
                    <label className="form-label" htmlFor="category-name">
                      Mobile No.
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="mobileNumber"
                        setValues={setSearchName}
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter mobile number"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>
                <Col className="gutter-row" span={6}>
                  <div className="form-group ">
                    <label className="form-label" htmlFor="category-name">
                      Customer Name
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="name"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter customer name"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>

                {/* <Col className="gutter-row" span={6}>
                  <div className="form-group ">
                    <label className="form-label" htmlFor="category-name">
                      Type
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="name"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter IMEI/SerialNo"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col> */}
                <Col className="gutter-row" span={6}>
                  <div className="form-group">
                    <label className="form-label" htmlFor="category-name">
                      Barcode Start With
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="barcodeStartWith"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter barcode start with"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>
                <Col className="gutter-row" span={6}>
                  <div className="form-group ">
                    <label className="form-label" htmlFor="category-name">
                      Round Off
                    </label>
                    <div className="form-control-wrap">
                      <TextInput
                        className="form-control"
                        name="roundOff"
                        disabled={false}
                        variant="standard"
                        type="text"
                        placeholder="Enter round off"
                        setFieldValue={props.handleChange}
                      />
                    </div>
                  </div>
                </Col>
                {/* <Col className="gutter-row" span={6}>
                  <div className="form-group ">
                    <label className="form-label" htmlFor="category-name">
                      Type
                    </label>
                    <div className="form-control-wrap">
                      <Select
                        name="status"
                        disabled={false}
                        variant="standard"
                        setFieldValue={props.handleChange}
                        arrayOfData={[]}
                        placeholder="Select Type"
                      />
                    </div>
                  </div>
                </Col> */}
              </Row>
              {deviceList?.length > 0 &&
                deviceList?.map((value) => (
                  <Row gutter={16}>
                    <Col className="gutter-row" span={6}>
                      <div className="form-group ">
                        <label className="form-label" htmlFor="category-name">
                          Device{value}
                        </label>
                        <div className="form-control-wrap">
                          <TextInput
                            className="form-control"
                            name="device"
                            disabled={false}
                            variant="standard"
                            type="text"
                            placeholder="Enter device"
                            setFieldValue={props.handleChange}
                          />
                        </div>
                      </div>
                    </Col>
                    <Col className="gutter-row" span={6}>
                      <div className="form-group ">
                        <label className="form-label" htmlFor="category-name">
                          Payment Mode
                        </label>
                        <div className="form-control-wrap">
                          <Select
                            name="paymode"
                            disabled={false}
                            variant="standard"
                            setFieldValue={props.handleChange}
                            arrayOfData={paymentMode}
                            placeholder="Select Type"
                          />
                        </div>
                      </div>
                    </Col>
                    <Col className="gutter-row" span={6}>
                      <div className="form-group">
                        <label className="form-label" htmlFor="category-name">
                          Price
                        </label>
                        <div className="form-control-wrap">
                          <TextInput
                            className="form-control"
                            name="price"
                            disabled={false}
                            variant="standard"
                            type="text"
                            placeholder="Enter price"
                            setFieldValue={props.handleChange}
                          />
                        </div>
                      </div>
                    </Col>

                    <div className="mt-5">
                      {deviceList?.map((el, index) => {
                        if (!deleteKeyArr.includes(el) && index >= 1) {
                          tempArr.push(index);
                          deleteKeyArr = [...new Set(tempArr)];
                          return (
                            <Link
                              className="mx-3"
                              to="#"
                              onClick={(e) => {
                                e.preventDefault();
                                deleteDevice(el);
                              }}
                            >
                              Delete
                            </Link>
                          );
                        }
                      })}
                    </div>
                  </Row>
                ))}
              <Link
                to="#"
                onClick={(e) => {
                  e.preventDefault();
                  addNewDevice();
                }}
              >
                Add New
              </Link>

              <div className="form-group text-center mt-4 ">
                <CommonButton
                  type="submit"
                  htmlType="submit"
                  className="btn btn-primary ripple-effect "
                  // loading={loading}
                >
                  {rowData?.id ? "Update" : "Submit"}
                </CommonButton>
              </div>
            </Form>
          );
        }}
      </Formik>
      <DataTable
        header={false}
        pagination={false}
        hasLimit
        noOfPage="1"
        sizePerPage="10"
        page="1"
        count="100"
        tableData={deviceListData}
        tableColumns={columns}
        // param={param}
        // defaultSort={defaultSort}
        setSizePerPage=""
        // tableLoader={tableLoader}
        // tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />

      <SweetAlert
        title="Number Not Register"
        text="you want to register this number?"
        show={isAlertVisible}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisible}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onConfirmAlert}
      />
    </>
  );
}

export default AddUsedInvioce;

/* eslint-disable import/extensions */
import React, { useEffect, useState } from "react";

import {
  Breadcrumb,
  //   CommonButton,
  DataTable,
  InvoiceFormatter,
  ListingHeader,
  ModalComponent,
  PageHeader,
  actionFormatter,
} from "../../../../components";
import adminRouteMap from "../../../../routeControl/adminRouteMap";
import AddInvoice from "./addInvoiceForm";
import { handlePrint, logger, modalNotification } from "../../../../utils";
import { addVanderService } from "../../../../services/Admin/Master/index.service";
import CustomerForm from "../../../../components/Form/User/Customer/index.form";

function NewInvoice() {
  const [show, setShow] = useState(false);
  const [rowData, setRowData] = useState("");
  const [printInvoice, setPrintInvoice] = useState(false);
  const [customerModal, setCustomerModal] = useState(false);
  const [customerName, setCustomerName] = useState("");
  const [loading, setLoading] = useState(false);
  const [customerNumber, setCustomerNumber] = useState("");
  const [deviceListData, setDeviceListData] = useState([]);

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "New Invoice",
    },
  ];

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };
  const options = (row) => {
    const optionsArr = [
      {
        name: "View",
        icon: "icon ni ni-eye",
        action: "redirect",
        path: adminRouteMap.INVENTORY_Details.path,
        // onClickHandle: () => {showViewDiscountModal(); setViewData(row); setViewDataModal('pending'); document.body.click()}
      },
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: "Print Invoice",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          setPrintInvoice(true);
        },
      },
    ];
    return optionsArr;
  };

  const customerData = [
    {
      id: 1,
      name: "Ashley Lawson",
      email: "ashley@test.com",
      phone: "9580095000",
      price: "$0",
      status: "active",
    },
  ];

  const columns = [
    {
      dataField: "id",
      text: "S.No.",
      headerClasses: "w_70",
    },
    {
      dataField: "name",
      text: "User Name",
      headerClasses: "sorting",
    },
    {
      dataField: "email",
      text: "Email ID",
      headerClasses: "sorting",
    },
    {
      dataField: "phone",
      text: "Phone Number",
      headerClasses: "sorting",
    },
    {
      dataField: "status",
      text: "Status",
      headerClasses: "sorting",
      // formatter: () => switchFormatter(false),
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  const addCustomer = async (value) => {
    setLoading(true);
    try {
      let payload = {
        ...value,
        user_type: "customer",
      };
      if (rowData?.id) {
        payload.vendor_id = rowData?.id;
        payload.status = rowData?.cstatus;
      }
      const res = await addVanderService(payload);
      if (res?.data?.status === "1") {
        setCustomerName(res?.data?.data[0]);
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setCustomerModal(false);
      } else {
        modalNotification({
          type: "error",
          message: res?.data?.message,
        });
      }
    } catch (error) {
      logger(error);
    }
    setLoading(false);
  };

  useEffect(() => {
    if (printInvoice) {
      handlePrint("billingInvoice");
      setPrintInvoice(false);
    }
  }, [printInvoice, rowData]);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="New Invoice">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Invoice"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        hasLimit
        noOfPage="1"
        sizePerPage="10"
        page="1"
        count="100"
        tableData={customerData}
        tableColumns={columns}
        // param={param}
        // defaultSort={defaultSort}
        setSizePerPage=""
        // tableLoader={tableLoader}
        // tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />

      <ModalComponent
        size="xl"
        show={show}
        title={rowData?.id ? "Edit Invoice" : "Add New Invoice"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddInvoice
          setCustomerModal={setCustomerModal}
          customerName={customerName}
          loading={loading}
          setCustomerNumber={setCustomerNumber}
          setCustomerName={setCustomerName}
          setDeviceListData={setDeviceListData}
          deviceListData={deviceListData}
        />
      </ModalComponent>
      {printInvoice && (
        <InvoiceFormatter rowData={rowData} id="billingInvoice" />
      )}

      <ModalComponent
        // size="lg"
        show={customerModal}
        title="Add Customer"
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setCustomerModal(false);
        }}
      >
        <CustomerForm
          onSubmit={addCustomer}
          customerNumber={customerNumber}
          loading={loading}
        />
      </ModalComponent>
    </>
  );
}

export default NewInvoice;

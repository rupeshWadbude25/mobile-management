import React from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import adminRouteMap from "../../../routeControl/adminRouteMap";
import { LoginForm } from "../../../components";
import { loginService } from "../../../services/Admin/Master/index.service";
import { modalNotification, setLocalStorageToken } from "../../../utils";
import { login } from "../../../redux/AuthSlice/index.slice";

function Login() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const loginSuccessfully = async (value) => {
    let payload = {
      ...value,
    };
    const res = await loginService(payload);
    if (res?.data?.status === "1") {
      modalNotification({
        type: "success",
        message: res?.data?.message,
      });
      setLocalStorageToken(res?.data?.data[0]?.login_token);
      dispatch(login(res?.data?.data[0]));
      navigate(adminRouteMap.DASHBOARD.path);
    } else {
      modalNotification({
        type: "error",
        message: res?.data?.message,
      });
    }
    // modalNotification({
    //   type: "success",
    //   message: "Signin Successfully",
    // });
    // setTimeout(() => {
    //   navigate(adminRouteMap.DASHBOARD.path);
    // }, 2000);
  };
  return (
    <>
      <div className="nk-block nk-block-middle nk-auth-body  wide-xs">
        {/* <AuthLogo /> */}
        <div className="card">
          <div className="card-inner card-inner-lg">
            <div className="nk-block-head">
              <div className="nk-block-head-content">
                <h4 className="nk-block-title">Sign-In</h4>
                <div className="nk-block-des">
                  <p>
                    Access the mobile managment panel using your email and
                    password.
                  </p>
                </div>
              </div>
            </div>
            <LoginForm onSubmit={loginSuccessfully} />
          </div>
        </div>
      </div>
    </>
  );
}

export default Login;

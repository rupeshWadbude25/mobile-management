import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  AddColorForm,
  Breadcrumb,
  checkValidData,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
  textFormatter,
} from "../../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import { decodeQueryData, logger, modalNotification } from "../../../../utils";
import {
  addColorService,
  getColorList,
  updateColorService,
} from "../../../../services/Admin/Master/index.service";

function Color() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");
  const [formLoading, setFormLoading] = useState(false);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const getColorData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
      };

      const res = await getColorList(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.color_list);
        setNoOfPage(
          res?.data?.data?.total_color > 0
            ? Math.ceil(res?.data?.data.total_color / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_color);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };
  const addColor = async (value) => {
    setFormLoading(true);
    try {
      let payload = {
        color_name: value?.name,
        status: value?.status,
        image: "",
      };
      if (rowData?.id) {
        payload.color_id = rowData?.id;
        payload.type = "1";
      }
      const response = rowData?.id
        ? await updateColorService(payload)
        : await addColorService(payload);
      if (response?.data?.status === "1") {
        setFormLoading(false);

        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
        setShow(false);
        getColorData();
        tableReset();
      }
    } catch (error) {
      logger(error);
    }
    setFormLoading(false);
  };

  async function deleteColor() {
    try {
      let payload = {
        color_name: rowData?.name,
        status: "deleted",
        image: "",
        type: "2",
        color_id: rowData?.id,
      };
      const res = await updateColorService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        getColorData();
        tableReset();
      }
    } catch (error) {
      logger(error);
    }
  }
  const onTypeDeleteConfirmAlert = () => {
    setLoading(true);
    deleteColor(rowData);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: "Delete",
        icon: "icon ni ni-trash",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Color",
    },
  ];

  const columns = [
    // {
    //   dataField: "id",
    //   text: "S.No.",
    //   headerClasses: "w_70",
    // },

    {
      dataField: "color_name",
      text: "Color Name",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "cstatus",
      text: "Status",
      // headerClasses: "sorting",
      formatter: (cell) => checkValidData(textFormatter(cell)),
    },

    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];
  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getColorData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getColorData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Color ">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Color"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this color?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        cancelButtonText="No"
        confirmButtonText="Yes"
        setIsAlertVisible={setIsAlertVisibleDelete}
        showLoaderOnConfirm
        loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        title={rowData?.id ? "Update Color" : "Add Color"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddColorForm
          onSubmit={addColor}
          rowData={rowData}
          loading={formLoading}
        />
      </ModalComponent>
    </>
  );
}

export default Color;

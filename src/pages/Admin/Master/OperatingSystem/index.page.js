import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  AddBatteryTypeForm,
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
} from "../../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import {
  checkValidData,
  decodeQueryData,
  logger,
  modalNotification,
  textFormatter,
} from "../../../../utils";
import {
  addOSService,
  getOSList,
  updateOSService,
} from "../../../../services/Admin/Master/index.service";

function OperatingSystem() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getOSData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
      };

      const res = await getOSList(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.os_list);
        setNoOfPage(
          res?.data?.data?.total_os > 0
            ? Math.ceil(res?.data?.data.total_os / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_os);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  async function deleteOS() {
    try {
      let payload = {
        os_name: rowData?.os_name,
        status: rowData?.cstatus,
        image: "",
        type: "2",
        os_id: rowData?.id,
      };
      const res = await updateOSService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setRowData("");
        getOSData();
      }
    } catch (error) {
      logger(error);
    }
  }
  const onTypeDeleteConfirmAlert = () => {
    deleteOS(rowData);
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const addOS = async (value) => {
    try {
      let payload = {
        os_name: value?.name,
        status: value?.status,
        image: "",
      };
      if (rowData?.id) {
        payload.os_id = rowData?.id;
        payload.type = "1";
      }
      const response = rowData?.id
        ? await updateOSService(payload)
        : await addOSService(payload);
      if (response?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
        setRowData("");
        setShow(false);
        getOSData();
        tableReset();
      }
    } catch (error) {
      logger(error);
    }
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: "Delete",
        icon: "icon ni ni-trash",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Operating System",
    },
  ];
  const columns = [
    {
      dataField: "os_name",
      text: "Operating System Name",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "cstatus",
      text: "Status",
      formatter: (cell) => textFormatter(cell),
      // headerClasses: "sorting",
    },

    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getOSData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getOSData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Operating System ">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Operating System"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this user?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        title={rowData?.id ? "Update Operating System" : "Add Operating System"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddBatteryTypeForm
          onSubmit={addOS}
          rowData={rowData}
          type="Operating System"
        />
      </ModalComponent>
    </>
  );
}

export default OperatingSystem;

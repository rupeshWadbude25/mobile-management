import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  AddCommonForm,
  Breadcrumb,
  DataTable,
  ListingHeader,
  // logoFormatter,
  ModalComponent,
  PageHeader,
  SweetAlert,
} from "../../../../components";
import {
  checkValidData,
  decodeQueryData,
  logger,
  modalNotification,
  textFormatter,
} from "../../../../utils";
import {
  addBrandService,
  getBrandList,
  updateBrandStatusService,
} from "../../../../services/Admin/Master/index.service";

function Brand() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [showImage, setShowImage] = useState(false);
  // const [imageURL, setImageURL] = useState("");
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getBrandData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
      };

      const res = await getBrandList(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.brand_list);
        setNoOfPage(
          res?.data?.data?.total_brand > 0
            ? Math.ceil(res?.data?.data.total_brand / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_brand);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const onHandleShow = (data) => {
    console.log("data", data);

    setRowData(data);
    setShow(true);
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  async function deleteBrand() {
    try {
      let payload = {
        brand_name: rowData?.name,
        status: rowData?.status,
        image: "",
        type: "2",
        brand_id: rowData?.id,
      };
      const res = await updateBrandStatusService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        tableReset();
        getBrandData();
      }
    } catch (error) {
      logger(error);
    }
  }
  const onTypeDeleteConfirmAlert = () => {
    deleteBrand(rowData);
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  // const setImagePath = (data) => {
  //   setShowImage(true);
  //   setImageURL(data);
  // };

  const addBrand = async (value) => {
    try {
      let payload = {
        brand_name: value?.name,
        status: value?.status,
        image: "",
      };
      if (rowData?.id) {
        payload.brand_id = rowData?.id;
        payload.type = "1";
      }
      const response = rowData?.id
        ? await updateBrandStatusService(payload)
        : await addBrandService(payload);
      if (response?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
        setShow(false);
        getBrandData();
        tableReset();
      }
    } catch (error) {
      logger(error);
    }
  };

  // const brandStatusUpdate = async (id, status) => {
  //   try {
  //     let bannerStatus = "";
  //     if (status === "active") {
  //       bannerStatus = "inactive";
  //     } else {
  //       bannerStatus = "active";
  //     }

  //     let bodyData = { status: bannerStatus };
  //     const res = await updateBrandStatusService(id, bodyData);
  //     const { success, message } = res;
  //     if (success) {
  //       modalNotification({
  //         type: "success",
  //         message,
  //       });

  //       return true;
  //     }
  //   } catch (error) {
  //     logger(error);
  //   }
  // };

  const options = (row) => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: "Delete",
        icon: "icon ni ni-trash",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Brand",
    },
  ];

  const columns = [
    // {
    //   dataField: "id",
    //   text: "S.No.",
    //   headerClasses: "w_70",
    // },
    // {
    //   dataField: "image",
    //   text: "Brand Image",
    //   formatter: () =>
    //     logoFormatter(
    //       "https://www.freepnglogos.com/uploads/original-samsung-logo-10.png",
    //       setImagePath
    //     ),
    //   // headerClasses: "sorting",
    // },
    {
      dataField: "brand_name",
      text: "Brand Name",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "cstatus",
      text: "Status",
      // headerClasses: "sorting",
    },

    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getBrandData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getBrandData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Brand ">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Brand"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this brand?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        title={rowData?.id ? "Update Brand" : "Add Brand"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddCommonForm brand onSubmit={addBrand} rowData={rowData} />
      </ModalComponent>
      <ModalComponent
        show={showImage}
        title="Brand Image"
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShowImage(false);
        }}
      >
        {/* <img className="category-img rounded-3" src={imageURL} alt="img" /> */}
      </ModalComponent>
    </>
  );
}

export default Brand;

import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  AddCommonForm,
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
} from "../../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import {
  checkValidData,
  dateFormatter,
  decodeQueryData,
  logger,
  modalNotification,
} from "../../../../utils";
import {
  addModalService,
  getBrandList,
  getModalList,
  updateModalService,
} from "../../../../services/Admin/Master/index.service";
import { classicDateFormat } from "../../../../helpers";

function Modal() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");
  const [brandData, setBrandData] = useState([]);

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getModalData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
      };

      const res = await getModalList(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.model_list);
        setNoOfPage(
          res?.data?.data?.total_model > 0
            ? Math.ceil(res?.data?.data.total_model / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_model);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const getBrandData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        list_type: "2",
      };

      const res = await getBrandList(payload);
      if (res?.data?.status === "1") {
        setBrandData(res?.data?.data?.brand_list);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  async function deleteModel() {
    try {
      let payload = {
        model_name: rowData?.model_name,
        status: rowData?.cstatus,
        image: "",
        type: "2",
        brand_id: rowData?.fk_brand_id,
        model_id: rowData?.id,
      };
      const res = await updateModalService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        setRowData("");
        getModalData();
      }
    } catch (error) {
      logger(error);
    }
  }
  const onTypeDeleteConfirmAlert = () => {
    deleteModel(rowData);
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const addModal = async (value) => {
    try {
      let payload = {
        model_name: value?.name,
        status: value?.status,
        brand_id: value?.brand_id,
        image: "",
      };
      if (rowData?.id) {
        payload.model_id = rowData?.id;
        payload.type = "1";
      }
      const response = rowData?.id
        ? await updateModalService(payload)
        : await addModalService(payload);
      if (response?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
        setRowData("");
        setShow(false);
        getModalData();
        tableReset();
      }
    } catch (error) {
      logger(error);
    }
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: "Delete",
        icon: "icon ni ni-trash",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };
  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Modal",
    },
  ];

  const columns = [
    {
      dataField: "video_type",
      text: "Modal Name",
      headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(row?.model_name),
    },
    {
      dataField: "cstatus",
      text: "Status",
      headerClasses: "sorting",
      formatter: (cell, row) => checkValidData(row?.cstatus),
    },
    {
      dataField: "Date",
      text: "Date",
      headerClasses: "sorting",
      formatter: (cell, row) =>
        checkValidData(dateFormatter(row?.updated_at, classicDateFormat)),
    },

    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];
  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getModalData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getModalData();
      getBrandData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Modal ">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add Modal"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this user?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        title={rowData?.id ? "Update Modal" : "Add Modal"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddCommonForm
          modal
          onSubmit={addModal}
          rowData={rowData}
          brandData={brandData}
        />
      </ModalComponent>
    </>
  );
}

export default Modal;

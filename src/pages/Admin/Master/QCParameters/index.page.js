import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import {
  actionFormatter,
  AddCommonForm,
  Breadcrumb,
  DataTable,
  ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
} from "../../../../components";
import {
  checkValidData,
  decodeQueryData,
  logger,
  modalNotification,
  textFormatter,
} from "../../../../utils";
import {
  addMasterQCService,
  getMasterQCList,
  updateMasterQCService,
} from "../../../../services/Admin/Master/index.service";

function QCParameters() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [param, setParam] = useState({});
  const [rowData, setRowData] = useState("");

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const getQCData = async () => {
    setTableLoading(true);
    try {
      let payload = {
        offset: page,
      };

      const res = await getMasterQCList(payload);
      if (res?.data?.status === "1") {
        setTableData(res?.data?.data?.qc_parameter_list);
        setNoOfPage(
          res?.data?.data?.total_qc_parameter > 0
            ? Math.ceil(res?.data?.data.total_qc_parameter / sizePerPage)
            : 1
        );
        setTotalCount(res?.data?.data?.total_qc_parameter);
      }
    } catch (error) {
      logger(error);
    }
    setTableLoading(false);
  };

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  async function deleteBrand() {
    try {
      let payload = {
        qc_name: rowData?.name,
        status: rowData?.status,
        image: "",
        type: "2",
        qc_id: rowData?.id,
      };
      const res = await updateMasterQCService(payload);
      if (res?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: res?.data?.message,
        });
        getQCData();
      }
    } catch (error) {
      logger(error);
    }
  }
  const onTypeDeleteConfirmAlert = () => {
    deleteBrand(rowData);
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const addQCParameter = async (value) => {
    try {
      let payload = {
        qc_name: value?.name,
        status: value?.status,
        image: "",
      };
      if (rowData?.id) {
        payload.qc_parameter_id = rowData?.id;
        payload.type = "1";
      }
      const response = rowData?.id
        ? await updateMasterQCService(payload)
        : await addMasterQCService(payload);
      if (response?.data?.status === "1") {
        modalNotification({
          type: "success",
          message: response?.data?.message,
        });
        setShow(false);
        getQCData();
        tableReset();
      }
    } catch (error) {
      logger(error);
    }
  };

  const options = (row) => {
    const optionsArr = [
      {
        name: "Update",
        icon: "icon ni ni-pen",
        action: "confirm",
        onClickHandle: () => {
          onHandleShow(row);
          document.body.click();
        },
      },
      {
        name: "Delete",
        icon: "icon ni ni-trash",
        action: "confirm",
        onClickHandle: () => {
          setRowData(row);
          setIsAlertVisibleDelete(true);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };

  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "QCParameters",
    },
  ];

  const columns = [
    // {
    //   dataField: "id",
    //   text: "S.No.",
    //   headerClasses: "w_70",
    // },

    {
      dataField: "qc_name",
      text: "QC Name",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "cstatus",
      text: "Status",
      // headerClasses: "sorting",
    },

    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  useEffect(() => {
    if (JSON.stringify(param) !== "{}") {
      getQCData();
    }
  }, [param]);

  useEffect(() => {
    if (!search) {
      getQCData();
    }
  }, []);
  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading=" QCParameters ">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          <ListingHeader
            btnText="Add QCParameters"
            btnArray={["create"]}
            onHandleShow={onHandleShow}
          />
        </div>
      </div>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this brand?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        title={rowData?.id ? "Update QCParameters" : "Add QCParameters"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <AddCommonForm
          QCParameters
          brand
          onSubmit={addQCParameter}
          rowData={rowData}
        />
      </ModalComponent>
    </>
  );
}

export default QCParameters;

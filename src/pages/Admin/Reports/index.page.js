import React, { useEffect, useState } from "react";
import { useLocation } from "react-router-dom";
import { Select } from "antd";
import { Formik } from "formik";
import {
  actionFormatter,
  Breadcrumb,
  // DailyReportFilter,
  DataTable,
  // ListingHeader,
  ModalComponent,
  PageHeader,
  SweetAlert,
} from "../../../components";
// import adminRouteMap from "../../../../routeControl/adminRouteMap";
import { checkValidData, decodeQueryData, textFormatter } from "../../../utils";
import PaymentForm from "../../../components/Form/User/AddPayment/index.form";

function ReportComponents() {
  const location = useLocation();
  const { search } = location;
  const [show, setShow] = useState(false);
  const [isAlertVisibleDelete, setIsAlertVisibleDelete] = useState(false);
  const [tableLoading, setTableLoading] = useState(false);
  const [tableData, setTableData] = useState([]);
  const [noOfPage, setNoOfPage] = useState();
  const [totalCount, setTotalCount] = useState(0);
  const [sizePerPage, setSizePerView] = useState(10);
  const [page, setPage] = useState(1);
  const [setParam] = useState({});
  const [rowData, setRowData] = useState("");

  useEffect(() => {
    if (search) {
      const data = decodeQueryData(search);
      setParam(data);
      setPage(data?.page ?? 1);
    }
  }, [location]);

  const onHandleShow = (data) => {
    setRowData(data);
    setShow(true);
  };

  const onTypeDeleteConfirmAlert = () => {
    setTableLoading(true);
    setIsAlertVisibleDelete(false);
    return false;
  };

  const tableReset = () => {
    setTableLoading(true);
    setTableData([]);
    setNoOfPage(0);
    setTotalCount(0);
  };

  const customerData = [
    {
      id: 1,
      name: "Ashley Lawson",
      mode: "Card, UPI, QR",
      phone: "9580095000",
      price: "5000",
      status: "active",
    },
  ];

  const options = () => {
    const optionsArr = [
      {
        name: "View",
        icon: "icon ni ni-eye",
        action: "confirm",
        onClickHandle: () => {
          // onHandleShow(row);
          document.body.click();
        },
      },
    ];

    return optionsArr;
  };
  const breadcrumb = [
    {
      path: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Report",
    },
  ];

  const columns = [
    {
      dataField: "name",
      text: "Customer Name",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "mode",
      text: "Payment Mode",
      formatter: (cell) => textFormatter(cell),
      // headerClasses: "sorting",
    },
    {
      dataField: "price",
      text: "Price",
      formatter: (cell) => checkValidData(textFormatter(cell)),
      // headerClasses: "sorting",
    },
    {
      dataField: "action",
      text: "Action",
      headerClasses: "nk-tb-col-tools text-end",
      formatter: (cell, row) => actionFormatter(options(row)),
    },
  ];

  const filterData = [
    {
      id: "/dashboard",
      name: "DASHBOARD",
    },
    {
      path: "#",
      name: "Report",
    },
  ];

  useEffect(() => {
    setTableData(customerData);
  }, []);

  return (
    <>
      <div className="nk-block-head nk-block-head-sm">
        <div className="nk-block-between">
          <PageHeader heading="Report">
            <Breadcrumb breadcrumb={breadcrumb} />
          </PageHeader>
          {/* <ListingHeader
            btnText="Add Payment"
            btnArray={["filter"]}
            onHandleShow={onHandleShow}
            // popover={
            //   <DailyReportFilter
            //   // onSubmit={onFilterData}
            //   // filterData={filterData}
            //   // onReset={onReset}
            //   // loading={tableLoader}
            //   />
            // }
          /> */}
        </div>
      </div>
      <Formik>
        <div className="mb-3">
          <Select
            id="month"
            className="w-20"
            name="month"
            disabled={false}
            placeholder="Select Type"
            data={filterData}
            // onSelect={(e) => onSelectMonth(props.setFieldValue, e)}
          />
        </div>
      </Formik>
      <DataTable
        header={false}
        hasLimit
        noOfPage={noOfPage}
        sizePerPage={sizePerPage}
        page={page}
        count={totalCount}
        tableData={tableData}
        tableColumns={columns}
        setSizePerPage={setSizePerView}
        // param={param}
        // defaultSort={defaultSort}
        tableLoader={tableLoading}
        tableReset={tableReset}
        // getSearchValue={getSearchValue}
        // searchPlaceholder={t("text.search.ManageSubscription")}
      />
      <SweetAlert
        title="Are you sure"
        text="you want to delete this user?"
        show={isAlertVisibleDelete}
        icon="warning"
        showCancelButton
        confirmButtonText="Yes"
        cancelButtonText="No"
        setIsAlertVisible={setIsAlertVisibleDelete}
        // showLoaderOnConfirm
        // loading={loading}
        onConfirmAlert={onTypeDeleteConfirmAlert}
      />

      <ModalComponent
        show={show}
        title={rowData?.id ? "Edit Payment" : "Add Payment"}
        onHandleShow={onHandleShow}
        onHandleCancel={() => {
          setShow(false);
        }}
      >
        <PaymentForm
          // onSubmit={addBatteryType}
          rowData={rowData}
          type="Battery Charging"
        />
      </ModalComponent>
    </>
  );
}

export default ReportComponents;
